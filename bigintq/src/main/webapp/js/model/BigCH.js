class BigCH {
	constructor(ko) {
		this.ko = ko
		this.parameters = ko.observableArray([])
		this.variables = ko.observableArray([])
		this.expressions = ko.observableArray([])
		this.lambdas = ko.observableArray([])
		
		this.prefixedX = ko.observableArray([])
		
		this.mathMLExpressions = ko.observableArray([])
	}
	
	setInstance(functions) {
		this.mathMLExpressions(functions.slice(0, functions.length-1))
		MathJax.typesetPromise()
	}
}

class BigLambda {
	constructor(ko) {
		this.value = ko.observable(1)
	}
}

class BigVariable {
	constructor(ko, name) {
		this.ko = ko
		this.name = name
		this.values = ko.observable(null)
		this.minRandom = ko.observable(1)
		this.maxRandom = ko.observable(10)
	}
	
	generateRandomValues(ch) {
		let self = this
		let data = {
			type : "post",
			url : "problems/randomValues?name=" + this.name() + "&min=" + this.minRandom() + "&max=" + this.maxRandom(),
			data : self.ko.toJSON(ch.parameters),
			contentType : "application/json",
			success : function(response) {
				self.values(response)
			},
			error : function(response) {
				alert(response.responseJSON.message)	
			}
		}
		$.ajax(data)		
	}
}

class BigParameter {
	constructor(ko, name) {
		this.name = name
		this.value = ko.observable(null)
	}
}