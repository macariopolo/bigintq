class NewRule {
	constructor(ko) {
		this.ko = ko
		this.summation = ko.observable(new Summation(ko, this))
		
		this.mathML = ko.observable(null)
	}
	
	addSummation() {
		this.summation().addSummation()
	}
	
	toJSON() {
		return this.summation().toJSON()
	}
}

class Summation {
	constructor(ko) {
		this.ko = ko
		this.next = ko.observable(null)
		this.from = ko.observable(new From(ko))
		this.to = ko.observable(new To(ko))
		this.body = ko.observable(new Body(ko))
		this.forAll = ko.observable(new ForAll(ko))
	}
	
	addSummation() {
		let next = this.next()
		let previous = this
		while (next) {
			previous = next
			next = next.next()
		}
		
		previous.body(null)
		previous.forAll(null)
		previous.next(new Summation(this.ko))
	}
	
	toJSON() {
		let result = {
			type : "IndexedSummation",
			from : this.from().toJSON(),
			to : this.to().toJSON(),
			forAlls : []
		}
		if (this.body())
			result.body = this.body().toJSON()
		else
			result.next = this.next().toJSON()
		if (this.forAll())
			result.forAlls.push(this.forAll().toJSON())
		
		return result
	}
}

class ForAll {
	constructor(ko) {
		this.ko = ko;
		this.variable = ko.observable("i")
		this.start = ko.observable(0)
		this.end = ko.observable("m")
	}
	
	toJSON() {
		return {
			type : "IntervalForAll",
			variable : this.variable(),
			start : this.start(),
			end : this.end() 
		}
	}
}

class From {
	constructor(ko) {
		this.ko = ko
		this.left = ko.observable("j")
		this.right = ko.observable(0)
	}
	
	toJSON() {
		return {
			left : this.left(),
			right : {
				type : "DoubleValue",
				value : this.right()
			}
		}
	}
}

class To {
	constructor(ko) {
		this.ko = ko
		this.expr = ko.observable("m")
	}
	
	toJSON() {
		return {
			type : "Parameter",
			name : this.expr()
		}
	}
}

class Body {
	constructor(ko) {
		this.ko = ko
		this.left = ko.observable("v_i_j·x_i_j")
		this.right = ko.observable("K")
		this.operator = ko.observable("EQ")
	}
	
	toJSON() {
		return {
			left : this.left(),
			operator : this.operator(),
			right : this.right()
		}
	}
}