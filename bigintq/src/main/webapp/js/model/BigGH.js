class BigGH {
	constructor($, ko) {
		this.$ = $
		this.ko = ko
		this.problemName = ko.observable(null)
		this.parameters = ko.observable(null)
		this.variables = ko.observableArray([])
		this.xCardinals = ko.observable(null)
		this.expressions = ko.observableArray([])
		this.mathMLExpressions = ko.observableArray([]) 
		
		this.ch = ko.observable(new BigCH(ko))
	}
	
	toJSON() {
		let info = {
			problemName : this.problemName(),
			parameters : this.parameters(),
			variables : this.variables(),
			xCardinals : this.xCardinals(),
			expressions : this.expressions()
		}
		return info
	}
	
	clear() {
		let self = this
		let data = {
			type : "get",
			url : "problems/clear",
			success : function() {
				self.problemName(null)
				self.parameters([])
				self.variables([])
				self.expressions([])
				
				self.mathMLExpressions([])				
			}
		}
		this.$.ajax(data)
	}
	
	buildInstance(vm, callback) {
		vm.gh().ch().setInstance([])
		let data = {
			type : "post",
			url : "problems/instantiate?forQA=true",
			contentType : "application/json",
			data : this.ko.toJSON(this.ch()),
			success : function(response) {
				vm.gh().ch().setInstance(response)
				vm.numberOfCombinations(response[response.length-1])
				vm.status("Problem instantiated")
				vm.errorInstantiating(null)
				if (callback)
					callback()
			},
			error : function(response) {
				vm.errorInstantiating(response.responseJSON.message)	
			}
		}
		this.$.ajax(data)
	}
	
	loadPrefixedX(vm) {
		let self = this
		vm.gh().ch().setInstance([])
		let data = {
			type : "post",
			url : "problems/getPrefixedX",
			contentType : "application/json",
			data : this.ko.toJSON(this.ch()),
			success : function(response) {
				for (let i=0; i<response.length; i++) {
					let datum = {
						coordinates : response[i],
						value : self.ko.observable(null)
					}
					vm.gh().ch().prefixedX.push(datum)
				}
				vm.status("Problem instantiated")
				vm.errorInstantiating(null)
			},
			error : function(response) {
				vm.errorInstantiating(response.responseJSON.message)	
			}
		}
		this.$.ajax(data)
	}
	
	giveValues() {
		this.ch().parameters([])
		this.ch().variables([])
		this.ch().lambdas([])
		
		let names = this.parameters().split(",")
		for (let i=0; i<names.length; i++)
			this.ch().parameters.push(new BigParameter(this.ko, names[i]))
			
		for (let i=0; i<this.variables().length; i++) 
			this.ch().variables.push(new BigVariable(this.ko, this.variables()[i].name))
		
		for (let i=0; i<this.expressions().length; i++)
			this.ch().lambdas.push(new BigLambda(this.ko))
	}
	
	fillWith(vm, problem) {
		this.problemName(problem.problemName)
		this.parameters(problem.parameters)
		this.variables([])
		if (problem.variables) {
			for (let i=0; i<problem.variables.length; i++) {
				let variable = new BigGVariable(this.ko, problem.variables[i].name, problem.variables[i].cardinals)
				this.variables.push(variable)
			}
			let names = "" 
			for (let i=0; i<problem.variables.length; i++)
				names = names + problem.variables[i].name + (i<problem.variables.length-1 ? "," : "")
			vm.variableNames(names)
		}

		this.xCardinals(problem.xCardinals)
		this.expressions(problem.expressions)
		this.loadMathML(problem)
	}
	
	toEquality(index) {
		let self = this
		let data = {
			type : "get",
			url : "problems/toEquality/" + index,
			contentType : "application/json",
			success : function(response) {
				self.expressions.replace(self.expressions()[index], JSON.parse(response))
				self.loadMathML()
			},
			error : function(response) {
				alert(response.responseJSON.message)	
			}
		}
		this.$.ajax(data)
	}
	
	removeExpression(index) {
		let self = this
		let data = {
			type : "delete",
			url : "problems/remove/" + index,
			contentType : "application/json",
			success : function(response) {
				self.expressions.splice(index, 1)
				self.mathMLExpressions.splice(index, 1)
			},
			error : function(response) {
				alert(response.responseJSON.message)	
			}
		}
		this.$.ajax(data)
	}
	
	loadMathML(problem) {
		let self = this
		this.mathMLExpressions([])
		let data = {
			type : "post",
			url : "mathML/getMathML",
			contentType : "application/json",
			success : function(response) {
				self.mathMLExpressions(response)
				MathJax.typesetPromise()
			},
			error : function(response) {
				alert(response.responseJSON.message)	
			}
		}
		if (problem)
			data.data = JSON.stringify(problem)
		this.$.ajax(data)
	}
}

class BigGVariable {
	constructor(ko, name, cardinals) {
		this.name = ko.observable(name)
		this.cardinals = ko.observable(cardinals)
	}
}