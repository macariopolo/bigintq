class RHCluster {
	constructor(ko, vm) {
		this.hosts = ko.observableArray([])
		this.vm = vm
		this.combinations = 0
		this.ch = null
		this.percentage = ko.observable(null)
		this.bests = ko.observableArray([])
		this.startTime = 0
		this.totalTime = ko.observable(null)
		this.remainingTime = ko.observable("-")
		
		this.runningHosts = 0
		this.finished = 0
	}
	
	sortBests() {
		this.bests.sort((a, b) => (a.energy() > b.energy() ? 1 : 0))
		this.totalTime((Date.now()-this.startTime)/1000)
	}
	
	checkFinish() {
		if (++this.finished==this.runningHosts) {
			this.percentage(100)
			this.remainingTime(0)
		}
	}
	
	setRemainingTime(seconds) {
		this.remainingTime(seconds) 
	}
	
	calculate(vm) {
		this.vm = vm
		this.startTime = Date.now()
		let host
		let info = []
		for (let i=0; i<this.hosts().length; i++) {
			host = this.hosts()[i]
			if (host.active()==1) {
				host.clear()
				info.push({
					host : host.id,
					cores : host.numberOfThreads(),
					chunk : vm.chunk()
				})
			}
		}
		this.runningHosts = info.length
		if (this.runningHosts==0) {
			alert("There are no running hosts")
			return
		}
		
		let self = this
		this.percentage(0)
		this.bests([])
		let data = {
			type : "post",
			url : "scheduler/distribute",
			contentType : "application/json",
			data : JSON.stringify(info),
			success : function(response) {
				self.vm.error(null)
				let ch = undefined
				let cluster = response.cluster
				for (let i=0; i<cluster.hosts.length; i++) {
					for (let j=0; j<self.hosts().length; j++) {
						host = self.hosts()[j]
						if (!ch)
							ch = JSON.parse(response.ch)
						if (host.id==cluster.hosts[i].id) {
							host.calculate(vm, cluster.hosts[i].start, cluster.hosts[i].end, ch, self)
							break
						}
					}	
				}					
			},
			error : function(response) {
				self.vm.error(response.responseJSON.message)	
			}
		}
		$.ajax(data)
	}
	
	loadPercentage() {
		let self = this
		let data = {
			type : "get",
			url : "scheduler/getPercentage",
			contentType : "application/json",
			success : function(response) {
				response = response.toFixed(2)
				self.percentage(response)				
			},
			error : function(response) {
				self.vm.error(response.responseJSON.message)	
			}
		}
		$.ajax(data)		
	}
	
	checkAvailability() {
		for (let i=0; i<this.hosts().length; i++)
			this.hosts()[i].checkAvailability()
	}
	
	clear() {
		this.hosts([])
	}
	
	push(remoteHost) {
		this.hosts.push(remoteHost)
	}
	
	loadHosts(ko, hosts) {
		let rHost
		let host
		for (let i=0; i<hosts.length; i++) {
			host = hosts[i]
			rHost = new RemoteHost(ko, host.id, host.ip, host.port)
			rHost.active(host.active)
			rHost.numberOfThreads(host.numberOfThreads)
			this.hosts.push(rHost)
		}
	}
}

class RemoteHost {
	constructor(ko, id, ip, port) {
		this.ko = ko
		this.id = id
		this.ip = ip
		this.port = port
		this.url = "ws://" + this.ip + ":" + this.port + "/wsQACalculator"
		this.time = ko.observable(null)
		this.active = ko.observable(0)
		this.chunk = ko.observable(null)
		this.numberOfThreads = ko.observable(0)
		this.totalCombinations = ko.observable(0)
		this.speed = ko.observable(null)
		this.bestEnergy = Infinity
		this.bestCombinations = ko.observableArray([])
		this.randomCombination = ko.observable(null)
		this.finished = ko.observable(false)
	}
	
	clear() {
		this.time(null)
		this.bestCombinations([])
		this.speed(null)
	}
	
	calculate(vm, start, end, ch, cluster) {
		let self = this
		
		self.bestEnergy = Infinity
		self.bestCombinations([])
		self.totalCombinations(parseInt(end)-parseInt(start))
		
		this.wsCalculator = new WebSocket(this.url)
			
		this.wsCalculator.onopen = function() {
			self.startTime = Date.now()
			let msg = {
				type : "FIRST",
				ch : ch,
				start : start,
				end : end
			}
			this.send(JSON.stringify(msg))
		}
		
		this.wsCalculator.onmessage = function(event) {
			let data = JSON.parse(event.data)
			if (data.type == "random") {
				self.randomCombination(data)
			} else if (data.type == "partial") {
				self._addCombination(data)				
			} else if (data.type == "final") {
				self._requestMore(cluster)
				cluster.loadPercentage()
			}
		}
	}
	
	_addCombination(data) {
		if (data.energy==this.bestEnergy) {
			let combination = new Combination(this.ko, data)
			this.bestCombinations.push(combination)
		} else if (data.energy<this.bestEnergy) {
			this.bestEnergy = data.energy
			this.bestCombinations([])
			let combination = new Combination(this.ko, data)
			this.bestCombinations.push(combination)
		}
	}
	
	_getSpeed() {
		let self = this
		let data = {
			type : "get",
			url : "scheduler/getSpeed/" + self.id,
			contentType : "application/json",
			success : function(response) {
				self.speed(response)		
			},
			error : function(response) {
				self.vm.error(response.responseJSON.message)	
			}
		}
		$.ajax(data)
	}
	
	_requestMore(cluster) {
		let self = this
		let data = {
			type : "get",
			url : "scheduler/getMore/" + self.id,
			contentType : "application/json",
			success : function(response) {
				cluster.setRemainingTime(response.remainingTime)
				let host = response.host
				self.totalCombinations(host.total)
				if (host.finished) {
					self.wsCalculator.close()
					self._getSpeed()
					self._highlightBests(cluster)
					self.finished(true)
					cluster.checkFinish()
				} else {
					let msg = {
						type : "NEXT",
						start : host.start,
						end : host.end,
						bestEnergy : self.bestEnergy
					}
					self.wsCalculator.send(JSON.stringify(msg))	
					self.chunk(host.chunk)
					self.speed(host.speed)
				}		
			},
			error : function(response) {
				self.vm.error(response.responseJSON.message)	
			}
		}
		$.ajax(data)		
	}
	
	checkAvailability() {
		let self = this
		
		this.active(100)
		
		let ws = new WebSocket(this.url)
		ws.onopen = function() {
			self.active(1)
		}
		
		ws.onmessage = function(event) {
			let data = JSON.parse(event.data)
			if (data.type=="threads")
				self.numberOfThreads(data.threads)
			this.close()
		}
		
		ws.onerror = function() {
			self.active(-1)
			this.close()
		}
	}
	
	_highlightBests(cluster) {
		let self = this
		let millis = Date.now()-self.startTime
		self.time((millis/1000) + " seconds")
		for (let i=0; i<self.bestCombinations().length; i++) {
			cluster.bests.push(self.bestCombinations()[i])
			self.bestCombinations()[i].makeDetachable()
		}
		cluster.sortBests()
	}
}

class Combination {
	constructor(ko, data) {
		this.ko = ko
		this.combination = ko.observable(data.combination)
		this.energy = ko.observable(data.energy)
		this.detachable = ko.observable(false)
		this.detachs = ko.observableArray([])
		this.positions = ko.observableArray([])
	}
	
	makeDetachable() {
		this.detachable(true)
		let values = this.combination().split(",")
		let value
		for (let i=0; i<values.length; i++) {
			value = values[i].trim()
			if (value.includes("1"))
				this.positions.push(i)
		}
	}
	
	detach() {
		let self = this
		let data = {
			type : "post",
			url : "problems/detach",
			data : self.ko.toJS(self.combination),
			contentType : "application/json",
			success : function(response) {
				self.detachs([])
				for (let i=0; i<response.length; i++) {
					self.detachs.push(response[i])
				}
			},
			error : function(response) {
				alert(response.responseJSON.message)	
			}
		}
		$.ajax(data)
	}
}