class DWaveSolver {
	constructor(ko, info) {
		this.id = info.id
		this.description = info.description
		this.qubits = info.qubits
		this.online = ko.observable(null)
		this.errorMessage = ko.observable(null)
	}
	
	checkOnline($) {
		let self = this
		let data = {
			type : "get",
			url : "dwave/checkOnline?id=" + this.id,
			contentType : "application/json",
			success : function(response) {
				self.online(response)
			},
			error : function(response) {
				self.online(false)
				self.errorMessage(response)
			}
		}
		$.ajax(data)
	}
}