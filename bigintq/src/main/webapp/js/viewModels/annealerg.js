define([ 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'accUtils',
		'jquery' ], function(ko, app, moduleUtils, accUtils, $) {

	class AnnealerGViewModel {
		constructor() {
			this.gh = ko.observable(new BigGH($, ko))
			this.ch = ko.observable(new BigCH(ko))
			this.simplifiedCh = ko.observable(new BigCH(ko))
			
			this.ghVisible = ko.observable(false)
			
			this.ghText = ko.observable('{"problemName":"boxes XY","variables":"v","parameters":"n,K","expressions":[{"type":"IndexedSummation","from":{"left":"i","right":{"type":"DoubleValue","value":0}},"to":{"type":"Parameter","name":"n"},"body":{"left":{"type":"Product","left":{"type":"IndexedVariableReference","variable":"v","indexes":"i"},"right":{"type":"UseOfX","indexes":"i"}},"right":{"type":"Parameter","name":"K"}}},{"type":"IndexedSummation","from":{"left":"i","right":{"type":"DoubleValue","value":0}},"to":{"type":"Parameter","name":"n"},"body":{"left":{"type":"UseOfX","indexes":"i"}}}]}')
			this.chInstance = ko.observable('{"problemName":"boxes XY","variables":[{"name":"v","values":"1, 3, 2, 6, 4, 5, 4, 10","cardinals":"8"}],"parameters":[{"name":"n","value":8},{"name":"K","value":1}],"xCardinals":"8"}')
			
			this.problemNames = ko.observableArray([])
			this.selectedProblemName = ko.observable(null)
			
			this.variableNames = ko.observable(null)
			
			this.newRule = ko.observable(new NewRule(ko))
			this.errorInNewRule = ko.observable(null)
			
			this.status = ko.observable(null)
			this.error = ko.observable(null)
			
			this.errorInstantiating = ko.observable(null)
			
			this.visibleInstructions = ko.observable(false)
			
			this.loadProblemNames()
		}
		
		connected() {
			document.title = "Generic annealer";
		}
		
		buildInstance() {
			this.gh().buildInstance(this)
		}
		
		loadPrefixedX() {
			this.gh().loadPrefixedX(this)
		}
		
		calculate(method, orderType) {
			if (method=="QASimulator") {
				this.getWebSockets()
				tableSolutions.innerHTML = "<tr><th>Host</th><th>Thread</th><th>Combination</th><th>Energy</th></tr>"
			} else {
				this.startSC(orderType)
			}
		}
		
		startSC(orderType) {
			tableSolutions.innerHTML = "<tr><th>Host</th><th>Thread</th><th>Combination</th><th>Energy</th></tr>"
			
			let self = this
			let data = {
				type : "post",
				url : "scheduler/startSC",
				data : ko.toJSON(this.gh().ch()),
				contentType : "application/json",
				success : function(httpSessionId) {
					self._openSCWebSocket(httpSessionId, orderType)
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		_openSCWebSocket(httpSessionId, orderType) {
			let self = this
			let url = location.protocol=="https:" ? "wss://" : "ws://"
			url = url + location.host
			if (location.port.length>0)
				url = url + ":" + location.port
			url = url + "/wsSCResults?httpSessionId=" + httpSessionId
			
			let ws = new WebSocket(url)
			ws.onopen =  function() {				
				self.calculateSortedConstraints(orderType)
				let tr = document.createElement("tr")
				let td = document.createElement("td")
				tr.appendChild(td)
				td.vAlign = "top"
				td.align = "center"
				td.innerHTML = this.url.substring(0, this.url.indexOf("?"))
				tableSolutions.appendChild(tr)
			}
			
			ws.onmessage = function(event) {				
				let data = JSON.parse(event.data)
				
				if (data.type=="calculating") {
					console.log("Expression: " + data.currentExpressionIndex)
				} else {
					let tr = document.createElement("tr")
					let td = document.createElement("td")
					tr.appendChild(td)
					td.align="right"
					td.innerHTML = "-"
					
					td = document.createElement("td")
					tr.appendChild(td)
					td.innerHTML = "-"
					
					td = document.createElement("td")
					tr.appendChild(td)
					td.align="right"
					td.innerHTML = data.combination
					tableSolutions.appendChild(tr)
					
					td = document.createElement("td")
					tr.appendChild(td)
					td.align="right"
					td.innerHTML = data.value + " (total time: " + data.totalTime + " seconds)"
					tableSolutions.appendChild(tr)
				}
			}
		}
		
		calculateSortedConstraints(orderType) {
			let self = this
			let data = {
				type : "get",
				url : "scheduler/calculateSC?orderType=" + orderType,
				//data : ko.toJSON(this.gh().ch()),
				contentType : "application/json",
				success : function(response) {
					self.gh().ch().setInstance(response)
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		_calculateSortedConstraints(httpSessionId) {
			let self = this
			let data = {
				type : "post",
				url : "calculator/calculateWithSortedConstraints",
				data : ko.toJSON(this.gh().ch()),
				contentType : "application/json",
				success : function(response) {
					
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		getMatrix() {
			let self = this
			let data = {
				type : "get",
				url : "problems/getMatrix",
				contentType : "application/json",
				success : function(response) {
					self.status("Matrix loaded")
					self.simplifiedCh().setInstance(response.mathML)
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		showGH(visible) {
			this.ghVisible(visible)
		}
		
		clearProblem() {
			this.gh().clear()
		}
		
		loadProblem() {
			let self = this
			let data = {
				type : "get",
				url : "problems/getProblem?problemName=" + this.selectedProblemName(),
				contentType : "application/json",
				success : function(response) {
					self.ghText(response.problem)
					let problem = JSON.parse(response.problem)
					self.gh().fillWith(self, problem)
					self.status("Problem loaded")
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		checkNewRule() {
			let self = this
			let data = {
				type : "post",
				url : "problems/checkNewRule",
				contentType : "application/json",
				data : JSON.stringify(this.newRule().toJSON()),
				success : function(response) {
					self.newRule(new NewRule(ko))
					self.errorInNewRule(null)
					self.ghText(response)
					self.gh().fillWith(self, response)
					MathJax.typesetPromise()
				},
				error : function(response) {
					self.newRule().mathML(null)
					self.errorInNewRule(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		loadProblemNames() {
			let self = this
			let data = {
				type : "get",
				url : "problems/getProblemNames",
				contentType : "application/json",
				success : function(response) {
					self.problemNames(response)
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		saveProblem() {
			let self = this
			let info = {
				problemName : this.gh().problemName(),
				parameters : this.gh().parameters(),
				variables : this.gh().variables(),
				xCardinals : this.gh().xCardinals() 
			}
			let data = {
				type : "put",
				url : "problems/saveProblem",
				contentType : "application/json",
				data : ko.toJSON(info),
				success : function(response) {
					self.status("Problem saved")
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		getWebSockets() {
			let self = this
			let data = {
				type : "get",
				url : "scheduler/getWebSockets",
				contentType : "application/json",
				success : function(response) {
					self.wwss = new Array()
					for (let i=0; i<response.length; i++) {
						createWebSocket(self, response[i])
					}
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		showInstructions(visible) {
			this.visibleInstructions(visible)
		}
		
		setCardinals() {
			this.gh().variables([])
			if (this.variableNames()==null)
				return
			let names = this.variableNames().split(",")
			for (let i=0; i<names.length; i++)
				this.gh().variables.push(new BigGVariable(ko, names[i])) 
		}
	}
	
	function createWebSocket(vm, info) {
		let ws = new WebSocket(info.url)
		vm.wwss.push(ws)
		ws.onopen = function() {
			this.send(JSON.stringify(info))
		}
		ws.onmessage = function(event) {			
			let data = JSON.parse(event.data)
			let solutions = JSON.parse(data.solutions)
			let tr = document.createElement("tr")
			let td = document.createElement("td")
			tr.appendChild(td)
			td.vAlign = "top"
			td.align = "center"
			td.innerHTML = this.url + "<br>(" + data.time + " seconds)"
			td.rowSpan = solutions.length + 1
			tableSolutions.appendChild(tr)
			
			for (let i=0; i<solutions.length; i++) {
				tr = document.createElement("tr")
				td = document.createElement("td")
				tr.appendChild(td)
				td.align="right"
				td.innerHTML = solutions[i].core
				
				td = document.createElement("td")
				tr.appendChild(td)
				td.innerHTML = solutions[i].combination
				
				td = document.createElement("td")
				tr.appendChild(td)
				td.align="right"
				td.innerHTML = solutions[i].value
				tableSolutions.appendChild(tr)
			}
		}
		ws.onerror = function(event) {
			console.log("Error: " + event.data)
		}
		ws.onclose = function(event) {
			console.log("Cierre: " + event.data)
		}
	}

	return AnnealerGViewModel;
});
