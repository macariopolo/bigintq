define([ 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'accUtils',
		'jquery' ], function(ko, app, moduleUtils, accUtils, $) {

	class QBViewModel {
		constructor() {
			this.cluster = app.cluster
			this.clusterVisible = ko.observable(false)
			
			if (!localStorage.hostConfigurationId)
				app.router.go( { path : "hosts" })
			else if (this.cluster()==null && localStorage.hosts) {
				let cluster = new RHCluster(ko, this)
				let hosts = JSON.parse(localStorage.hosts)
				cluster.loadHosts(ko, hosts)
				app.cluster(cluster)
				this.cluster = app.cluster
			}
			
			let self = this
			let data = {
				type : "get",
				url : "problems/get",
				success : function(response) {
					self.sessionId = response
				}
			}
			$.ajax(data)

			this.gh = ko.observable(null)
			this.ch = ko.observable(new BigCH(ko))
			this.numberOfCombinations = ko.observable(null)
			
			this.simplifiedCh = ko.observable(new BigCH(ko))
			this.mathMLMatrix = ko.observable(null)
			this.jsonMatrix = ko.observable(null)
			this.textMatrix = ko.observable(null)
			
			this.ghVisible = ko.observable(false)
			this.showGetValuesVisible = ko.observable(false)
			
			this.ghText = ko.observable('{"problemName":"boxes XY","variables":"v","parameters":"n,K","expressions":[{"type":"IndexedSummation","from":{"left":"i","right":{"type":"DoubleValue","value":0}},"to":{"type":"Parameter","name":"n"},"body":{"left":{"type":"Product","left":{"type":"IndexedVariableReference","variable":"v","indexes":"i"},"right":{"type":"UseOfX","indexes":"i"}},"right":{"type":"Parameter","name":"K"}}},{"type":"IndexedSummation","from":{"left":"i","right":{"type":"DoubleValue","value":0}},"to":{"type":"Parameter","name":"n"},"body":{"left":{"type":"UseOfX","indexes":"i"}}}]}')
			this.chInstance = ko.observable('{"problemName":"boxes XY","variables":[{"name":"v","values":"1, 3, 2, 6, 4, 5, 4, 10","cardinals":"8"}],"parameters":[{"name":"n","value":8},{"name":"K","value":1}],"xCardinals":"8"}')
			
			this.problemNames = ko.observableArray([])
			this.selectedProblemName = ko.observable(null)
			
			this.variableNames = ko.observable(null)
			
			this.newRule = ko.observable(null)
			this.errorInNewRule = ko.observable(null)
			
			this.status = ko.observable(null)
			this.error = ko.observable(null)
			
			this.errorInstantiating = ko.observable(null)
			
			this.visibleInstructions = ko.observable(false)
			this.visibleCalculating = ko.observable(false)
			
			this.pythonVisible = ko.observable(false)
			this.pythonCode = ko.observableArray([])
			this.pythonResults = ko.observableArray([])
			this.pythonErrors = ko.observableArray([])
			this.solvers = ko.observableArray([])
			this.selectedSolver = ko.observable(null)
			this.classPath = ko.observable("/Users/macariopolousaola/opt/anaconda3/bin:/Users/macariopolousaola/opt/anaconda3/condabin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:.:/Library/Apple/usr/bin")
			
			this.thirdPlatform = ko.observable(null)
			
			this.loadProblemNames()
			this.loadProblemInstance()
			
			this.isInstanceBuilt = ko.observable(false)
			
			this.chunk = ko.observable(100)
			this.maxChunk = ko.observable(1000)

			this.isGHInIsing = ko.observable(false)	
			this.isCHInIsing = ko.observable(false)
		}
		
		connected() {
			document.title = "Generic annealer";
		}
		
		showOrHidePythonCode() {
			this.pythonVisible(!this.pythonVisible())
		}
		
		showOrHideCluster() {
			this.clusterVisible(!this.clusterVisible())
		}
		
		loadProblemInstance() {
			let self = this
			let data = {
				type : "get",
				url : "problems/getProblem",
				contentType : "application/json",
				success : function(response) {
					if (response) {
						self.ghText(response.problem)
						let problem = JSON.parse(response.problem)
						self.gh(new BigGH($, ko))
						self.gh().fillWith(self, problem)
						self._loadInstance()
					}
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		_loadInstance() {
			let self = this
			let data = {
				type : "get",
				url : "problems/getProblemInstance",
				contentType : "application/json",
				success : function(response) {
					if (response) {
						self.ch().setInstance(response)
						self.showGetValuesVisible(true)
					}
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		goTo(path) {
			app.router.go( { path : path } )
		}
		
		showGiveValues() {
			this.showGetValuesVisible(true)
			this.gh().giveValues()
		}
		
		buildInstance() {
			let self = this
			this.gh().buildInstance(this, function() {
				let host
				let info = []
				for (let i=0; i<self.cluster().hosts().length; i++) {
					host = self.cluster().hosts()[i]
					if (host.active()==1) {
						host.clear()
						info.push({
							host : host.id,
							cores : host.numberOfThreads()
						})
					}
				}
				$.ajax({
					type : "post",
					url : "scheduler/getRecommendedChunk",
					contentType : "application/json",
					data : JSON.stringify(info),
					success: function(response) {
						self.chunk(response)
						self.maxChunk(response*10)
					}
				})
			})
			this.isInstanceBuilt(true)
		}
		
		runOnThirdPlatform() {
			let self = this
			self.pythonErrors([])
			self.pythonResults([])
			if (this.thirdPlatform()=="dwave") {
				let info = {
					classPath : self.classPath(),
					code : this.pythonCode()
				}
				let url = location.protocol=="https:" ? "wss://" : "ws://"
				url = url + location.host
				if (location.port.length>0)
					url = url + ":" + location.port
				url = url + "/wsDwave"
				
				let ws = new WebSocket(url)
				ws.onopen = function() {
					this.send(JSON.stringify(info))
				}
				ws.onerror = function() {
					self.pythonErrors.push("Error connecting to the websocket")
				}
				ws.onmessage = function(event) {
					let response = JSON.parse(event.data)
					if (response.returnCode==0) {
						self.pythonErrors([])
						self.pythonResults(response.outputLines)
					} else {
						self.pythonErrors(response.errorLines)
						self.pythonResults([])
					}
					ws.close()
				}
			}
		}
		
		parseGHToIsing() {
			let self = this
			let data = {
				type : "post",
				url : "parser/parseGHToIsing",
				contentType : "application/json",
				data: this.gh().ko.toJSON(this.gh()),
				success : function(response) {
					self.errorInNewRule(null)
					self.ghText(response.problem)
					let problem = JSON.parse(response.problem)
					self.gh().fillWith(self, problem)
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
			this.isGHInIsing(true)
		}
		
		parseGHToQUBO() {
			let self = this
			let data = {
				type : "post",
				url : "parser/parseGHToQUBO",
				contentType : "application/json",
				data: this.gh().ko.toJSON(this.gh()),
				success : function(response) {
					self.errorInNewRule(null)
					self.ghText(response.problem)
					let problem = JSON.parse(response.problem)
					self.gh().fillWith(self, problem)
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
			this.isGHInIsing(false)
		}
		
		parseCHToIsing() {
			let self = this
			let data = {
				type : "post",
				url : "parser/parseCHToIsing",
				contentType : "application/json",
				data: this.gh().ko.toJSON(this.gh().ch()),
				success : function(response) {
					self.gh().ch().setInstance(response)
					
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
			this.isCHInIsing(true)
		}
		
		parseCHToQUBO() {
			let self = this
			let data = {
				type : "post",
				url : "parser/parseCHToQUBO",
				contentType : "application/json",
				data: this.gh().ko.toJSON(this.gh().ch()),
				success : function(response) {
					self.gh().ch().setInstance(response)
					
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
			this.isCHInIsing(false)
 		}
		
		setSolver() {
			let code = this.pythonCode()
			let i=0
			while (code[i]!="#SOLVER")
				i++
			code[i+1] = this.selectedSolver().solver
			while (code[i]!="#RESULT")
				i++
			code[i+1] = this.selectedSolver().result
			this.pythonCode(code)
		}
		
		getPythonCode(platform) {
			if (platform=="dwave") {
				let self = this
				if (self.jsonMatrix()==null) {
					alert("Please, build first the matrix")
					return
				}
				self.thirdPlatform("dwave")
				let data = {
					type : "post",
					url : "dwave/getPythonCode",
					data : this.jsonMatrix(),
					contentType : "application/json",
					success : function(response) {
						self.pythonVisible(true)
						self.solvers(response.solvers)
						self.pythonCode(response.code)
					},
					error : function(response) {
						self.error(response.responseJSON.message)	
					}
				}
				$.ajax(data)
			}
		}
		
		loadPrefixedX() {
			this.gh().loadPrefixedX(this)
		}
		
		createRule() {
			this.newRule(new NewRule(ko))
		}
		
		calculate(method, orderType) {
			this._clearErrors()
			this.visibleCalculating(true)
			let hostConfigurationId = localStorage.hostConfigurationId
			if (!hostConfigurationId) {
				this.errorInstantiating("Please, select a host configuration for running the problem")
				return
			}
			if (method=="QASimulator") {
				this.cluster().calculate(this)
			} else {
				this.startSC(orderType, hostConfigurationId)
			}
		}
		
		startSC(orderType, hostConfigurationId) {
			this._clearErrors()
			
			let self = this
			let data = {
				type : "post",
				url : "scheduler/startSC",
				data : ko.toJSON(this.gh().ch()),
				contentType : "application/json",
				success : function() {
					self._openSCWebSocket(orderType, hostConfigurationId)
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		_openSCWebSocket(orderType, hostConfigurationId) {
			let self = this
			let url = location.protocol=="https:" ? "wss://" : "ws://"
			url = url + location.host
			if (location.port.length>0)
				url = url + ":" + location.port
			url = url + "/wsSCResults"
			
			let ws = new WebSocket(url)
			ws.onopen =  function() {
				self._clearErrors()		
				self.calculateSC(orderType, hostConfigurationId)
			}
			
			ws.onmessage = function(event) {				
				let data = JSON.parse(event.data)
				console.log(event.data)
			}
		}
		
		calculateSC(orderType, hostConfigurationId) {
			let self = this
			let data = {
				type : "get",
				url : "scheduler/calculateSC?orderType=" + orderType + "&hostConfigurationId=" + hostConfigurationId,
				contentType : "application/json",
				success : function(response) {
					self.gh().ch().setInstance(response[0])
					self.remoteHosts([])
					for (let i=0; i<response[1].length; i++) {
						let remoteHost = {
							url : response[1][i],
							time : ko.observable(null),
							threads : ko.observableArray([
								{
									thread : 1,
									combination : null,
									energy : null,
									color : null,
									detachable : ko.observable(false),
									detachs : ko.observableArray([]),
									positions : ko.observableArray([])
								}
							])
						}
						self.remoteHosts.push(remoteHost)
					}
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		_calculateSortedConstraints(httpSessionId) {
			let self = this
			let data = {
				type : "post",
				url : "calculator/calculateWithSortedConstraints",
				data : ko.toJSON(this.gh().ch()),
				contentType : "application/json",
				success : function(response) {
					
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		getMatrix() {
			this._clearErrors()			
			let self = this
			let data = {
				type : "get",
				url : "problems/getMatrix?format=math",
				contentType : "application/json",
				success : function(response) {
					self.status("Matrix loaded")
					self.mathMLMatrix(response)
					MathJax.typesetPromise()
					self.getJSON()
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		getJSON() {
			this._clearErrors()			
			let self = this
			let data = {
				type : "get",
				url : "problems/getMatrix?format=json",
				contentType : "application/json",
				success : function(response) {
					self.jsonMatrix(response)
					sessionStorage.jsonMatrix = JSON.stringify(response)
					self.getText()
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		getText() {
			this._clearErrors()			
			let self = this
			let data = {
				type : "get",
				url : "problems/getMatrix?format=text",
				contentType : "application/json",
				success : function(response) {
					let data = response
					if (data.length>30)
						data=data.substring(0, 30) + "..."
					self.textMatrix(data)
					sessionStorage.textMatrix = response
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		showGH(visible) {
			this.ghVisible(visible)
		}
		
		clearProblem() {
			this._clearErrors()
			this.gh().clear()
			this.gh(null)
			this.variableNames(null)
			this.showGetValuesVisible(false)
			this.isInstanceBuilt(false)
			this.isGHInIsing(false)
			this.isCHInIsing(false)
		}
		
		_clearErrors() {
			this.error(null)
			this.errorInNewRule(null)
			this.errorInstantiating(null)			
		}
		
		createProblem() {
			this.gh(new BigGH($, ko))		
		}
		
		loadProblem() {
			this._clearErrors()
			let self = this
			let data = {
				type : "get",
				url : "problems/getProblem?problemName=" + this.selectedProblemName(),
				contentType : "application/json",
				success : function(response) {
					self.ghText(response.problem)
					let problem = JSON.parse(response.problem)
					self.gh(new BigGH($, ko))
					self.gh().fillWith(self, problem)
					self.status("Problem loaded")
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		checkNewRule() {
			this._clearErrors()
			let self = this
			let data = {
				type : "post",
				url : "problems/checkNewRule",
				contentType : "application/json",
				data : JSON.stringify(this.newRule().toJSON()),
				success : function(response) {
					self.newRule(new NewRule(ko))
					self.errorInNewRule(null)
					self.ghText(JSON.stringify(response))
					self.gh().fillWith(self, response)
					MathJax.typesetPromise()
				},
				error : function(response) {
					self.newRule().mathML(null)
					self.errorInNewRule(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		loadProblemNames() {
			let self = this
			let data = {
				type : "get",
				url : "problems/getProblemNames",
				contentType : "application/json",
				success : function(response) {
					self.problemNames(response)
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		saveProblem() {
			this._clearErrors()
			let self = this
			if (self.variableNames().length>0 && this.gh().variables().length==0) {
				alert("Please, press Set cardinals to specify variable cardinals")
				return
			}
			let info = this.gh().toJSON()
			let data = {
				type : "put",
				url : "problems/saveProblem",
				contentType : "application/json",
				data : ko.toJSON(info),
				success : function() {
					self.status("Problem saved")
				},
				error : function(response) {
					alert(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		showInstructions(visible) {
			this.visibleInstructions(visible)
		}
		
		setCardinals() {
			this.gh().variables([])
			if (this.variableNames()==null)
				return
			let names = this.variableNames().split(",")
			for (let i=0; i<names.length; i++)
				this.gh().variables.push(new BigGVariable(ko, names[i])) 
		}
	}
	
	return QBViewModel;
});
