define([ 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'accUtils',
		'jquery' ], function(ko, app, moduleUtils, accUtils, $) {

	class HostsViewModel {
		constructor() {
			this.hostConfigurations = ko.observableArray([])
			this.cluster = new RHCluster(ko, this)
			this.selectedHostConfiguration = ko.observable(null)
			
			this.error = ko.observable(null)
			
			let self = this
			let data = {
				type : "get",
				url : "problems/get",
				success : function(response) {
					self.sessionId = response
					self._loadConfigurations()
				}
			}
			$.ajax(data)
		}
		
		connected() {
			document.title = "Hosts configuration";
		}
		
		loadHosts() {
			let self = this
			let data = {
				type : "get",
				url : "hosts/getHosts?id=" + this.selectedHostConfiguration().id,
				contentType : "application/json",
				success : function(response) {
					self.error(null)
					self.cluster.clear()
					for (let i=0; i<response.length; i++) {
						let host = new RemoteHost(ko, response[i].id, response[i].ip, response[i].port)
						host.active(false)
						self.cluster.push(host)
					}
				},
				error : function(response) {
					self.error(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		check() {
			this.cluster.checkAvailability()
		}
		
		_loadConfigurations() {
			let self = this
			self.hostConfigurations([])
			let data = {
				type : "get",
				url : "hosts/getAll",
				contentType : "application/json",
				success : function(response) {
					self.error(null)
					for (let i=0; i<response.length; i++)
						self.hostConfigurations.push(
							{
								id : response[i].id,
								name : response[i].name,
								hosts : ko.observableArray([])
							}
						)
				},
				error : function(response) {
					self.error(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		select() {
			app.cluster(this.cluster)
			localStorage.hostConfigurationId = this.selectedHostConfiguration().id
			localStorage.hosts = ko.toJSON(this.cluster.hosts)
			app.router.go( { path : "qb" })
		}
	}

	return HostsViewModel;
});
