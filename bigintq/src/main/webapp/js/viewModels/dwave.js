define([ 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'accUtils',
		'jquery' ], function(ko, app, moduleUtils, accUtils, $) {

	class DWaveViewModel {
		constructor() {
			this.solvers = ko.observableArray([])
			this.problems = ko.observableArray([])
			this.selectedSolver = ko.observable(null)
			
			this.status = ko.observable(null)
			this.error = ko.observable(null)
			
			this.label = ko.observable(null)
			this.type = ko.observable("qubo")
			let textMatrix = sessionStorage.textMatrix
			this.textMatrix = ko.observable(textMatrix)
			this.jsonMatrix = ko.observable(JSON.parse(sessionStorage.jsonMatrix))
			this.numReads = ko.observable(100)
		}
		
		connected() {
			document.title = "DWave proxy"
			this.listSolvers()
			this.getProblems()
		}
		
		getPythonCode() {
			let self = this
			let data = {
				type : "post",
				url : "dwave/getPythonCode",
				data : this.jsonMatrix(),
				contentType : "application/json",
				success : function(response) {
					
				},
				error : function(response) {
					self.error(response.responseJSON.message)	
				}
			}
			$.ajax(data)
		}
		
		submit(target) {
			let info = {
				solver : this.selectedSolver().id,
				label : this.label(),
				type : this.type(),
				data : this.textMatrix(),
				params : {
					num_reads : this.numReads()
				}
			}
			if (target=="dwave") {
				let self = this
				let data = {
					type : "post",
					url : "dwave/send",
					data : JSON.stringify(info),
					contentType : "application/json",
					success : function(response) {
						self.status(response)
					},
					error : function(response) {
						self.error(response.responseJSON.message)	
					}
				}
				$.ajax(data)
			}
		}
		
		selectSolver(solver) {
			if (solver.qubits) {
				let textMatrix = sessionStorage.textMatrix
				if (textMatrix) {
					textMatrix = solver.qubits + textMatrix.substring(6)
					this.textMatrix(textMatrix)
				}
				
			}
			this.selectedSolver(solver)
		}
		
		updateSolvers() {
			const spinner = document.getElementById("spinner");
			spinner.removeAttribute('hidden');
			
			let self = this
			let data = {
				type : "get",
				url : "dwave/updateSolvers",
				contentType : "application/json",
				success : function(response) {
					self.listSolvers()
				},
				error : function(response) {
					self.error(response.responseJSON.message)	
					spinner.setAttribute('hidden', '')
				}
			}
			$.ajax(data)
		}
		
		listSolvers() {
			const spinner = document.getElementById("spinner");
			spinner.removeAttribute('hidden');
			
			let self = this
			let data = {
				type : "get",
				url : "dwave/listSolvers",
				contentType : "application/json",
				success : function(response) {
					let solver
					self.solvers([])
					for (let i=0; i<response.length; i++) {
						solver = new DWaveSolver(ko, response[i])
						self.solvers.push(solver)
						solver.checkOnline($)
					}
					spinner.setAttribute('hidden', '')
				},
				error : function(response) {
					self.error(response.responseJSON.message)	
					spinner.setAttribute('hidden', '')
				}
			}
			$.ajax(data)
		}
		
		getProblems() {
			const spinner = document.getElementById("spinner");
			spinner.removeAttribute('hidden');
			
			let self = this
			let data = {
				type : "get",
				url : "dwave/getProblems",
				contentType : "application/json",
				success : function(response) {
					self.problems([])
					for (let i=0; i<response.length; i++) {
						let problem = response[i]
						problem.answer = ko.observable(null)
						self.problems.push(problem)
					}
					spinner.setAttribute('hidden', '')
				},
				error : function(response) {
					self.error(response.responseJSON.message)	
					spinner.setAttribute('hidden', '')
				}
			}
			$.ajax(data)
		}
		
		getProblemInfo(problem) {
			const spinner = document.getElementById("spinner");
			spinner.removeAttribute('hidden');
			
			let url = problem.status=="FAILED" ? "dwave/getProblemInfo/?id=" : "dwave/getProblemAnswer?id="
			
			let self = this
			let data = {
				type : "get",
				url : url + problem.id,
				contentType : "application/json",
				success : function(response) {
					if (problem.status=="FAILED") {
						problem.answer(response.metadata.messages[0].message)
					} else {
						problem.answer(JSON.stringify(response.answer))
					}
					spinner.setAttribute('hidden', '')
				},
				error : function(response) {
					self.error(response.responseJSON.message)	
					spinner.setAttribute('hidden', '')
				}
			}
			$.ajax(data)
		}
	}

	return DWaveViewModel;
});
