package edu.uclm.bigintq.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import edu.uclm.bigintq.model.g.JBigGH;

@Repository
public interface JBigGHDAO extends JpaRepository <JBigGH, String> {

	@Query(value = "select problem_name from jbiggh order by problem_name", nativeQuery = true)
	List<String> getProblemNames();

}
