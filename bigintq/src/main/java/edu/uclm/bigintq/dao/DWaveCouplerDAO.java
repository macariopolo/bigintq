package edu.uclm.bigintq.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.uclm.bigintq.dwave.DWaveCoupler;

@Repository
public interface DWaveCouplerDAO extends JpaRepository <DWaveCoupler, String> {
	
}
