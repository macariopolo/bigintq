package edu.uclm.bigintq.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import edu.uclm.bigintq.entities.Host;

@Repository
public interface HostDAO extends JpaRepository <Host, String> {

	@Query(value = "select * from host where active=1", nativeQuery = true)
	List<Host> getActiveHosts();

	@Query(value = "select * from host where configuration_id= :id", nativeQuery = true)
	List<Host> findByConfigurationId(String id);

	@Query(value = "select * from host where active=1 and configuration_id= :id", nativeQuery = true)
	List<Host> getActiveHosts(String id);
}
