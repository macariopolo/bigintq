package edu.uclm.bigintq.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import edu.uclm.bigintq.dwave.DWaveSolver;

@Repository
public interface DWaveSolverDAO extends JpaRepository <DWaveSolver, String> {
	@Query(value = "select id, description from dwave_solver", nativeQuery = true)
	List<DWaveSolver> getSolvers();
}
