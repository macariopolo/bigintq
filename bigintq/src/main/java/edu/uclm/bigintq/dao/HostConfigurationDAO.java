package edu.uclm.bigintq.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.uclm.bigintq.entities.HostConfiguration;

@Repository
public interface HostConfigurationDAO extends JpaRepository <HostConfiguration, String> {
}
