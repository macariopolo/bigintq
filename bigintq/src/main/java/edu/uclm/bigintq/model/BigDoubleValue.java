package edu.uclm.bigintq.model;

import java.util.List;
import java.util.Objects;

import org.json.JSONObject;

public class BigDoubleValue extends BigExpression implements IBigSimpleExpression {
	private double value;

	public BigDoubleValue(JSONObject jso, BigCH h) {
		super(h);
		this.value = jso.getDouble("value");
	}
	
	public BigDoubleValue(Double value, BigCH h) {
		super(h);
		this.value = value;
	}
	
	public void setValue(double value) {
		this.value = value;
	}
	
	@Override
	protected BigExpression square() {
		BigDoubleValue result = new BigDoubleValue(value*value, getH());
		return result;
	}
	
	@Override
	public List<BigUseOfX> loadUsesOfX() {
		return this.usesOfX;
	}
	
	@Override
	public double getValue() {
		return this.value;
	}

	@Override
	public double getValue(List<Integer> combination) {
		return this.value;
	}

	@Override
	public String toString() {
		return "" + this.value;
	}

	@Override
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "DoubleValue");
		jso.put("value", this.value);
		return jso;
	}

	@Override
	protected String toMathML() {
		return "<mn>" + this.value + "</mn>";
	}
	
	@Override
	protected BigExpression divideInto(double value) {
		this.value = this.value / value;
		return this;
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigDoubleValue other = (BigDoubleValue) obj;
		return Double.doubleToLongBits(value) == Double.doubleToLongBits(other.value);
	}

	public BigDoubleValue times(BigDoubleValue other) {
		BigDoubleValue result = new BigDoubleValue(this.value*other.value, getH());
		return result;
	}
	
	@Override
	public BigExpression parseToIsing() {
		return this;
	}
	
	@Override
	public BigExpression parseToQUBO() {
		return this;
 	}
}
