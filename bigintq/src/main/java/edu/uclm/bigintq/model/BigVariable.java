package edu.uclm.bigintq.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.bigintq.model.g.BigGParameter;
import edu.uclm.bigintq.model.g.BigGVariable;

public class BigVariable {

	private String name;
	private BigArbitraryArray<Double> values;
	
	public BigVariable(String name) {
		this.name = name;
		this.values = new BigArbitraryArray<Double>();
	}

	public void setCardinals(String cardinals) {
		this.values.setDimensions(cardinals);
	}
	
	public void setValues(String sValues, BigGVariable bgv, BigCH h) throws Exception {
		String[] tokens = sValues.split(",");
		String token;
		ArrayList<Double> values = new ArrayList<>();
		for (int i=0; i<tokens.length; i++) {
			token = tokens[i].trim();
			if (token.charAt(0)=='[')
				token = token.substring(1);
			if (token.endsWith("]"))
				token = token.substring(0, token.length()-1);
			try {
				values.add(Double.parseDouble(token));
			} catch (NumberFormatException e) {
				throw new Exception("The value " + token + " used for the variable " + this.name + " is not a number");
			}
		}
		
		int requiredCardinal = 1;
		List<BigGParameter> bigCardinals = bgv.getCardinals();
		for (BigGParameter bigCardinal : bigCardinals) {
			BigParameter parameter = h.findParameter(bigCardinal.getName());
			requiredCardinal = requiredCardinal * (int) parameter.getValue();
		}
		
		for (int i=tokens.length; i<requiredCardinal; i++)
			values.add(0.0);
		//BigGExpr oldRight = (BigGExpr) inequalityIndex[1];
		//if ((int) inequalityIndex[0]>-1)
		//	values.set(tokens.length + (int) inequalityIndex[0], oldRight.getValue(h));
		
		this.values.setValues(values);
	}

	public void setValues(JSONArray jsaValues, BigGVariable bgv, BigCH h) {
		ArrayList<Double> values = new ArrayList<>();
		for (int i=0; i<jsaValues.length(); i++)
			values.add(jsaValues.getDouble(i));
		this.values.setValues(values);
	}
	
	public void setValues(JSONArray jsaValues) {
		ArrayList<Double> values = new ArrayList<>();
		for (int i=0; i<jsaValues.length(); i++)
			values.add(jsaValues.getDouble(i));
		this.values.setValues(values);
	}
	
	public void setValues(double... values) throws Exception {
		int requiredCardinal = values.length; 
		if (values.length<requiredCardinal)
			throw new Exception("Provide " + requiredCardinal + " values at least for the " + this.name + " variable");
		ArrayList<Double> vv = new ArrayList<>();
		for (int i=0; i<values.length; i++)
			vv.add(values[i]);
		this.values.setValues(vv);
	}
	
	public void addValue(double value) {
		this.values.add(value);
	}

	public double getValue(List<Integer> indexes) {
		return this.values.get(indexes);
	}
	
	public String getName() {
		return name;
	}

	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("name", this.name);
		jso.put("cardinals", this.values.getCardinalsAsString());
		jso.put("values", this.values.toJSON());
		return jso;
	}

	public ArrayList<Double> getValues() {
		return this.values.getValues();
	}
}
