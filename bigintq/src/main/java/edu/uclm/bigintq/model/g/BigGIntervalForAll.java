package edu.uclm.bigintq.model.g;

import java.util.Objects;

import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;

public class BigGIntervalForAll extends BigGForAll {
	private BigGExpr start, end;
	
	public BigGIntervalForAll(JSONObject jso, BigGIndexedSummation summation, BigGH h) throws Exception {
		this.variable = jso.getString("variable");
		
		JSONObject jsoStart = this.buildJSONObjectBigExpression(jso, "start");
		this.start = BigGExpr.build(jsoStart, h);
		
		JSONObject jsoEnd = this.buildJSONObjectBigExpression(jso, "end");
		this.end = BigGExpr.build(jsoEnd, h);
	}

	private JSONObject buildJSONObjectBigExpression(JSONObject jso, String keyName) {
		JSONObject jsoKey = jso.optJSONObject(keyName);
		if (jsoKey!=null)
			return jsoKey;
		
		String sKey = jso.optString(keyName);
		try {
			int key = Integer.parseInt(sKey);
			jsoKey = new JSONObject().put("type", "DoubleValue").put("value", key);
		} catch (Exception e) {
			jsoKey = new JSONObject().put("type", "Parameter").put("name", sKey);
		}
		return jsoKey;
	}
	
	@Override
	public String toString() {
		return "fA " + this.variable + " in [" + this.start.toString() + ", " + this.end.toString() + ")";
	}
	
	@Override
	protected JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "IntervalForAll");
		jso.put("variable", variable);
		jso.put("start", this.start.toJSON());
		jso.put("end", this.end.toJSON());
		return jso;
	}

	@Override
	protected String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<mo>∀</mo>");
		sb.append("<mi>" + this.variable + "</mi>");
		sb.append("<mo>∈</mo>");
		sb.append("<mfenced separators='' open='[' close=')'>");
		sb.append(start.toMathML());
		sb.append("<mo>,</mo>");
		sb.append(end.toMathML());
		sb.append("</mfenced>");
		return sb.toString();
	}
	
	@Override
	protected int getStart(BigCH h) {
		return (int) this.start.getValue(h);
	}
	
	@Override
	protected int getEnd(BigCH h) {
		return (int) this.end.getValue(h);
	}

	@Override
	public int hashCode() {
		return Objects.hash(end, start, variable);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGIntervalForAll other = (BigGIntervalForAll) obj;
		return Objects.equals(end, other.end) && Objects.equals(start, other.start)
				&& Objects.equals(variable, other.variable);
	}
	
	
}
