package edu.uclm.bigintq.model.g;

import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;

public abstract class BigGForAll {

	protected String variable;

	public static BigGForAll build(JSONObject jso, BigGIndexedSummation summation, BigGH h) throws Exception {
		String type = jso.getString("type");
		if (type.equals("IntervalForAll"))
			return new BigGIntervalForAll(jso, summation, h);
		return null;
	}

	protected abstract JSONObject toJSON();

	protected abstract String toMathML();
	
	@Override
	public abstract String toString();

	protected abstract int getStart(BigCH h);
	
	protected abstract int getEnd(BigCH h);

	public String getVariable() {
		return this.variable;
	}

}
