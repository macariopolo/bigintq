package edu.uclm.bigintq.model.g;

import java.util.Objects;

import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigDoubleValue;
import edu.uclm.bigintq.model.BigExpression;

public class BigGDoubleValue extends BigGExpr {

	private double value;
	
	public BigGDoubleValue(double value) {
		this.value = value;
	}

	public BigGDoubleValue(JSONObject jso, BigGH h) throws Exception {
		double value = jso.optDouble("value");
		if (Double.isNaN(value))
			throw new Exception("Expected a double value");
		this.value = value;
	}

	@Override
	protected BigExpression getBigExpression(BigCH h, String indexName, int indexValue) {
		return new BigDoubleValue(this.value, h);
	}

	@Override
	public double getValue(BigCH h) {
		return this.value;
	}
	
	@Override
	public String toString() {
		return "" + this.value;
	}

	@Override
	protected JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "DoubleValue");
		jso.put("value", value);
		return jso;
	}

	@Override
	public String toMathML() {
		return "<mn>" + this.value + "</mn>";
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGDoubleValue other = (BigGDoubleValue) obj;
		return Double.doubleToLongBits(value) == Double.doubleToLongBits(other.value);
	}

	@Override
	protected BigGExpr expand(int indexValue, String indexName) {
		return this;
	}
	
	@Override
	protected boolean uses(BigGVariable bgv) {
		return false;
	}
	
	@Override
	public BigGExpr parseToIsing() {
		return this;
	}
	
	@Override
	public BigGExpr parseToQUBO() {
		return this;
	}
}
