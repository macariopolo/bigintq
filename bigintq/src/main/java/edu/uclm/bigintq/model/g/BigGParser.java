package edu.uclm.bigintq.model.g;

public class BigGParser {
	
	public static BigGQuotient parseQUBOToIsing(BigGUseOfX x) {
		// x = (s + 1) / 2
		BigGQuotient bgq = new BigGQuotient();
		BigGDoubleValue bdv = new BigGDoubleValue(1.0);
		BigGUseOfS uos = new BigGUseOfS();
		uos.setIndexes(x.getIndexes());
		BigGSum bs = new BigGSum();
		bs.add(uos);
		bs.add(bdv);
		bgq.setNum(bs);
		bgq.setDenom(new BigGDoubleValue(2.0));
		return bgq;
	}
	
	public static BigGSubtraction parseIsingToQUBO(BigGUseOfS s) {
		// s = 2x - 1 
		BigGSubtraction bs = new BigGSubtraction();
		BigGProduct bp = new BigGProduct();
		bp.setLeft(new BigGDoubleValue(2.0));
		BigGUseOfX uox = new BigGUseOfX();
		uox.setIndexes(s.getIndexes());
		bp.setRight(uox);
		bs.setLeft(bp);
		bs.setRight(new BigGDoubleValue(1.0));
		return bs;
	}

}
