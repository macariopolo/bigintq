package edu.uclm.bigintq.model.g;

import java.util.Objects;

import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigDoubleValue;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigParameter;

public class BigGParameter extends BigGExpr {

	private String name;
	
	public BigGParameter(String name, BigGH h) throws Exception {
		name = name.trim();
		if (name.length()==0)
			throw new Exception("Expected parameter name");
		if (h.findParameter(name)==null)
			throw new Exception("Parameter " + name + " not found in parameters list");
		this.name = name;
	}
	
	public BigGParameter(JSONObject jso, BigGH h) throws Exception {
		this(jso.optString("name"), h);
	}
	
	@Override
	public String toString() {
		return this.name;
	}

	@Override
	protected JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "Parameter");
		jso.put("name", this.name);
		return jso;
	}
	
	@Override
	public String toMathML() {
		return "<mi>" + this.name + "</mi>";
	}

	public BigGParameter(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	protected BigExpression getBigExpression(BigCH h, String indexName, int indexValue) {
		BigParameter bp = h.findParameter(this.name);
		return new BigDoubleValue(bp.getValue(), h);
	}

	@Override
	public double getValue(BigCH h) {
		BigParameter bp = h.findParameter(this.name);
		return bp.getValue();
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGParameter other = (BigGParameter) obj;
		return Objects.equals(name, other.name);
	}

	@Override
	protected BigGExpr expand(int indexValue, String indexName) {
		return this;
	}
	
	@Override
	protected boolean uses(BigGVariable bgv) {
		return false;
	}
	
	@Override
	public BigGExpr parseToIsing() {
		return this;
	}
	
	@Override
	public BigGExpr parseToQUBO() {
		return this;
	}
	
}
