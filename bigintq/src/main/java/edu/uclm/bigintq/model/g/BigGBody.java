package edu.uclm.bigintq.model.g;

import java.util.List;
import java.util.Objects;

import org.json.JSONObject;

public class BigGBody {
	
	private BigGExpr left, right;
	private String operator;
	private boolean slacks;
	
	public BigGBody() {
	}
	
	public BigGBody(JSONObject jso, BigGSummation summation, BigGH h) throws Exception {
		this();
		this.operator = jso.optString("operator");
		if (this.operator.length()==0)
			this.operator="=";
		JSONObject jsoTemp = jso.optJSONObject("left");
		if (jsoTemp!=null)
			this.left = BigGExpr.build(jso.getJSONObject("left"), h);
		else {
			String sLeft = jso.getString("left");
			Character c = check(sLeft);
			if (c!=null)
				throw new Exception("Invalid character (" + c + " ) at " + sLeft);
			if (sLeft.indexOf('·')!=-1)
				this.left = new BigGProduct(sLeft, summation, h);
			else {
				sLeft = sLeft.substring(2);
				this.left = new BigGUseOfX(sLeft, summation);
			}
		}
			
		if (jso.has("right")) {
			jsoTemp = jso.optJSONObject("right");
			if (jsoTemp!=null)
				this.right = BigGExpr.build(jso.getJSONObject("right"), h);
			else if (jso.getString("right").length()>0)
				this.right = new BigGParameter(jso.getString("right"), h);
		}
	}
	
	public void divideByRight() {
		this.left = this.left.divideBy(this.right);
		this.right = new BigGDoubleValue(1);
	}

	public void setSlacks(boolean slacks) {
		this.slacks = slacks;
	}
	
	public boolean hasSlacks() {
		return this.slacks;
	}

	private Character check(String sLeft) {
		Character c=null;
		for (int i=0; i<sLeft.length(); i++) {
			c = sLeft.charAt(i);
			if (!((c>='a' && c<='z' || c>='A' && c<='Z') || c=='_' || c=='·'))
				return c;
		}
		return null;
	}

	public List<BigGIndexedSummation> replace(String indexName, int value) {
		//return left.expand(value, indexName);
		return null;
	}
	
	public void setLeft(BigGExpr left) {
		this.left = left;
	}
	
	public BigGExpr getLeft() {
		return left;
	}
	
	public void setRight(BigGExpr right) {
		this.right = right;
	}
	
	public BigGExpr getRight() {
		return right;
	}
	
	@Override
	public String toString() {
		return this.left.toString() + (this.right!=null ? this.operator + this.right.toString() : "");
	}

	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("left", this.left.toJSON());
		if (this.right!=null) {
			jso.put("operator", this.operator);
			jso.put("right", this.right.toJSON());
		}
		return jso;
	}

	public String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.left.toMathML());
		if (this.slacks) {
			sb.append("<mo>+</mo><mover accent='true'><mi>s</mi><mo>_</mo></mover>");
		}
		if (this.right!=null) {
			sb.append("<mo>" + this.operator + "</mo>");
			sb.append(this.right.toMathML());
		}
		return sb.toString();
	}
	
	public String getOperator() {
		return operator;
	}
	
	public void setOperator(String operator) {
		this.operator = operator;
	}

	@Override
	public int hashCode() {
		return Objects.hash(left, right);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGBody other = (BigGBody) obj;
		return Objects.equals(left, other.left) && Objects.equals(right, other.right);
	}

	public boolean uses(BigGVariable bgv) {
		return this.left.uses(bgv) || this.right.uses(bgv);
	}
	
	public BigGBody parseToIsing() {
		if (this.left != null)
			this.left = left.parseToIsing();
		if (this.right != null)
			this.right = right.parseToIsing();
		return this;
	}

	public BigGBody parseToQUBO() {
		if (this.left != null)
			this.left = left.parseToQUBO();
		if (this.right != null)
			this.right = right.parseToQUBO();
		return this;
	}
}
