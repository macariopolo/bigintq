package edu.uclm.bigintq.model.g;

import java.util.Objects;

import org.json.JSONException;
import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigDoubleValue;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigProduct;

public class BigGSubtraction extends BigGExpr implements IBigGExpandibleExpr {
	
	private BigGExpr left, right;
	
	public BigGSubtraction() {
	}
	
	public BigGSubtraction(String sProduct, BigGH h) throws Exception {
		this();
		String[] tokens = sProduct.split("-");
		if (tokens.length==1)
			throw new Exception("Use '-' to subtract");
		String[] xIndexes, varIndexes;
		if (tokens[0].startsWith("x")) {
			xIndexes = tokens[0].split("_");
			varIndexes = tokens[1].split("_");
		} else {
			xIndexes = tokens[1].split("_");
			varIndexes = tokens[0].split("_");
		}
		if (xIndexes.length==1)
			throw new Exception("Use '_' to separate subindexes (i.e.: x_i_j)");
		if (varIndexes.length==1)
			throw new Exception("Use '_' to separate subindexes (i.e.: v_i_j)");
		
		JSONObject jsoX = new JSONObject().put("type", "UseOfX");
		String sXIndexes = "";
		for (int i=1; i<xIndexes.length; i++)
			sXIndexes = sXIndexes + xIndexes[i] + "_";
		sXIndexes = sXIndexes.substring(0, sXIndexes.length()-1);
		jsoX.put("indexes", sXIndexes);
		
		JSONObject jsoVar = new JSONObject().put("type", "IndexedVariableReference").put("variable", varIndexes[0]);
		String sVarIndexes = "";
		for (int i=1; i<varIndexes.length; i++)
			sVarIndexes = sVarIndexes + varIndexes[i] + "_";
		sVarIndexes = sVarIndexes.substring(0, sVarIndexes.length()-1);
		jsoVar.put("indexes", sVarIndexes);
		
		this.left = BigGExpr.build(jsoVar, h);
		this.right = BigGExpr.build(jsoX, h);
	}
	
	public BigGSubtraction(JSONObject jso, BigGH h) throws JSONException, Exception {
		this();
		this.left = BigGExpr.build(jso.getJSONObject("left"), h);
		this.right = BigGExpr.build(jso.getJSONObject("right"), h);
	}
	
	@Override
	public String toString() {
		return this.left.toString() + "-" + this.right.toString();
	}
	
	@Override
	protected JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "Subtraction");
		jso.put("left", this.left.toJSON());
		jso.put("right", this.right.toJSON());
		return jso;
	}

	@Override
	public String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<mfenced separators='' open='(' close = ')'>");
		sb.append(this.left.toMathML());
		sb.append("<mo>-</mo>");
		sb.append(this.right.toMathML());
		sb.append("</mfenced>");
		return sb.toString();
	}

	@Override
	protected BigExpression getBigExpression(BigCH h, String indexName, int indexValue) throws Exception {
		BigProduct product = new BigProduct(h);
		product.setLeft(this.left.getBigExpression(h, indexName, indexValue));
		product.setRight(this.right.getBigExpression(h, indexName, indexValue));
		
		if (product.getLeft() instanceof BigDoubleValue) {
			BigDoubleValue value = (BigDoubleValue) product.getLeft();
			if (value.getValue()==0.0)
				return null;
		}
		if (product.getRight() instanceof BigDoubleValue) {
			BigDoubleValue value = (BigDoubleValue) product.getRight();
			if (value.getValue()==0.0)
				return null;
		}
		if (product.getLeft() instanceof BigDoubleValue) {
			BigDoubleValue value = (BigDoubleValue) product.getLeft();
			if (value.getValue()==1.0)
				return product.getRight();
		}
		if (product.getRight() instanceof BigDoubleValue) {
			BigDoubleValue value = (BigDoubleValue) product.getRight();
			if (value.getValue()==1.0)
				return product.getLeft();
		}
		return product;
	}

	@Override
	public double getValue(BigCH h) {
		return this.left.getValue(h) - this.right.getValue(h);
	}
	
	public void setLeft(BigGExpr left) {
		this.left = left;
	}
	
	public void setRight(BigGExpr right) {
		this.right = right;
	}

	@Override
	public int hashCode() {
		return Objects.hash(left, right);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGSubtraction other = (BigGSubtraction) obj;
		return Objects.equals(left, other.left) && Objects.equals(right, other.right);
	}

	@Override
	public BigGExpr expand(int indexValue, String indexName) {
		BigGIndexedExpr left = (BigGIndexedExpr) this.left;
		BigGIndexedExpr right = (BigGIndexedExpr) this.right;
		BigGSubtraction result = new BigGSubtraction();
		result.left = left.expand(indexValue, indexName);
		result.right = right.expand(indexValue, indexName);
		return result;
	}

	@Override
	public BigGExpr parseToIsing() {
		this.left = left.parseToIsing();
		this.right = right.parseToIsing();
		return this;
	}

	@Override
	public BigGExpr parseToQUBO() {
		this.left = left.parseToQUBO();
		this.right = right.parseToQUBO();
		return this;
	}

	@Override
	protected boolean uses(BigGVariable bgv) {
		if (left.uses(bgv))
				return true;
		
		if (right.uses(bgv))
			return true;
		return false;
	}	
}
