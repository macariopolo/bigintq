package edu.uclm.bigintq.model.simplifier;

import java.util.Comparator;

import edu.uclm.bigintq.model.BigDoubleValue;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigProduct;
import edu.uclm.bigintq.model.BigSquare;
import edu.uclm.bigintq.model.BigUseOfX;

public class BigExpressionSorter implements Comparator<BigExpression> {

	@Override
	public int compare(BigExpression o1, BigExpression o2) {
		if (o1 instanceof BigDoubleValue)
			return 1;
		if (o2 instanceof BigDoubleValue)
			return -1;
		
		if (o1 instanceof BigUseOfX && o2 instanceof BigUseOfX)
			return compare((BigUseOfX) o1, (BigUseOfX) o2);
		
		if (o1 instanceof BigSquare && o2 instanceof BigSquare) {
			BigSquare a = (BigSquare) o1;
			BigSquare b = (BigSquare) o2;
			return compare(a, b);
		}
		
		if (o1 instanceof BigProduct && o2 instanceof BigProduct) {
			BigProduct a = (BigProduct) o1;
			BigProduct b = (BigProduct) o2;
			
			return compare(a, b);
		}
		return 0;
	}
	
	private int compare(BigProduct a, BigProduct b) {
		if (a.getRight() instanceof BigSquare && b.getRight() instanceof BigSquare) {
			BigSquare as = (BigSquare) a.getRight();
			BigSquare bs = (BigSquare) b.getRight();
			return compare(as, bs);
		}
		if (a.getRight() instanceof BigSquare)
			return -1;
		if (a.getLeft() instanceof BigSquare)
			return 1;
		return 0;
	}
	
	private int compare(BigSquare a, BigSquare b) {
		BigUseOfX ax = this.getUseOfX(a);
		BigUseOfX bx = this.getUseOfX(b);
		return new ArraySorter().compare(ax.getIndexes(), bx.getIndexes());
	}
	
	private int compare(BigUseOfX a, BigUseOfX b) {
		return new ArraySorter().compare(a.getIndexes(), b.getIndexes());
	}

	private BigUseOfX getUseOfX(BigSquare square) {
		BigUseOfX uox;
		if (square.getExpr() instanceof BigProduct) {
			uox = (BigUseOfX) ((BigProduct) square.getExpr()).getRight();
		} else {
			uox = (BigUseOfX) square.getExpr();
		}
		return uox;
	}

}
