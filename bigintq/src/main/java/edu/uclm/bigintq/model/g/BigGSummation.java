package edu.uclm.bigintq.model.g;

import java.util.List;

import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigExpression;

public abstract class BigGSummation {

	protected List<BigGBody> body;
	protected BigGSummation previous, next;
	protected boolean proceedsFromInequality;
	protected String oldOperator;
	private BigGExpr oldRight;
	protected int startSlack;	
	
	public static BigGSummation build(JSONObject jso, BigGSummation previous, BigGH gh) throws Exception {
		if (jso.getString("type").equals("IndexedSummation"))
			return new BigGIndexedSummation(jso, previous, gh);
		throw new Exception(jso.getString("type") + " not supported");
	}
	
	public abstract List<BigExpression> expand(BigCH h, boolean forQASimulator, int[] slackCounter) throws Exception;

	public abstract String toMathML();
	public abstract JSONObject toJSON();
	
	@Override
	public abstract String toString();

	protected abstract List<BigGBody> developSummation(BigCH h, boolean forQASimulator);

	protected abstract List<BigGIndexedSummation> replace(String indexName, int i);

	protected abstract boolean containsAsIndex(String indexVariable);

	public final boolean hasRightMember() {
		return this.body.get(0).getRight()!=null;
	}

	public final void toEquality() {
		BigGSummation last = this;
		if (this.next!=null) {
			while (last.next!=null) {
				last = last.next;
			}
		}
		for (BigGBody bodyElement : last.body) {
			bodyElement.setSlacks(true);
			//bodyElement.divideByRight();
			bodyElement.setOperator("=");
		}
		this.oldRight = last.body.get(0).getRight();
	}

	public final boolean proceedsFromInequality() {
		return this.proceedsFromInequality;
	}
	
	public BigGExpr getOldRight() {
		return oldRight;
	}

	public void increaseXCardinals(BigCH ch) {
		ch.increaseXCardinals();
	}

	public boolean uses(BigGVariable bgv) {
		for (BigGBody bodyElement : this.body)
			if (bodyElement.uses(bgv))
				return true;
		return false;
	}

	protected final void setStartSlack(int startSlack) {
		this.startSlack = startSlack;
	}
	
	public String getOldOperator() {
		return oldOperator;
	}
	
	public void setOldOperator(String oldOperator) {
		this.oldOperator = oldOperator;
	}
	
	public abstract BigGSummation parseToIsing();
	
	public abstract BigGSummation parseToQUBO();
}
