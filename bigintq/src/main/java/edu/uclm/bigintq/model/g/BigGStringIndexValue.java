package edu.uclm.bigintq.model.g;

import java.util.Objects;

import edu.uclm.bigintq.model.BigCH;

public class BigGStringIndexValue extends BigGIndexValue {

	private String index;

	public BigGStringIndexValue(String index) {
		this.index = index;
	}

	@Override
	protected Integer getValue(BigCH h) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		return this.index;
	}

	@Override
	protected String toMathML() {
		return "<mn>" + this.index + "</mn>";
	}

	@Override
	public int hashCode() {
		return Objects.hash(index);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGStringIndexValue other = (BigGStringIndexValue) obj;
		return Objects.equals(index, other.index);
	}

	
}
