package edu.uclm.bigintq.model;

import java.util.List;
import java.util.Objects;

import org.json.JSONObject;

public class BigSubtraction extends BigLambdadedExpression {
	private BigExpression left, right;

	public BigSubtraction(BigCH h) {
		super(h);
	}
	
	public BigSubtraction(JSONObject jso, BigCH h) {
		this(h);
		this.left = BigExpression.build(jso.getJSONObject("left"), h);
		this.right = BigExpression.build(jso.getJSONObject("right"), h);
	}

	@Override
	public double getValue(List<Integer> combination) {
		return this.left.getValue(combination) - this.right.getValue(combination);
	}
	
	@Override
	protected BigExpression square() {
		BigSum result = new BigSum(getH());
		BigExpression left = this.left.square();
		BigExpression right = this.right.square();
		
		BigProduct duplo = new BigProduct(getH());
		duplo.setLeft(new BigDoubleValue(-2.0, getH()));
		BigProduct duploRight = new BigProduct(getH());
		duploRight = new BigProduct(getH());
		duploRight.setLeft(this.left);;
		duploRight.setRight(this.right);;
		duplo.setRight(duploRight);;
		
		result.add(left);
		result.add(right);
		result.add(duploRight);
		return result;
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}
	
	public BigExpression getLeft() {
		return left;
	}
	
	public void setLeft(BigExpression left) {
		this.left = left;
	}
	
	public BigExpression getRight() {
		return right;
	}
	
	public void setRight(BigExpression right) {
		this.right = right;
	}
	
	@Override
	public List<BigUseOfX> loadUsesOfX() {
		this.usesOfX.clear();
		this.usesOfX.addAll(this.left.loadUsesOfX());
		this.usesOfX.addAll(this.right.loadUsesOfX());
		return this.usesOfX;
	}

	@Override
	public String toString() {
		return this.left + "-" + this.right;
	}
	
	@Override
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "Subtraction");
		jso.put("left", this.left.toJSON());
		jso.put("right", this.right.toJSON());
		jso.put("lambda", this.lambda);
		return jso;
	}

	@Override
	protected String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<mfenced separators='' open='(' close=')'>");
		sb.append(this.left.toMathML());
		sb.append("<mo>-</mo>");
		sb.append(this.right.toMathML());
		sb.append("</mfenced>");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(left, right);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigSubtraction other = (BigSubtraction) obj;
		return Objects.equals(left, other.left) && Objects.equals(right, other.right);
	}

	@Override
	public BigSum develop(BigCH h) {
		// TODO Auto-generated method stub
		return null;
	}

	public BigExpression divideInto(double value) {
		if (value==1)
			return this;
		this.left = this.left.divideInto(value);
		this.right = this.right.divideInto(value);
		return this;
	}

	public BigSum toSum() {
		BigSum result = new BigSum(getH());
		BigSum left = (BigSum) this.left;
		for (BigExpression expr : left.getExpressions())
			result.add(expr);
		
		IBigSimpleExpression right = (IBigSimpleExpression) this.right;
		BigDoubleValue minusRight = new BigDoubleValue(-right.getValue(), getH());
		
		result.add(minusRight);
		return result;
	}
	
	@Override
	public BigExpression parseToIsing() {
		this.left = left.parseToIsing();
		this.right = right.parseToIsing();
		return this;
	}
	
	@Override
	public BigExpression parseToQUBO() {
		this.left = left.parseToQUBO();
		this.right = right.parseToQUBO();
		return this;
	}
}
