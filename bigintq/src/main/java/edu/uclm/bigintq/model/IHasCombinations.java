package edu.uclm.bigintq.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public interface IHasCombinations {
	int getCombinationLength();
	
	default List<Integer> getCombination(BigInteger index) {
		List<Integer> result = new ArrayList<>();
		BigInteger TWO = BigInteger.valueOf(2);
		
		BigInteger quotient = index;
		BigInteger remainder;
		do {
			remainder = quotient.remainder(TWO);
			quotient = quotient.divide(TWO);
			result.add(0, remainder.intValue());
		} while (quotient.compareTo(BigInteger.ONE)>0);
		
		result.add(0, quotient.intValue());
		for (int i=result.size(); i<this.getCombinationLength(); i++)
			result.add(0, 0);
		return result;
	}
	
	default List<Integer> getCombination(long index) {
		return this.getCombination(BigInteger.valueOf(index));
	}
}
