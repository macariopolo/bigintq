package edu.uclm.bigintq.model.simplifier;

import edu.uclm.bigintq.model.BigUseOfX;

public class TwoBigUsesOfX {
	private BigUseOfX a, b;
	
	public TwoBigUsesOfX(BigUseOfX a, BigUseOfX b) {
		this.a = a;
		this.b = b;
	}
	
	@Override
	public String toString() {
		return "(" + a.toString() + ", " + b.toString() + ")";
	}
	
	public BigUseOfX getA() {
		return a;
	}
	
	public BigUseOfX getB() {
		return b;
	}
}
