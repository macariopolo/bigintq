package edu.uclm.bigintq.model.g;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigDoubleValue;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigVariable;

public class BigGIndexedVariableReference extends BigGIndexedExpr {
	private BigGVariable variable;
	
	public BigGIndexedVariableReference() {
		super();
	}
	
	public BigGIndexedVariableReference(JSONObject jso, BigGH h) throws Exception {
		super(jso);
		String variableName = jso.getString("variable");
		if (variableName.length()==0)
			throw new Exception("Missing the name of a variable");
		this.variable = h.findVariable(variableName);
		if (this.variable==null) {
			BigGVariable variable = new BigGVariable(jso.getString("variable"));
			String cardinals = jso.optString("cardinals");
			if (cardinals.length()==0)
				throw new Exception("Missing cardinals of " + variableName);
			variable.setCardinals(h, jso.getString("cardinals"));
			this.variable = h.addVariable(variable);
		}
	}		
	
	@Override
	public String toString() {
		String r = this.variable.getName();
		for (int i=0; i<this.indexes.size(); i++)
			r = r + "_" + this.indexes.get(i);
		return r;
	}
	
	@Override
	public BigGIndexedExpr expand(int indexValue, String indexName) {
		BigGIndexedVariableReference result = new BigGIndexedVariableReference();
		result.variable = this.variable;
		BigGIndexValue index;
		for (int i=0; i<this.indexes.size(); i++) {
			index = this.indexes.get(i);
			if (index.toString().equals(indexName)) {
				result.indexes.add(new BigGIntegerIndexValue(indexValue));
			} else {
				result.indexes.add(index);
			}
		}
		return result;
	}

	@Override
	protected JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "IndexedVariableReference");
		jso.put("variable", this.variable.getName());
		jso.put("indexes", this.indexesToJSON());
		return jso;
	}

	@Override
	public String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<msub>");
		sb.append("<mi>" + this.variable.getName() + "</mi>");
		if (indexes.size()>0) {
			sb.append("<mrow>");
			for (int i=0; i<indexes.size(); i++)
				sb.append(this.indexes.get(i).toMathML());
			sb.append("</mrow>");
		}
		sb.append("</msub>");
		return sb.toString();
	}
	
	@Override
	protected BigExpression getBigExpression(BigCH h, String indexName, int indexValue) {
		BigGIndexedVariableReference bivr = (BigGIndexedVariableReference) this.expand(indexValue, indexName);
		BigVariable bv = h.findVariable(this.variable.getName());
		List<Integer> indexes = new ArrayList<>();
		for (BigGIndexValue index : bivr.indexes)
			indexes.add(index.getValue(h));
		return new BigDoubleValue(bv.getValue(indexes), h);
	}

	@Override
	public double getValue(BigCH h) {
		BigVariable vb = h.findVariable(this.variable.getName());
		List<Integer> indexes = new ArrayList<>();
		for (BigGIndexValue biv : this.indexes) {
			indexes.add(biv.getValue(h));
		}
		return vb.getValue(indexes);
	}

	@Override
	public int hashCode() {
		return Objects.hash(indexes, variable);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGIndexedVariableReference other = (BigGIndexedVariableReference) obj;
		return Objects.equals(indexes, other.indexes) && Objects.equals(variable, other.variable);
	}

	@Override
	protected boolean uses(BigGVariable bgv) {
		return this.variable.getName().equals(bgv.getName());
	}
	
	@Override
	public BigGExpr parseToIsing() {
		return this;
	}
	
	@Override
	public BigGExpr parseToQUBO() {
		return this;
	}
}
