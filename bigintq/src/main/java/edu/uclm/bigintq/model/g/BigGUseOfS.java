package edu.uclm.bigintq.model.g;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigUseOfS;

public class BigGUseOfS extends BigGIndexedExpr {
	
	public BigGUseOfS() {
	}
	
	public BigGUseOfS(String xSubindexes) {
		super(xSubindexes);
	}
	
	public BigGUseOfS(JSONObject jso, BigGH h) {
		super(jso);
	}
	
	@Override
	public String toString() {
		String r = "s";
		for (int i=0; i<this.indexes.size(); i++)
			r = r + "_" + this.indexes.get(i);
		return r;
	}
	
	@Override
	public BigGIndexedExpr expand(int indexValue, String indexName) {
		BigGUseOfS result = new BigGUseOfS();
		BigGIndexValue index;
		for (int i=0; i<this.indexes.size(); i++) {
			index = this.indexes.get(i);
			if (index.toString().equals(indexName)) {
				result.indexes.add(new BigGIntegerIndexValue(indexValue));
			} else {
				result.indexes.add(index);
			}
		}
		return result;
	}
	
	@Override
	protected JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "UseOfS");
		jso.put("indexes", this.indexesToJSON());
		return jso;
	}

	@Override
	public String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<msub>");
		sb.append("<mi>s</mi>");
		if (indexes.size()>0) {
			sb.append("<mrow>");
			for (int i=0; i<indexes.size(); i++)
				sb.append(this.indexes.get(i).toMathML());
			sb.append("</mrow>");
		}
		sb.append("</msub>");
		return sb.toString();
	}

	@Override
	protected BigExpression getBigExpression(BigCH h, String indexName, int indexValue) {
		BigGUseOfS bguos = (BigGUseOfS) this.expand(indexValue, indexName);
		List<Integer> indexes = new ArrayList<>();
		for (BigGIndexValue index : bguos.indexes)
			indexes.add(index.getValue(h));
		
		BigUseOfS buos = new BigUseOfS(h);
		buos.setIndexes(indexes);
		return buos;
	}

	@Override
	public double getValue(BigCH h) {
		List<Integer> indexes = new ArrayList<>();
		for (BigGIndexValue biv : this.indexes) {
			indexes.add(biv.getValue(h));
		}
		return h.getX().getValue(indexes);
	}

	@Override
	public int hashCode() {
		return Objects.hash(indexes);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGUseOfS other = (BigGUseOfS) obj;
		return Objects.equals(indexes, other.indexes);
	}

	@Override
	public BigGExpr parseToIsing() {
		return this;
	}

	@Override
	public BigGExpr parseToQUBO() {
		return BigGParser.parseIsingToQUBO(this);
	}

	@Override
	protected boolean uses(BigGVariable bgv) {
		// TODO Auto-generated method stub
		return false;
	}
}
