package edu.uclm.bigintq.model.g;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class BigGIndexedExpr extends BigGExpr implements IBigGExpandibleExpr {

	protected List<BigGIndexValue> indexes;
	
	public BigGIndexedExpr() {
		this.indexes = new ArrayList<>();
	}
	
	public BigGIndexedExpr(String indexes) {
		this();
		setIndexes(indexes);
	}
	
	public BigGIndexedExpr(JSONObject jso) {
		this();
		JSONArray jsaIndexes = jso.optJSONArray("indexes");
		if (jsaIndexes!=null)
			this.setIndexes(jsaIndexes);
		else
			this.setIndexes(jso.optString("indexes"));
	}

	private void setIndexes(JSONArray jsa) {
		String indexes = "";
		for (int i=0; i<jsa.length(); i++) 
			indexes = indexes + jsa.get(i) + "_";
		indexes = indexes.substring(0, indexes.length()-1);
		this.setIndexes(indexes);
	}

	private void setIndexes(String indexes) {
		String[] tokens = indexes.split("_");
		for (String token : tokens) {
			try {
				int index = Integer.parseInt(token.trim());
				this.indexes.add(new BigGIntegerIndexValue(index));
			} catch (Exception e) {
				this.indexes.add(new BigGStringIndexValue(token.trim()));
			}
		}
	}
	
	protected JSONArray indexesToJSON() {
		JSONArray jsa = new JSONArray();
		for (BigGIndexValue index : this.indexes)
			jsa.put(index.toString());
		return jsa;
	}

	public List<BigGIndexValue> getIndexes() {
		return indexes;
	}

	public void setIndexes(List<BigGIndexValue> indexes) {
		this.indexes = indexes;
	}
}
