package edu.uclm.bigintq.model.g;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigDoubleValue;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigUseOfX;

public class BigGUseOfX extends BigGIndexedExpr {
	
	public BigGUseOfX() {
	}
	
	public BigGUseOfX(String xSubindexes, BigGSummation summation) {
		super(xSubindexes);
	}
	
	public BigGUseOfX(JSONObject jso, BigGH h) {
		super(jso);
	}
	
	@Override
	public String toString() {
		String r = "x";
		for (int i=0; i<this.indexes.size(); i++)
			r = r + "_" + this.indexes.get(i);
		return r;
	}
	
	@Override
	public BigGIndexedExpr expand(int indexValue, String indexName) {
		BigGUseOfX result = new BigGUseOfX();
		BigGIndexValue index;
		for (int i=0; i<this.indexes.size(); i++) {
			index = this.indexes.get(i);
			if (index.toString().equals(indexName)) {
				result.indexes.add(new BigGIntegerIndexValue(indexValue));
			} else {
				result.indexes.add(index);
			}
		}
		return result;
	}
	
	@Override
	protected JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "UseOfX");
		jso.put("indexes", this.indexesToJSON());
		return jso;
	}

	@Override
	public String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<msub>");
		sb.append("<mi>x</mi>");
		if (indexes.size()>0) {
			sb.append("<mrow>");
			for (int i=0; i<indexes.size(); i++)
				sb.append(this.indexes.get(i).toMathML());
			sb.append("</mrow>");
		}
		sb.append("</msub>");
		return sb.toString();
	}

	@Override
	protected BigExpression getBigExpression(BigCH h, String indexName, int indexValue) throws Exception {
		BigGUseOfX bivr = (BigGUseOfX) this.expand(indexValue, indexName);
		List<Integer> indexes = new ArrayList<>();
		Integer value;
		for (BigGIndexValue index : bivr.indexes) {
			value = index.getValue(h);
			if (value==null)
				throw new Exception("Variable " + index + " not found in " + this.toString());
			indexes.add(value);
		}
		
		BigExpression result;
		Double prefixedValue = h.getPrefixedXValue(indexes);
		if (prefixedValue==null) {
			BigUseOfX uox = new BigUseOfX(h);
			uox.setIndexes(indexes);
			result = uox;
		} else {
			result = new BigDoubleValue(prefixedValue, h);
		}
		return result;
	}

	@Override
	public double getValue(BigCH h) {
		List<Integer> indexes = new ArrayList<>();
		for (BigGIndexValue biv : this.indexes) {
			indexes.add(biv.getValue(h));
		}
		return h.getX().getValue(indexes);
	}

	@Override
	public int hashCode() {
		return Objects.hash(indexes);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGUseOfX other = (BigGUseOfX) obj;
		return Objects.equals(indexes, other.indexes);
	}
	
	@Override
	protected boolean uses(BigGVariable bgv) {
		return false;
	}
	
	@Override
	public BigGExpr parseToIsing() {
		return BigGParser.parseQUBOToIsing(this);
	}
	
	@Override
	public BigGExpr parseToQUBO() {
		return this;
	}
}
