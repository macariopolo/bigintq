package edu.uclm.bigintq.model;

import java.util.List;
import java.util.Objects;

import org.json.JSONObject;

import edu.uclm.bigintq.model.simplifier.TwoBigUsesOfX;

public class BigProduct extends BigExpression implements IBigSimpleExpression {
	private BigExpression left, right;
	
	public BigProduct(BigCH h) {
		super(h);
	}

	public BigProduct(JSONObject jso, BigCH h) {
		this(h);
		this.left = BigExpression.build(jso.getJSONObject("left"), h);
		this.right = BigExpression.build(jso.getJSONObject("right"), h);
	}
	
	public BigProduct(BigExpression left, BigExpression right, BigCH h) {
		this(h);
		this.left = left;
		this.right = right;
	}
	
	@Override
	protected BigExpression square() {
		this.simplify();
		BigProduct result = new BigProduct(getH());
		result.setLeft(this.left.square());
		result.setRight(this.right.square());
		return result;
	}
	
	@Override
	protected BigExpression divideInto(double value) {
		this.left = this.left.divideInto(value);
		this.right = this.right.divideInto(value);
		return this;
	}
	
	private void interchange() {
		if (!(this.left instanceof BigDoubleValue) && this.right instanceof BigDoubleValue) {
			BigExpression auxi = this.left;
			this.left = this.right;
			this.right = auxi;
		}
		if (this.left instanceof BigProduct) 
			((BigProduct) this.left).interchange();
		if (this.right instanceof BigProduct)
			((BigProduct) this.right).interchange();
	}
	
	public BigExpression simplify() {
		this.interchange();
		if (this.left instanceof BigProduct)
			((BigProduct) this.left).interchange();
		if (this.right instanceof BigProduct)
			((BigProduct) this.right).interchange();
		
		if (this.left instanceof BigProduct && this.right instanceof BigProduct) {
			BigProduct left1 = (BigProduct) this.left;
			BigProduct right1 = (BigProduct) this.right;
			
			if (left1.left instanceof BigDoubleValue && right1.left instanceof BigDoubleValue) {
				BigProduct result = new BigProduct(getH());
				result.left = ((BigDoubleValue) left1.left).times((BigDoubleValue) right1.left);
				BigProduct right2 = new BigProduct(getH());
				right2.left = left1.right;
				right2.right = right1.right;
				result.right = right2;
				result.interchange();
				return result;
			}
		}
		
		if (this.left instanceof BigUseOfX && this.right instanceof BigProduct) {
			BigProduct result = new BigProduct(getH());
			BigProduct right = (BigProduct) this.right;
			result.left = right.left;
			BigProduct auxi = new BigProduct(getH());
			auxi.left = this.left;
			auxi.right = right.right;
			result.right = auxi;
			return result;
		}
		
		if (this.left instanceof BigProduct && this.right instanceof BigUseOfX) {
			BigProduct result = new BigProduct(getH());
			BigProduct left = (BigProduct) this.left;
			result.left = left.left;
			BigProduct auxi = new BigProduct(getH());
			auxi.left = left.right;
			auxi.right = this.right;
			result.right = auxi;
			return result;
		}
		
		if (this.left instanceof BigDoubleValue && this.right instanceof BigProduct) {
			BigDoubleValue bpLeft1 = (BigDoubleValue) this.left;
			BigProduct bpRight1 = (BigProduct) this.right;
			if (bpRight1.left instanceof BigDoubleValue) {
				BigDoubleValue bpLeft2 = (BigDoubleValue) bpRight1.left;
				BigProduct result = new BigProduct(getH());
				result.left = bpLeft1.times(bpLeft2);
				result.right = bpRight1.right;
				result.interchange();
				return result;
			}
		}
		
		if (this.left instanceof BigProduct && this.right instanceof BigDoubleValue) {
			BigProduct bpLeft1 = (BigProduct) this.left;
			BigDoubleValue bpRight1 = (BigDoubleValue) this.right;
			BigDoubleValue bpLeft2 = (BigDoubleValue) bpLeft1.left;
			BigProduct result = new BigProduct(getH());
			result.left = bpRight1.times(bpLeft2);
			result.right = bpLeft1.right;
			result.interchange();
			return result;
		}
		
		if (this.left instanceof BigDoubleValue && this.right instanceof BigDoubleValue) {
			return ((BigDoubleValue) this.left).times((BigDoubleValue) this.right);
		}
		
		if (this.left instanceof BigDoubleValue) {
			BigDoubleValue left = (BigDoubleValue) this.left;
			if (this.right instanceof BigProduct) {
				BigProduct right = (BigProduct) this.right;
				if (right.getLeft() instanceof BigDoubleValue) {
					BigDoubleValue value = (BigDoubleValue) right.getLeft();
					double r = left.getValue() * value.getValue();
					BigProduct result = new BigProduct(new BigDoubleValue(r, this.getH()), right.getRight(), this.getH());
					return result;
				}
				if (right.getRight() instanceof BigDoubleValue) {
					BigDoubleValue value = (BigDoubleValue) right.getRight();
					double r = left.getValue() * value.getValue();
					BigProduct result = new BigProduct(new BigDoubleValue(r, this.getH()), right.getLeft(), this.getH());
					return result;
				}
			}
			if (this.right instanceof BigDoubleValue) {
				BigDoubleValue right = (BigDoubleValue) this.right;
				left.setValue(left.getValue()*right.getValue());
				return left;
			}
		}
		return this;
	}
	
	@Override
	public List<BigUseOfX> loadUsesOfX() {
		this.usesOfX.clear();
		this.usesOfX.addAll(this.left.loadUsesOfX());
		this.usesOfX.addAll(this.right.loadUsesOfX());
		return this.usesOfX;
	}

	@Override
	public double getValue() {
		IBigSimpleExpression left = (IBigSimpleExpression) this.left;
		IBigSimpleExpression right = (IBigSimpleExpression) this.right;
		return left.getValue() * right.getValue();
	}
	
	public double getCoefficientFor(BigUseOfX uox) {
		IBigSimpleExpression left = (IBigSimpleExpression) this.left;
		IBigSimpleExpression right = (IBigSimpleExpression) this.right;
		if (left.contains(uox)) 
			return right.getValue();
		return left.getValue();
	}
	
	public boolean containsSquare(BigUseOfX uox) {
		IBigSimpleExpression left = (IBigSimpleExpression) this.left;
		IBigSimpleExpression right = (IBigSimpleExpression) this.right;
		if ((left instanceof BigSquare) && left.contains(uox)) 
			return true;
		if ((right instanceof BigSquare) && right.contains(uox)) 
			return true;
		return false;
	}
	
	@Override
	public boolean contains(BigUseOfX uox) {
		return this.left instanceof BigUseOfX && ((BigUseOfX) this.left).equals(uox)
				||
				this.right instanceof BigUseOfX && ((BigUseOfX) this.right).equals(uox);
	}
	
	@Override
	public boolean contains(TwoBigUsesOfX uoxs) {
		IBigSimpleExpression left = (IBigSimpleExpression) this.left;
		IBigSimpleExpression right = (IBigSimpleExpression) this.right;
		
		BigUseOfX a = uoxs.getA();
		BigUseOfX b = uoxs.getB();
		
		return left.contains(uoxs) || 
				right.contains(uoxs) ||
				(left.contains(a) && right.contains(b)) ||
				(left.contains(b) && right.contains(a));
	}
	
	@Override
	public double getValue(List<Integer> combination) {
		return this.left.getValue(combination) * 
				this.right.getValue(combination);
	}

	public void setLeft(BigExpression left) {
		this.left = left;
	}
	
	public void setRight(BigExpression right) {
		this.right = right;
	}

	@Override
	public String toString() {
		return this.left.toString() + "*" + this.right.toString();
	}
	
	@Override
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "Product");
		jso.put("left", this.left.toJSON());
		jso.put("right", this.right.toJSON());
		return jso;
	}

	@Override
	protected String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.left.toMathML()); 
		sb.append("<mo>·</mo>");
		sb.append(this.right.toMathML());
		return sb.toString();
	}
	
	public BigExpression getLeft() {
		return left;
	}
	
	public BigExpression getRight() {
		return right;
	}

	@Override
	public int hashCode() {
		return Objects.hash(left, right);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigProduct other = (BigProduct) obj;
		return Objects.equals(left, other.left) && Objects.equals(right, other.right);
	}
	
	@Override
	public BigExpression parseToIsing() {
		this.left = left.parseToIsing();
		this.right = right.parseToIsing();
		return this;
	}

	@Override
	public BigExpression parseToQUBO() {
		this.left = left.parseToQUBO();
		this.right = right.parseToQUBO();
		return this;
	}
}
