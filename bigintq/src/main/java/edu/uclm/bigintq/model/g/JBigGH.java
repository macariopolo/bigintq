package edu.uclm.bigintq.model.g;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.json.JSONObject;

@Entity
public class JBigGH {

	@Id
	private String problemName;
	@Column(columnDefinition = "LONGTEXT")
	@Lob
	private String problem;
	
	public JBigGH() {
	}
	
	public JBigGH(String problemName) {
		this.problemName = problemName;
	}

	public JBigGH(String problemName, String problem) {
		this(problemName);
		this.problem = problem;
	}

	public JSONObject toJSON() {
		return new JSONObject(this.problem);
	}
	
	public void setProblem(String problem) {
		this.problem = problem;
	}
	
	public String getProblem() {
		return problem;
	}
	
	public String getProblemName() {
		return problemName;
	}
	
	public void setProblemName(String problemName) {
		this.problemName = problemName;
	}

}
