package edu.uclm.bigintq.model.simplifier;

import java.util.List;

import org.json.JSONArray;

import edu.uclm.bigintq.model.BigUseOfX;

public class Matrix {
	
	private List<BigUseOfX> headers;
	private Double[][] values;

	public Matrix(List<BigUseOfX> headers, int size) {
		this.headers = headers;
		this.values = new Double[size][size];
	}

	public void add(BigUseOfX uox, double coefficient) {
		int index = this.find(uox);
		add(coefficient, index, index);
	}
	
	public void add(TwoBigUsesOfX uoxs, double coefficient) {
		BigUseOfX a = uoxs.getA();
		BigUseOfX b = uoxs.getB();
		int indexA = this.find(a);
		int indexB = this.find(b);
		
		this.add(coefficient, indexA, indexB);
		this.add(coefficient, indexB, indexA);
	}

	private void add(double coefficient, int indexA, int indexB) {
		if (this.values[indexA][indexB]==null)
			this.values[indexA][indexB] = coefficient;
		else
			this.values[indexA][indexB] = this.values[indexA][indexB] + coefficient;
	}

	private int find(BigUseOfX uox) {
		for (int i=0; i<headers.size(); i++)
			if (headers.get(i)==uox)
				return i;
		return 0;
	}

	public JSONArray toJSON() {
		JSONArray result = new JSONArray();
		JSONArray jsaHeaders = new JSONArray();
		result.put(jsaHeaders);
		JSONArray jsaRow;
		jsaHeaders.put("");
		for (int i=0; i<this.headers.size(); i++) {
			jsaHeaders.put(this.headers.get(i));
			jsaRow = new JSONArray();
			jsaRow.put(this.headers.get(i));
			result.put(jsaRow);
		}
		
		Double value;
		for (int i=0; i<this.values[0].length; i++) {
			jsaRow = result.getJSONArray(i+1);
			for (int j=0; j<this.values[0].length; j++) {
				value = this.values[i][j];
				jsaRow.put(value==null ? 0 : value);
			}
		}
		
		return result;
	}
	
	public String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<math xmlns = 'http://www.w3.org/1998/Math/MathML'>");
		sb.append("<mrow>");
		sb.append("<mo>[</mo>");
		sb.append("<mtable>");
		
		sb.append("<mtr>");
		sb.append("<mtd></mtd>");
		for (int i=0; i<this.headers.size(); i++) {
			sb.append("<mtd><mi>" + this.headers.get(i) + "</mi></mtd>");
		}
		sb.append("</mtr>");
		
		Double value;
		for (int i=0; i<this.values[0].length; i++) {
			sb.append("<mtr>");
			sb.append("<mtd><mi>" + this.headers.get(i) + "</mi></mtd>");
			for (int j=0; j<this.values[0].length; j++) {
				value = this.values[i][j];
				sb.append("<mtd><mn>" + (value==null ? 0 : value) + "</mn></mtd>");
			}
			sb.append("</mtr>");
		}
		
		sb.append("</mtable>");		
		sb.append("<mo>]</mo>");
		sb.append("</mrow>");
		sb.append("</math>");
		return sb.toString();
	}

	public String toText() {
		StringBuilder sb = new StringBuilder();
		Double value;
		int lines = 0;
		for (int i=0; i<this.values[0].length; i++) {
			for (int j=i; j<this.values[0].length; j++) {
				value = this.values[i][j];
				if (value!=null) {
					sb.append(i + " " + j + " " + value + "\n");
					lines++;
				}
			}
		}
		sb.insert(0, "QUBITS " + lines + "\n");
		String r = sb.toString().trim();
		//if (r.endsWith("\\n"))
		//	r = r.substring(0, r.length()-2);
		return r;
	}

}
