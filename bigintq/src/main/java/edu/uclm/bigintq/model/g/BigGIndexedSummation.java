package edu.uclm.bigintq.model.g;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigDoubleValue;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigProduct;
import edu.uclm.bigintq.model.BigSquare;
import edu.uclm.bigintq.model.BigSubtraction;
import edu.uclm.bigintq.model.BigSum;
import edu.uclm.bigintq.model.BigUseOfX;
import edu.uclm.bigintq.utils.InequalityUtils;

public class BigGIndexedSummation extends BigGSummation {
	
	private BigGFrom from;
	private BigGExpr to;
	private List<BigGForAll> forAlls;
	
	public BigGIndexedSummation() {
		super();
		this.body = new ArrayList<>();
	}

	public BigGIndexedSummation(JSONObject jso, BigGSummation previous, BigGH h) throws Exception {
		this();
		this.previous = previous;
		this.from = new BigGFrom(jso.getJSONObject("from"), h);
		this.to = BigGExpr.build(jso.getJSONObject("to"), h);
				
		this.forAlls = new ArrayList<>();
		JSONArray jsa = jso.optJSONArray("forAlls");
		if (jsa!=null && jsa.length()>0) {
			JSONObject jsoForAll;
			BigGForAll forAll;
			for (int i=0; i<jsa.length(); i++) {
				jsoForAll = jsa.getJSONObject(i);
				if (jsoForAll.optString("variable").length()>0) {
					forAll = BigGForAll.build(jsoForAll, this, h);
					this.forAlls.add(forAll);
				}
			}
		}
		
		JSONObject jsoNext = jso.optJSONObject("next");
		if (jsoNext!=null) {
			this.next = new BigGIndexedSummation(jsoNext, this, h);
		} else {
			JSONObject jsoBody = jso.optJSONObject("body");
			if (jsoBody!=null)	
				this.body.add(new BigGBody(jsoBody, this, h));
			else {
				JSONArray jsaBody = jso.getJSONArray("body");
				for (int i=0; i<jsaBody.length(); i++)
					this.body.add(new BigGBody(jsaBody.getJSONObject(i), this, h));
			}
		}
	}
		
	@Override
	protected boolean containsAsIndex(String indexVariable) {
		if (this.from.getVariable().equals(indexVariable))
			return true;
		for (BigGForAll forAll : this.forAlls) {
			if (forAll.getVariable().equals(indexVariable))
				return true;
		}
		if (this.previous!=null)
			return this.previous.containsAsIndex(indexVariable);
		return false;
	}
	
	@Override
	public String toString() {
		String r = "";
		r = "SUM(" + this.from.toString() + ", " + this.to.toString();
		if (this.next!=null)
			r = r + this.next.toString();
		else
			r = r + " [" + this.body.toString() + "])";
		for (int i=0; i<this.forAlls.size(); i++)
			r = r + ", " + this.forAlls.get(i).toString();
		return r;
	}

	@Override
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "IndexedSummation");
		jso.put("from", this.from.toJSON());
		jso.put("to", this.to.toJSON());
		if (this.next!=null) {
			JSONObject jsoNext = this.next.toJSON();
			jso.put("next", jsoNext);
		} else {
			JSONArray jsa = new JSONArray();
			for (BigGBody body : this.body)
				jsa.put(body.toJSON());
			jso.put("body", jsa);
		}
		if (!this.forAlls.isEmpty())
			jso.put("forAlls", this.forAllsToJSON());
		return jso;
	}

	@Override
	public String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<munderover>");
		sb.append("<mo data-mjx-texclass=\"OP\">&#x2211;</mo>");
		sb.append(this.from.toMathML());
		sb.append(this.to.toMathML());
		sb.append("</munderover>");
		if (this.next!=null) {
			sb.append("<mo>(</mo>");
			sb.append(this.next.toMathML());
			sb.append("<mo>)</mo>");
		} else {
			for (BigGBody body : this.body)
				sb.append(body.toMathML());
		}
		if (!this.forAlls.isEmpty())
			sb.append(this.forAllsToMathML());
		return sb.toString();
	}

	private String forAllsToMathML() {
		StringBuilder sb = new StringBuilder();
		for (BigGForAll forAll : this.forAlls)
			sb.append("<mo>,</mo>" + forAll.toMathML());
		return sb.toString();
	}

	private JSONArray forAllsToJSON() {
		JSONArray jsa = new JSONArray();
		for (BigGForAll forAll : this.forAlls)
			jsa.put(forAll.toJSON());
		return jsa;
	}
	
	@Override
	protected List<BigGIndexedSummation> replace(String indexName, int value) {
		List<BigGIndexedSummation> newSummations = new ArrayList<>();
		for (BigGBody body : this.body) {
			List<BigGIndexedSummation> replacements = body.replace(indexName, value);
			newSummations.addAll(replacements);
		}
		return newSummations;
	}
	
	@Override
	protected List<BigGBody> developSummation(BigCH h, boolean forQASimulator) {
		int start = (int) this.from.getValue(h);
		int to = (int) this.to.getValue(h);
		String indexName = this.from.getVariable();
		
		List<BigGBody> exprs = new ArrayList<>();
		for (int i=start; i<to; i++)
			exprs.addAll(this.expandBody(i, indexName));
		return exprs;
	}
		
	@Override
	public List<BigExpression> expand(BigCH h, boolean forQASimulator, int[] slackCounter) throws Exception {
		this.developNextSummations(h, forQASimulator);
		this.applyForAlls(h);
				
		int start = (int) this.from.getValue(h);
		int to = (int) this.to.getValue(h);
		String indexName = this.from.getVariable();
		
		ArrayList<BigExpression> result = new ArrayList<>();
		for (int i=0; i<this.body.size(); i++) {
			BigSum summation = new BigSum(h);
			BigGBody bodyElement = this.body.get(i);
			for (int j=start; j<to; j++) {
				BigGExpr left = bodyElement.getLeft();
				BigExpression sum = left.getBigExpression(h, indexName, j);
				if (sum!=null)
					summation.add(sum);
			}
			
			if (bodyElement.hasSlacks()) {
				List<BigUseOfX> list = summation.loadUsesOfX();
				double rightValue = this.getOldRight().getValue(h);
				rightValue = Math.log10(rightValue) / Math.log10(2.0);
				rightValue = Math.ceil(rightValue);
				for (int j=0; j<rightValue; j++) {
					BigUseOfX uox = new BigUseOfX(h);
					uox.setSlack(true);
					slackCounter[0] = (slackCounter[0] == 0) ? list.size() : slackCounter[0];
					uox.setIndexes(slackCounter[0]++);
					double factor = Math.pow(2, rightValue-j-1);
					if (factor!=1.0) {
						if (InequalityUtils.hasGreaterOperator(this.getOldOperator()))
							factor *= -1;
						BigProduct product = new BigProduct(h);
						product.setLeft(new BigDoubleValue(factor, h));
						product.setRight(uox);
						summation.add(product);
					} else {
						if (InequalityUtils.hasGreaterOperator(this.getOldOperator())) {
							factor *= -1;
							BigProduct product = new BigProduct(h);
							product.setLeft(new BigDoubleValue(-1.0, h));
							product.setRight(uox);
							summation.add(product);
						} else {
							summation.add(uox);
						}
					}
				}
			}

			if (bodyElement.getRight()!=null) {
				BigSubtraction subtraction = new BigSubtraction(h);
				subtraction.setLeft(summation);
				BigExpression right = bodyElement.getRight().getBigExpression(h, indexName, i);
				subtraction.setRight(right);
				if (forQASimulator) {
					BigSquare square = new BigSquare(h);
					square.setExpr(subtraction);
					result.add(square);
				} else {
					result.add(subtraction);
				}
			} else if (!summation.getExpressions().isEmpty()){
				result.add(summation);
			}				
		}
		return result;
	}

	private void developNextSummations(BigCH h, boolean forQASimulator) {
		if (this.next!=null) {
			BigGSummation parent = null;
			BigGSummation last = this;
			while (last.next!=null) {
				parent = last;
				last = last.next;
			}
			while (parent!=null) {
				List<BigGBody> newBody = parent.next.developSummation(h, forQASimulator);
				parent.body = newBody;
				parent.next = null;
				parent = parent.previous;
			}
			this.developSummation(h, forQASimulator);
		}
	}

	private void applyForAlls(BigCH h) {
		if (!this.forAlls.isEmpty()) {
			BigGForAll forAll = this.forAlls.get(0);
			String variable = forAll.getVariable();
			int start = forAll.getStart(h);
			int end = forAll.getEnd(h);
			
			List<BigGBody> newBody = new ArrayList<>();
			for (int i=start; i<end; i++) {
				List<BigGBody> aux = this.expandBody(i, variable);
				newBody.addAll(aux);
			}
			this.body = newBody;
		}
	}

	private List<BigGBody> expandBody(int indexValue, String indexName) {
		List<BigGBody> result = new ArrayList<>();
		for (int i=0; i<this.body.size(); i++) {
			BigGBody originalBody = this.body.get(i);
			BigGBody newBody = new BigGBody();
			newBody.setSlacks(originalBody.hasSlacks());
			newBody.setOperator(originalBody.getOperator());
			newBody.setLeft(originalBody.getLeft().expand(indexValue, indexName));
			if (originalBody.getRight()!=null)
				newBody.setRight(originalBody.getRight().expand(indexValue, indexName));
			result.add(newBody);
		}
		return result;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(body, forAlls, from, to);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGIndexedSummation other = (BigGIndexedSummation) obj;
		return Objects.equals(body, other.body) && Objects.equals(forAlls, other.forAlls)
				&& Objects.equals(from, other.from) && Objects.equals(to, other.to);
	}
	
	@Override
	public BigGSummation parseToIsing() {
		for (int i = 0; i < this.body.size(); i++) {
			this.body.set(i, this.body.get(i).parseToIsing());
		}
		return this;
	}
	
	@Override
	public BigGSummation parseToQUBO() {
		for (int i = 0; i < this.body.size(); i++) {
			this.body.set(i, this.body.get(i).parseToQUBO());
		}
		return this;
	}
	
}
