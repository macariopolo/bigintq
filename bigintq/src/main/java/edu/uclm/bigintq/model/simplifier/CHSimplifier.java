package edu.uclm.bigintq.model.simplifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigDoubleValue;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigLambdadedExpression;
import edu.uclm.bigintq.model.BigProduct;
import edu.uclm.bigintq.model.BigSquare;
import edu.uclm.bigintq.model.BigSum;
import edu.uclm.bigintq.model.BigUseOfX;
import edu.uclm.bigintq.model.IBigSimpleExpression;

public class CHSimplifier {

	private BigCH ch;
	private Map<BigUseOfX, List<Double>> mapSquareX;
	private Map<TwoBigUsesOfX, List<Double>> mapProductX;
	private double scalars;
	private Matrix matrix;
	
	public CHSimplifier(BigCH ch) {
		this.ch = new BigCH();
		this.ch.setFunctionName(ch.getFunctionName());
		this.ch.setX(ch.getX());
		this.ch.setVariables(ch.getVariables());
		this.ch.setParameters(ch.getParameters());
		this.ch.setCombinationLength(ch.getCombinationLength());
		this.ch.setUsesOfX(ch.getUsesOfX());
		
		for(BigExpression expr : ch.getExpressions()) {
			BigLambdadedExpression ble = (BigLambdadedExpression) expr;
			BigSum sum = ble.develop(this.ch);
			if (sum!=null)
				this.ch.addExpression(sum);
		}

		
		this.mapProductX = new HashMap<>();
		this.mapSquareX = new HashMap<>();
		
		List<BigUseOfX> usesOfX = this.ch.getUsesOfX();
		usesOfX.sort(new BigExpressionSorter());
		int size = usesOfX.size();
		this.matrix = new Matrix(usesOfX, size);
		
		BigUseOfX uoxI, uoxJ;
		TwoBigUsesOfX pair;
		for (int i=0; i<size; i++) {
			uoxI = usesOfX.get(i);
			this.mapSquareX.put(uoxI, new ArrayList<>());
			for (int j=i+1; j<size; j++) {
				uoxJ = usesOfX.get(j);
				pair = new TwoBigUsesOfX(uoxI, uoxJ);
				this.mapProductX.put(pair, new ArrayList<>());
			}
		}		
	}

	public void simplify() {
		this.convertSimplesToSquares();
		
		this.scalarFactors();
		this.squareFactors();
		this.productFactors();
		
		this.sumSquareFactors();
		this.sumProductFactors();
		
		this.ch.getExpressions().clear();
		
		Iterator<BigUseOfX> squares = this.mapSquareX.keySet().iterator();
		while (squares.hasNext()) {
			BigUseOfX uox = squares.next();
			if (!this.mapSquareX.get(uox).isEmpty()) {
				double coefficient = this.mapSquareX.get(uox).get(0);
				BigSquare square = new BigSquare(ch);
				square.setExpr(uox);
				BigProduct product = new BigProduct(ch);
				product.setLeft(new BigDoubleValue(coefficient, ch));
				product.setRight(square);
				ch.addExpression(product);
				this.matrix.add(uox, coefficient);
			}
		}
		
		Iterator<TwoBigUsesOfX> products = this.mapProductX.keySet().iterator();
		while (products.hasNext()) {
			TwoBigUsesOfX uoxs = products.next();
			if (!this.mapProductX.get(uoxs).isEmpty()) {
				double coefficient = this.mapProductX.get(uoxs).get(0);
				if (coefficient==0.0)
					continue;
				BigProduct product = new BigProduct(ch);
				product.setLeft(new BigDoubleValue(coefficient, ch));
				product.setRight(new BigProduct(uoxs.getA(), uoxs.getB(), ch));
				ch.addExpression(product);
				this.matrix.add(uoxs, coefficient);
			}
		}
		
		if (this.scalars!=0) {
			BigDoubleValue value = new BigDoubleValue(this.scalars, ch);
			ch.addExpression(value);
		}
		
		BigExpressionSorter sorter = new BigExpressionSorter();
		this.ch.getExpressions().sort(sorter);
	}
	
	private void convertSimplesToSquares() {
		List<BigExpression> expressions = this.ch.getExpressions();
		for (BigExpression expr : expressions) {
			BigSum sum = (BigSum) expr;
			for (int i=0; i<sum.getExpressions().size(); i++) {
				BigExpression summand = sum.getExpressions().get(i);
				if (summand instanceof BigUseOfX) {
					BigSquare square = new BigSquare(this.ch);
					square.setExpr(summand);
					sum.getExpressions().set(i, square);
				}
				if (summand instanceof BigProduct) {
					BigProduct simple = (BigProduct) summand;
					IBigSimpleExpression left = (IBigSimpleExpression) simple.getLeft();
					IBigSimpleExpression right = (IBigSimpleExpression) simple.getRight();
					if (left instanceof BigUseOfX) {
						BigSquare square = new BigSquare(this.ch);
						square.setExpr(simple.getLeft());
						simple.setLeft(square);
					} 
					if (right instanceof BigUseOfX) {
						BigSquare square = new BigSquare(this.ch);
						square.setExpr(simple.getRight());
						simple.setRight(square);
					}	
				}
			}
		}
	}

	private void sumProductFactors() {
		Iterator<TwoBigUsesOfX> products = this.mapProductX.keySet().iterator();
		while (products.hasNext()) {
			TwoBigUsesOfX twoUox = products.next();
			List<Double> values = this.mapProductX.get(twoUox);
			double r = 0.0;
			for (Double value : values)
				r = r + value.doubleValue();
			values.clear();
			values.add(r);
		}
	}

	private void sumSquareFactors() {
		Iterator<BigUseOfX> squares = this.mapSquareX.keySet().iterator();
		while (squares.hasNext()) {
			BigUseOfX uox = squares.next();
			List<Double> values = this.mapSquareX.get(uox);
			double r = 0.0;
			for (Double value : values)
				r = r + value.doubleValue();
			values.clear();
			values.add(r);
		}
	}
	
	private void scalarFactors() {
		for (BigExpression expr : this.ch.getExpressions()) {
			BigSum sum = (BigSum) expr;
			for (BigExpression summand : sum.getExpressions()) {
				if (summand instanceof BigDoubleValue) {
					BigDoubleValue simple = (BigDoubleValue) summand;
					this.scalars = this.scalars + simple.getValue();
				}
			}
		}
	}

	private void squareFactors() {
		Iterator<BigUseOfX> squares = this.mapSquareX.keySet().iterator();
		List<BigExpression> expressions = this.ch.getExpressions();
		while (squares.hasNext()) {
			BigUseOfX uox = squares.next();
			for (BigExpression expr : expressions) {
				BigSum sum = (BigSum) expr;
				for (BigExpression summand : sum.getExpressions()) {
					if (summand instanceof BigSquare) {
						BigSquare simple = (BigSquare) summand;
						if (simple.contains(uox)) {
							this.mapSquareX.get(uox).add(1.0);
						}
					} else if (summand instanceof BigProduct) {
						BigProduct simple = (BigProduct) summand;
						if (simple.containsSquare(uox)) {
							this.mapSquareX.get(uox).add(simple.getCoefficientFor(uox));
						}
					}
				}
			}
		}
	}

	private void productFactors() {
		TwoBigUsesOfX[] products = this.mapProductX.keySet().toArray(new TwoBigUsesOfX[0]);
		List<BigExpression> expressions = this.ch.getExpressions();
		for (TwoBigUsesOfX twoUox : products) {
			for (BigExpression expr : expressions) {
				BigSum sum = (BigSum) expr;
				for (BigExpression summand : sum.getExpressions()) {
					if (summand instanceof BigProduct) {
						BigProduct simple = (BigProduct) summand;
						IBigSimpleExpression left = (IBigSimpleExpression) simple.getLeft();
						IBigSimpleExpression right = (IBigSimpleExpression) simple.getRight();
						if (left.contains(twoUox)) {
							double factor = right.getValue();
							this.mapProductX.get(twoUox).add(factor);
						} 
						if (right.contains(twoUox)) {
							double factor = left.getValue();
							this.mapProductX.get(twoUox).add(factor);
						}
					}
				}
			}
		}
	}
	
	public String getMathML() {
		return this.matrix.toMathML();
	}

	public JSONArray toJSON() {
		return this.matrix.toJSON();
	}
	
	public String toText() {
		return this.matrix.toText();
	}
}
