package edu.uclm.bigintq.model;

import java.util.Arrays;
import java.util.List;

import org.json.JSONObject;

public class BigUseOfS extends BigExpression implements IBigSimpleExpression {
	
	private int[] indexes;
	
	public BigUseOfS(BigCH h) {
		super(h);
	}

	public BigUseOfS(JSONObject jso, BigCH h) {
		this(h);
		String[] tokens = jso.getString("value").split(",");
		this.indexes = new int[tokens.length];
		for (int i=0; i<tokens.length; i++)
			this.indexes[i] = Integer.parseInt(tokens[i].trim());
	}
	
	@Override
	public double getValue() {
		return 0;
	}

	@Override
	public double getValue(List<Integer> combination) {
		return this.getH().getX().getValue(combination, this.indexes);
	}
	
	public void setIndexes(int[] indexes) {
		this.indexes = indexes;
		this.getH().addUseOfS(this);
	}
	
	public void setIndexes(List<Integer> indexes) {
		this.indexes = new int[indexes.size()];
		for (int i=0; i<indexes.size(); i++)
			this.indexes[i] = indexes.get(i);
		this.getH().addUseOfS(this);
	}
	
	public int[] getIndexes() {
		return indexes;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("s");
		for (int i=0; i<this.indexes.length; i++)
			sb.append("_" + this.indexes[i]);
		return sb.toString();
	}
	
	@Override
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "UseOfS");
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<this.indexes.length-1; i++)
			sb = sb.append("" + this.indexes[i] + ",");
		sb = sb.append(this.indexes[this.indexes.length-1]);
		jso.put("value", sb.toString());
		return jso;
	}

	@Override
	protected String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<msub>");
		sb.append("<mi>s</mi>");
		sb.append("<mrow>");
		for (int i=0; i<indexes.length-1; i++)
			sb.append("<mn>" + (int) this.indexes[i] + "</mn><mo>,</mo>");
		sb.append("<mn>" + (int) this.indexes[this.indexes.length-1] + "</mn>");
		sb.append("</mrow>");
		sb.append("</msub>");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(indexes);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigUseOfS other = (BigUseOfS) obj;
		return Arrays.equals(indexes, other.indexes);
	}
	
	@Override
	public BigExpression parseToIsing() {
		return this;
	}

	@Override
	public BigExpression parseToQUBO() {
		return BigParser.parseIsingToQUBO(this);
	}

	@Override
	public List<BigUseOfX> loadUsesOfX() {
		return null;
	}

	@Override
	protected BigExpression divideInto(double value) {
		return this;
	}

	@Override
	protected BigExpression square() {
		BigSquare result = new BigSquare(getH());
		result.setExpr(this);
		return result;
	}
	
}
