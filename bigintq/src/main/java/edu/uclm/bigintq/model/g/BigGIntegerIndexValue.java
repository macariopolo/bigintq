package edu.uclm.bigintq.model.g;

import java.util.Objects;

import edu.uclm.bigintq.model.BigCH;

public class BigGIntegerIndexValue extends BigGIndexValue {

	private int index;

	public BigGIntegerIndexValue(int index) {
		this.index = index;
	}

	@Override
	protected Integer getValue(BigCH h) {
		return this.index;
	}

	@Override
	public String toString() {
		return "" + this.index;
	}

	@Override
	protected String toMathML() {
		return "<mn>" + this.index + "</mn>";
	}

	@Override
	public int hashCode() {
		return Objects.hash(index);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGIntegerIndexValue other = (BigGIntegerIndexValue) obj;
		return index == other.index;
	}

	
}
