package edu.uclm.bigintq.model;

import java.util.List;
import java.util.Objects;

import org.json.JSONObject;

import edu.uclm.bigintq.model.simplifier.TwoBigUsesOfX;

public class BigSquare extends BigLambdadedExpression implements IBigSimpleExpression {
	private BigExpression expr;

	public BigSquare(BigCH h) {
		super(h);
	}
	
	public BigSquare(BigExpression expr, BigCH h) {
		this(h);
		this.expr = expr;
	}
	
	public BigSquare(JSONObject jso, BigCH h) {
		this(h);
		this.expr = BigExpression.build(jso.getJSONObject("value"), h);
	}
	
	@Override
	protected BigExpression square() {
		BigSquare result = new BigSquare(getH());
		result.expr = this.expr.square();
		return result;
	}
	
	@Override
	public boolean isEmpty() {
		return this.expr==null;
	}
	
	@Override
	public double getValue() {
		IBigSimpleExpression expr = (IBigSimpleExpression) this.expr;
		double r = expr.getValue();
		return r*r;
	}
	
	@Override
	public boolean contains(BigUseOfX uox) {
		IBigSimpleExpression expr = (IBigSimpleExpression) this.expr;
		return expr.contains(uox);
	}
	
	@Override
	public List<BigUseOfX> loadUsesOfX() {
		this.usesOfX.clear();
		this.usesOfX = this.expr.loadUsesOfX();
		return this.usesOfX;
	}
	
	@Override
	public boolean contains(TwoBigUsesOfX uoxs) {
		IBigSimpleExpression expr = (IBigSimpleExpression) this.expr;
		return expr.contains(uoxs);
	}

	@Override
	public BigSum develop(BigCH h) {
		if (this.getLambda()==null || this.getLambda()==0)
			return null;
		
		BigSum sum = null;
		BigSubtraction subtraction = (BigSubtraction) this.expr;
		
		sum = subtraction.toSum();
		sum = (BigSum) sum.square();
		
		if (this.getLambda()!=1) {
			for (int i=0; i<sum.getExpressions().size(); i++) {
				BigExpression expr = sum.getExpressions().get(i);
				BigProduct product = new BigProduct(h);
				product.setLeft(this.getLambdaAsBigDoubleValue(h));
				product.setRight(expr);
				sum.getExpressions().set(i, product.simplify());			
			}
			// sum.simplifyProducts();
		} 
		
		return sum;

		/*BigSum result = new BigSum(h);
		BigExpression exprI;
		BigSquare square;
		int size = sum.getExpressions().size();
		for (int i=0; i<size; i++) {
			exprI = sum.getExpressions().get(i);
			if (exprI instanceof BigDoubleValue) {
				BigDoubleValue value = (BigDoubleValue) exprI;
				BigDoubleValue newValue = new BigDoubleValue(value.getValue()*value.getValue(), h);
				result.add(newValue);
			} else {
				square = new BigSquare(h);
				square.setExpr(exprI);
				result.add(exprI.square());
			}
		}
		
		BigExpression exprJ;
		BigProduct twice, exprIJ;
		for (int i=0; i<size; i++) {
			exprI = sum.getExpressions().get(i);
			for (int j=i+1; j<size; j++) {
				exprJ = sum.getExpressions().get(j);
				exprIJ = new BigProduct(h);
				exprIJ.setLeft(exprI);
				exprIJ.setRight(exprJ);
				exprIJ.simplify();
				twice = new BigProduct(h);
				twice.setLeft(new BigDoubleValue(2.0, h));
				twice.setRight(exprIJ);
				result.add(twice.simplify());
			}
		}
		
		if (this.getLambda()!=1) {
			for (int i=0; i<result.getExpressions().size(); i++) {
				BigExpression expr = result.getExpressions().get(i);
				BigProduct product = new BigProduct(h);
				product.setLeft(this.getLambdaAsBigDoubleValue(h));
				product.setRight(expr);
				result.getExpressions().set(i, product.simplify());			
			}
			result.simplifyProducts();
		} 
		
		return result;*/
	}

	@Override
	public double getValue(List<Integer> combination) {
		Double value = expr.getValue(combination);
		if (value==0)
			return 0;
		value = value * value;
		if (this.lambda!=null)
			value = this.lambda * value;
		return value;
	}

	public void setExpr(BigExpression expr) {
		if (expr instanceof BigDoubleValue) {
			BigDoubleValue value = (BigDoubleValue) expr;
			double r = value.getValue();
			this.expr = new BigDoubleValue(r*r, getH());
		} else if (expr instanceof BigParameter) {
			BigParameter value = (BigParameter) expr;
			double r = value.getValue();
			this.expr = new BigDoubleValue(r*r, getH());
		} else { 
			this.expr = expr;
		}
	}
	
	@Override
	protected BigExpression divideInto(double value) {
		this.expr = this.expr.divideInto(value);
		return this;
	}

	@Override
	public String toString() {
		String r = "(" + this.expr.toString() + ")^2";
		if (this.getLambda()!=null)
			return this.getLambda() + "*" + r;
		return r;
	}
	
	@Override
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "Square");
		jso.put("value", this.expr.toJSON());
		jso.put("lambda", this.lambda);
		return jso;
	}

	@Override
	protected String toMathML() {
		StringBuilder sb = new StringBuilder();	
		if (this.lambda!=null && lambda.doubleValue()!=1) 
			sb.append("<mn>" + this.lambda + "</mn><mo>·</mo>");
		sb.append("<msup>");
		sb.append(this.expr.toMathML());
		sb.append("<mn>2</mn>");
		sb.append("</msup>");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(expr);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigSquare other = (BigSquare) obj;
		return Objects.equals(expr, other.expr);
	}
	
	public BigExpression getExpr() {
		return expr;
	}
	
	@Override
	public BigExpression parseToIsing() {
		return expr.parseToIsing();
	}
	
	@Override
	public BigExpression parseToQUBO() {
		return expr.parseToQUBO();
	}
}
