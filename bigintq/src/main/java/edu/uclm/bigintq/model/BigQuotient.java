package edu.uclm.bigintq.model;

import java.util.List;
import java.util.Objects;

import org.json.JSONObject;

public class BigQuotient extends BigExpression {

	private BigExpression num;
	private BigExpression denom;
	
	public BigQuotient(BigCH h) {
		super(h);
	}

	public BigQuotient(JSONObject jso, BigCH h) {
		this(h);
		this.num = BigExpression.build(jso.getJSONObject("num"), h);
		this.denom = BigExpression.build(jso.getJSONObject("denom"), h);
	}
	
	@Override
	protected BigExpression square() {
		BigQuotient result = new BigQuotient(getH());
		result.setNum(this.num.square());
		result.setDenom(this.num.square());
		return result;
	}
	


	@Override
	public double getValue(List<Integer> combination) {
		return this.num.getValue(combination)/this.denom.getValue(combination);
	}

	@Override
	public String toString() {
		return this.num.toString() + "/" + this.denom.toString();
	}

	@Override
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "Quotient").
			put("num", this.num.toJSON()).
			put("denom", this.denom.toJSON());
		return jso;
	}

	@Override
	protected String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.num.toMathML());
		sb.append("<mo>/</mo>");
		sb.append(this.denom.toMathML());
		return sb.toString();
	}

	@Override
	public List<BigUseOfX> loadUsesOfX() {
		this.usesOfX.clear();
		this.usesOfX.addAll(this.num.loadUsesOfX());
		this.usesOfX.addAll(this.denom.loadUsesOfX());
		return this.usesOfX;
	}

	@Override
	protected BigExpression divideInto(double value) {
		BigQuotient result = new BigQuotient(this.getH());
		result.setNum(this.num);
		result.setDenom(new BigDoubleValue(value, this.getH()));
		return result;
	}

	public void setNum(BigExpression num) {
		this.num = num;
	}

	public void setDenom(BigExpression denom) {
		this.denom = denom;
	}

	@Override
	public int hashCode() {
		return Objects.hash(denom, num);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigQuotient other = (BigQuotient) obj;
		return Objects.equals(denom, other.denom) && Objects.equals(num, other.num);
	}
	
	@Override
	public BigExpression parseToIsing() {
		this.num = num.parseToIsing();
		this.denom = denom.parseToIsing();
		return this;
	}

	@Override
	public BigExpression parseToQUBO() {
		this.num = num.parseToQUBO();
		this.denom = denom.parseToQUBO();
		return this;
	}

}
