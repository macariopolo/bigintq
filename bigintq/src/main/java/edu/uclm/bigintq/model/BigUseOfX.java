package edu.uclm.bigintq.model;

import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class BigUseOfX extends BigExpression implements IBigSimpleExpression {
	
	private int[] indexes;
	private boolean slack;
	
	public BigUseOfX(BigCH h) {
		super(h);
	}

	public BigUseOfX(JSONObject jso, BigCH h) {
		this(h);
		String[] tokens = jso.getString("value").split(",");
		this.indexes = new int[tokens.length];
		for (int i=0; i<tokens.length; i++)
			this.indexes[i] = Integer.parseInt(tokens[i].trim());
	}
	
	@Override
	protected BigExpression square() {
		BigSquare result = new BigSquare(getH());
		result.setExpr(this);
		return result;
	}
	
	@Override
	public boolean contains(BigUseOfX uox) {
		return this.equals(uox);
	}
	
	@Override
	public double getValue() {
		return 0;
	}
	
	@Override
	protected BigExpression divideInto(double value) {
		return this;
	}
	
	@Override
	public List<BigUseOfX> loadUsesOfX() {
		this.usesOfX.add(this);
		return this.usesOfX;
	}

	@Override
	public double getValue(List<Integer> combination) {
		BigX x = this.getH().getX();
		return x.getValue(combination, this.indexes);
	}
	
	public void setIndexes(int... indexes) {
		this.indexes = indexes;
		if (this.getH()!=null)
			this.getH().addUseOfX(this);
	}
	
	public void setIndexes(List<Integer> indexes) {
		this.indexes = new int[indexes.size()];
		for (int i=0; i<indexes.size(); i++)
			this.indexes[i] = indexes.get(i);
		if (this.getH()!=null)
			this.getH().addUseOfX(this);
	}
	
	public void setIndexes(JSONArray indexes) {
		this.indexes = new int[indexes.length()];
		for (int i=0; i<indexes.length(); i++)
			this.indexes[i] = indexes.getInt(i);
		if (this.getH()!=null)
			this.getH().addUseOfX(this);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder((this.slack ? "s" : "x"));
		for (int i=0; i<this.indexes.length; i++)
			sb.append("_" + this.indexes[i]);
		return sb.toString();
	}
	
	@Override
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "UseOfX");
		jso.put("slack", this.slack);
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<this.indexes.length-1; i++)
			sb = sb.append("" + this.indexes[i] + ",");
		sb = sb.append(this.indexes[this.indexes.length-1]);
		jso.put("value", sb.toString());
		return jso;
	}

	@Override
	protected String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<msub>");
		sb.append("<mi>" + (this.slack ? "s" : "x") + "</mi>");
		sb.append("<mrow>");
		for (int i=0; i<indexes.length-1; i++)
			sb.append("<mn>" + (int) this.indexes[i] + "</mn><mo>,</mo>");
		sb.append("<mn>" + (int) this.indexes[this.indexes.length-1] + "</mn>");
		sb.append("</mrow>");
		sb.append("</msub>");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(indexes);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigUseOfX other = (BigUseOfX) obj;
		return Arrays.equals(indexes, other.indexes);
	}
	
	public int[] getIndexes() {
		return indexes;
	}

	public void setSlack(boolean slack) {
		this.slack = slack;
	}
	
	@Override
	public BigExpression parseToIsing() {
		return BigParser.parseQUBOToIsing(this);
	}
	
	@Override
	public BigExpression parseToQUBO() {
		return this;
	}
}
