package edu.uclm.bigintq.model;

import java.util.List;
import java.util.Objects;

import org.json.JSONObject;

public class BigParameter extends BigExpression implements IBigSimpleExpression {

	private String name;
	private double value;
	private int slacks;

	public BigParameter(BigCH h) {
		super(h);
	}
	
	public BigParameter(String name, double value, BigCH h) {
		this(h);
		this.name = name;
		this.value = value;
	}
	
	@Override
	protected BigExpression square() {
		BigDoubleValue result = new BigDoubleValue(value*value, getH());
		return result;
	}

	public BigParameter(JSONObject jso, BigCH h) {
		this(jso.getString("name"), jso.getDouble("value"), h);
	}
	
	@Override
	protected BigExpression divideInto(double value) {
		BigDoubleValue result = new BigDoubleValue(this.value/value, getH());
		return result;
	}
	
	@Override
	public double getValue(List<Integer> combination) {
		return this.value;
	}
	
	@Override
	public List<BigUseOfX> loadUsesOfX() {
		return this.usesOfX;
	}

	@Override
	public double getValue() {
		return this.value;
	}

	@Override
	public String toString() {
		return "" + this.value;
	}

	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("name", this.name);
		jso.put("type", "Parameter");
		jso.put("value", this.value);
		return jso;
	}

	@Override
	protected String toMathML() {
		return "<mn>" + this.value + "</mn>";
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigParameter other = (BigParameter) obj;
		return Objects.equals(name, other.name)
				&& Double.doubleToLongBits(value) == Double.doubleToLongBits(other.value);
	}

	public void increaseSlacks(double increment) {
		this.slacks = this.slacks + (int) increment;
	}
	
	public int getSlacks() {
		return slacks;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public BigExpression parseToIsing() {
		return this;
	}
	
	@Override
	public BigExpression parseToQUBO() {
		return this;
 	}
}
