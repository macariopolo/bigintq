package edu.uclm.bigintq.model.g;

import java.util.Objects;

import org.json.JSONException;
import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigQuotient;

public class BigGQuotient extends BigGExpr {
	private BigGExpr num, denom;
	
	public BigGQuotient() {
	}
	
	public BigGQuotient(JSONObject jso, BigGH h) throws JSONException, Exception {
		this.num = BigGExpr.build(jso.getJSONObject("num"), h);
		this.denom = BigGExpr.build(jso.getJSONObject("denom"), h);
	}
	
	@Override
	protected BigExpression getBigExpression(BigCH h, String indexName, int indexValue) throws Exception {
		BigQuotient quotient = new BigQuotient(h);
		BigExpression num = this.num.getBigExpression(h, indexName, indexValue);
		if (num==null)
			return null;
		BigExpression denom = this.denom.getBigExpression(h, indexName, indexValue);
		if (denom==null)
			return null;
		quotient.setNum(num);
		quotient.setDenom(denom);
		return quotient;
	}

	@Override
	public double getValue(BigCH h) {
		return this.num.getValue(h)/this.denom.getValue(h);
	}

	@Override
	public String toString() {
		return this.num.toString() + "/" + this.denom.toString();
	}
	
	@Override
	protected JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "Quotient");
		jso.put("num", this.num.toJSON());
		jso.put("denom", this.denom.toJSON());
		return jso;
	}

	@Override
	public String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.num.toMathML());
		sb.append("<mo>/</mo>");
		sb.append(this.denom.toMathML());
		return sb.toString();
	}

	@Override
	protected BigGExpr expand(int indexValue, String indexName) {
		BigGQuotient result = new BigGQuotient();
		result.setNum(this.num.expand(indexValue, indexName));
		result.setDenom(this.denom.expand(indexValue, indexName));
		return result;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(denom, num);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGQuotient other = (BigGQuotient) obj;
		return Objects.equals(denom, other.denom) && Objects.equals(num, other.num);
	}

	public void setNum(BigGExpr num) {
		this.num = num;
	}
	
	public void setDenom(BigGExpr denom) {
		this.denom = denom;
	}
	
	@Override
	protected boolean uses(BigGVariable bgv) {
		return this.num.uses(bgv) || this.denom.uses(bgv);
	}
	
	@Override
	public BigGExpr parseToIsing() {
		this.num = num.parseToIsing();
		this.denom = denom.parseToIsing();
		return this;
	}

	@Override
	public BigGExpr parseToQUBO() {
		this.num = num.parseToQUBO();
		this.denom = denom.parseToQUBO();
		return this;
	}
}
