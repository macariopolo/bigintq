package edu.uclm.bigintq.model;

import edu.uclm.bigintq.model.simplifier.TwoBigUsesOfX;

public interface IBigSimpleExpression {

	double getValue();
	
	default boolean contains(BigUseOfX uox) {
		return false;
	}
	
	default boolean contains(TwoBigUsesOfX uoxs) {
		return false;
	}

	default boolean contains(BigUseOfS uos) {
		return false;
	}
}
