package edu.uclm.bigintq.model.g;

public interface IBigGExpandibleExpr {
	BigGExpr expand(int indexValue, String indexName);
}
