package edu.uclm.bigintq.model.g;

import edu.uclm.bigintq.model.BigCH;

public abstract class BigGIndexValue {

	protected abstract Integer getValue(BigCH h);
	
	@Override
	public abstract String toString();

	protected abstract String toMathML();

}
