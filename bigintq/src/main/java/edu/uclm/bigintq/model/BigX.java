package edu.uclm.bigintq.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class BigX {
	private List<Integer> cardinals;
	private List<Integer> slackPositions;
	private int[] prods;
	private BigInteger numberOfCombinations;
	private Map<List<Integer>, Double> fixedValues;

	public BigX() {
		this.cardinals = new ArrayList<>();
		this.slackPositions = new ArrayList<>();
		this.fixedValues = new HashMap<>();
	}

	public void setXValue(double value, JSONArray jsaIndexes) {
		List<Integer> indexes = new ArrayList<>();
		for (int i=0; i<jsaIndexes.length(); i++)
			indexes.add(jsaIndexes.getInt(i));
		this.fixedValues.put(indexes, value);
	}
	
	public void setPrefixedValues(JSONArray jsaPrefixedX) {
		JSONObject jso;
		for (int i=0; i<jsaPrefixedX.length(); i++) {
			jso = jsaPrefixedX.getJSONObject(i);
			this.setXValue(jso.getDouble("value"), jso.getJSONArray("coordinates"));
		}		
	}
	
	public int setCardinals(String xCardinals) {
		this.cardinals.clear();
		String[] tokens = xCardinals.split(",");
		int cardinal;
		this.numberOfCombinations = BigInteger.ONE;
		int combinationLength = 1;
		for (String token : tokens) {
			cardinal = Integer.parseInt(token.trim());
			combinationLength = combinationLength * cardinal;
			this.cardinals.add(cardinal);
			this.numberOfCombinations = this.numberOfCombinations.multiply(BigInteger.valueOf(cardinal));
		}
		this.numberOfCombinations = this.pow(BigInteger.valueOf(2), this.numberOfCombinations);
		
		this.prods = new int[tokens.length];
		for (int i=1; i<tokens.length; i++) {
			this.prods[i-1] = 1;
			for (int j=i; j<this.cardinals.size(); j++)
				this.prods[i-1] = this.prods[i-1] * this.cardinals.get(j);
		}
		return combinationLength;
	}

	public int increaseCardinals() {
		int cardinal;
		for (int i=0; i<this.cardinals.size(); i++) {
			cardinal = this.cardinals.get(i);
			this.cardinals.set(i, cardinal+1);
			this.slackPositions.add(cardinal+1);
		}
		
		this.numberOfCombinations = BigInteger.ONE;
		int combinationLength = 1;
		for (int i=0; i<this.cardinals.size(); i++) {
			cardinal = this.getCardinals().get(i);
			combinationLength = combinationLength * cardinal;
			this.numberOfCombinations = this.numberOfCombinations.max(BigInteger.valueOf(cardinal));
		}
		this.numberOfCombinations = this.pow(BigInteger.valueOf(2), this.numberOfCombinations);
		
		this.prods = new int[this.cardinals.size()];
		for (int i=1; i<this.cardinals.size(); i++) {
			this.prods[i-1] = 1;
			for (int j=i; j<this.cardinals.size(); j++)
				this.prods[i-1] = this.prods[i-1] * this.cardinals.get(j);
		}
		return combinationLength;
	}
	
	public int getValue(List<Integer> combination, int[] indexes) {
		int pos = 0;
		for (int i=0; i<this.prods.length-1; i++) 
			pos = pos + indexes[i] * this.prods[i];
		pos = pos + indexes[indexes.length-1];
		return combination.get(pos);
	}
	
	public List<List<Integer>> getListOfCoordinates() {
		int size = 1;
		for (int i=0; i<this.cardinals.size(); i++)
			size = size * this.cardinals.get(i);
		List<List<Integer>> result = new ArrayList<>();
		for (int i=0; i<size; i++)
			result.add(getCoordinates(i));
		return result;
	}
	
	public List<Integer> getCoordinates(int index) {
		List<Integer> result = new ArrayList<>();
		int quotient = index;
		int remainder = quotient;
		for (int i=0; i<this.prods.length-1; i++) {
			remainder = quotient % this.prods[i];
			quotient = quotient / this.prods[i];
			result.add(quotient);
		}
		result.add(remainder);
		return result;
	}

	public double getValue(List<Integer> indexes) {
		// TODO Auto-generated method stub
		return 0;
	}

	public BigInteger getNumberOfCombinations() {
		return numberOfCombinations;
	}

	private BigInteger pow(BigInteger base, BigInteger exponent) {
		BigInteger result = BigInteger.ONE;
		while (exponent.signum() > 0) {
			if (exponent.testBit(0)) result = result.multiply(base);
			base = base.multiply(base);
			exponent = exponent.shiftRight(1);
		}
		return result;
	}

	public String getXCardinalsAsString() {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<this.cardinals.size()-1; i++)
			sb = sb.append(this.cardinals.get(i) + ",");
		sb = sb.append(this.cardinals.get(this.cardinals.size()-1).toString());
		return sb.toString();
	}
	
	public List<Integer> getCardinals() {
		return cardinals;
	}

	public Double getPrefixedValue(List<Integer> indexes) {
		Double uox = this.fixedValues.get(indexes);
		return uox;
	}

	public JSONArray getPrefixedValues() {
		Iterator<List<Integer>> keys = this.fixedValues.keySet().iterator();
		JSONArray prefixedValues = new JSONArray();
		List<Integer> key;
		while (keys.hasNext()) {
			key = keys.next();
			prefixedValues.put(new JSONObject().
				put("coordinates", key).
				put("value", this.fixedValues.get(key)));
		}
		return prefixedValues;
	}

}
