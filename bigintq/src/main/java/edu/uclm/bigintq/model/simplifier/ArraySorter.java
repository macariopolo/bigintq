package edu.uclm.bigintq.model.simplifier;

import java.util.Comparator;

public class ArraySorter implements Comparator<int[]> {

	@Override
	public int compare(int[] o1, int[] o2) {
		int i=0;
		while (i<o1.length) {
			if (o1[i]!=o2[i])
				return o1[i]-o2[i];
			i++;
		}
		return 0;
	}

	
}
