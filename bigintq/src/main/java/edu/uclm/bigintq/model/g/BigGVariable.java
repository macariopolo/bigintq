package edu.uclm.bigintq.model.g;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.json.JSONArray;
import org.json.JSONObject;

public class BigGVariable {

	private String name;
	private List<BigGParameter> cardinals; 

	public BigGVariable(String name) {
		this.name = name;
		this.cardinals = new ArrayList<>();
	}
	
	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGVariable other = (BigGVariable) obj;
		return Objects.equals(name, other.name);
	}

	public void setCardinals(BigGH h, String cardinals) throws Exception {
		String[] tokens = cardinals.split(",");
		String token;
		BigGParameter par;
		for (int i=0; i<tokens.length; i++) {
			token = tokens[i].trim();
			par = h.findParameter(token);
			if (par==null)
				throw new Exception("Parameter " + token + " not found in the cardinals of the variable " + this.name);
			this.cardinals.add(par);
		}
	}
	
	public void setCardinals(BigGH h, JSONArray cardinals) throws Exception {
		BigGParameter par;
		String token;
		for (int i=0; i<cardinals.length(); i++) {
			token = cardinals.getString(i).trim();
			par = h.findParameter(token);
			if (par==null)
				throw new Exception("Parameter " + token + " not found in the cardinals of the variable " + this.name);
			this.cardinals.add(par);
		}
	}

	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("name", this.name);
		StringBuilder sb= new StringBuilder();
		for (int i=0; i<this.cardinals.size(); i++)
			sb.append(this.cardinals.get(i).getName() + ",");
		String r = sb.toString();
		if (r.endsWith(","))
			r = r.substring(0, r.length()-1);
		jso.put("cardinals", r);
		return jso;
	}

	public List<BigGParameter> getCardinals() {
		return cardinals;
	}

	public String generateRandomValues(int min, int max, JSONArray jsaParameters) {
		int n = 1;
		for (int i=0; i<this.cardinals.size(); i++) {
			BigGParameter parameter = this.cardinals.get(i);
			n = n * this.findValue(parameter.getName(), jsaParameters);
		}
		StringBuilder sb = new StringBuilder();
		SecureRandom dado = new SecureRandom();
		for (int i=0; i<n; i++) {
			sb.append(min + dado.nextInt(max+1) + ","); 
		}
		String r = sb.toString();
		r = r.substring(0, r.length()-1);
		return r;
	}

	private int findValue(String name, JSONArray jsaParameters) {
		for (int i=0; i<jsaParameters.length(); i++)
			if (jsaParameters.getJSONObject(i).getString("name").equals(name))
				return jsaParameters.getJSONObject(i).getInt("value");
		return 1;
	}

	public boolean isInInequality() {
		// TODO Auto-generated method stub
		return false;
	}
}
