package edu.uclm.bigintq.model.g;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigLambdadedExpression;
import edu.uclm.bigintq.model.BigParameter;
import edu.uclm.bigintq.model.BigVariable;

public class BigGH {

	private String problemName;
	private Map<String, BigGParameter> parameters;
	private Map<String, BigGVariable> variables;
	private List<BigGSummation> expressions;
	private List<BigGParameter> xCardinals;
	
	public BigGH() {
		this.parameters = new HashMap<>();
		this.variables = new HashMap<>();
		this.expressions = new ArrayList<>();
		this.xCardinals = new ArrayList<>();
	}

	public BigGH(JSONObject jso) throws JSONException, Exception {
		this();
		this.problemName = jso.optString("problemName");
		this.setParameters(jso.optString("parameters"));
		this.setVariables(jso.optJSONArray("variables"));
		JSONArray jsa = jso.optJSONArray("xCardinals");
		if (jsa!=null)
			this.setXCardinals(jsa);
		else
			this.setXCardinals(jso.optString("xCardinals"));
		this.loadExpressions(jso.getJSONArray("expressions"));
	}
	
	public void clear() {
		this.problemName = null;
		this.parameters.clear();
		this.variables.clear();
		this.expressions.clear();
	}

	public List<String> toMathML() {
		List<String> result = new ArrayList<>();
		for (BigGSummation expr : this.expressions) {
			StringBuilder sb = new StringBuilder();
			sb.append("<math xmlns='http://www.w3.org/1998/Math/MathML' display='block'>");
			sb.append(expr.toMathML());
			sb.append("</math>");
			result.add(sb.toString());
		}
		return result;
	}

	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("problemName", this.problemName);
		jso.put("variables", this.variablesToJSON());
		jso.put("parameters", this.parametersToJSON());
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<this.xCardinals.size(); i++)
			sb.append(this.xCardinals.get(i).getName() + ",");
		String r = sb.toString();
		if (r.endsWith(","))
			r = r.substring(0, r.length()-1);
		jso.put("xCardinals", r);
		jso.put("expressions", this.expressionsToJSON());
		return jso;
	}

	private void loadExpressions(JSONArray jsa) throws Exception {
		JSONObject jso;
		BigGSummation expr;
		for (int i=0; i<jsa.length(); i++) {
			jso = jsa.getJSONObject(i);
			expr = BigGSummation.build(jso, null, this);
			this.expressions.add(expr);
		}
	}
	
	private JSONArray expressionsToJSON() {
		JSONArray jsa = new JSONArray();
		for (BigGSummation expr : this.expressions)
			jsa.put(expr.toJSON());
		return jsa;
	}

	public void setParameters(String parameters) throws Exception {
		this.parameters.clear();
		String[] tokens = parameters.split(",");
		for (String token : tokens) {
			token = token.trim();
			if (token.length()==0)
				throw new Exception("Expected at least a parameter's name in: " + parameters);
			if (token.equalsIgnoreCase("x"))
				throw new Exception("x is a reseved name for the dependent variable");
			this.addParameter(token);
		}
	}
	
	private String parametersToJSON() {
		StringBuilder sb = new StringBuilder();
		if (this.parameters.isEmpty()) 
			return null;
		Collection<BigGParameter> parameters = this.parameters.values();
		for (BigGParameter parameter : parameters)
			sb.append(parameter.getName() + ",");
		String r = sb.toString();
		r = r.substring(0, r.length()-1);
		return r;
	}
	
	public void setVariables(JSONArray jsaVariables) throws Exception {
		JSONObject jso;
		String name;
		this.variables.clear();
		if (jsaVariables==null)
			return;
		for (int i=0; i<jsaVariables.length(); i++) {
			jso = jsaVariables.getJSONObject(i);
			name = jso.getString("name");
			BigGVariable variable = new BigGVariable(name);
			JSONArray cardinals = jso.optJSONArray("cardinals");
			if (cardinals!=null)
				variable.setCardinals(this, jso.optJSONArray("cardinals"));
			String sCardinals = jso.optString("cardinals").trim();
			if (sCardinals.length()==0)
				throw new Exception("Expected cardinals of the variable " + name);
			variable.setCardinals(this, sCardinals);
			this.addVariable(variable);
		}
	}
	
	public void setXCardinals(JSONArray jsaXCardinals) {
		String name;
		BigGParameter par;
		this.xCardinals.clear();
		for (int i=0; i<jsaXCardinals.length(); i++) {
			name = jsaXCardinals.getString(i);
			par = this.findParameter(name);
			this.xCardinals.add(par);
		}
	}

	private JSONArray variablesToJSON() {
		if (this.variables.isEmpty()) 
			return null;
		Collection<BigGVariable> variables = this.variables.values();
		JSONArray jsa = new JSONArray();
		for (BigGVariable variable : variables)
			jsa.put(variable.toJSON());
		return jsa;
	}

	public BigGParameter findParameter(String parameterName) {
		return this.parameters.get(parameterName);
	}
	
	public BigGVariable findVariable(String variableName) {
		return this.variables.get(variableName);
	}
	
	public BigGParameter addParameter(String parameterName) {
		parameterName = parameterName.trim();
		BigGParameter parameter = new BigGParameter(parameterName);
		this.parameters.put(parameterName, parameter);
		return parameter;
	}
	
	public BigGVariable addVariable(BigGVariable variable) {
		String name = variable.getName();
		this.variables.put(name, variable);
		return variable;
	}

	public BigCH instantiate(JSONObject jsoInstance, boolean forQASimulator) throws Exception {
		BigCH ch = new BigCH();
				
		JSONObject jso;
		JSONArray parameters = jsoInstance.getJSONArray("parameters");
		double value;
		String s;
		for (int i=0; i<parameters.length(); i++) {
			jso = parameters.getJSONObject(i);
			s = jso.getString("name");
			value = jso.optDouble("value");
			if (Double.isNaN(value))
				throw new Exception("Missing the value of parameter " + s);
			ch.addParameter(s, jso.getDouble("value"));
		}
		
		int totalSlacks = 0;
		for (int i=0; i<this.expressions.size(); i++) {
			BigGSummation summation = this.expressions.get(i);
			if (summation.proceedsFromInequality()) {
				double rightValue = summation.getOldRight().getValue(ch);
				rightValue = Math.log10(rightValue) / Math.log10(2.0);
				rightValue = Math.ceil(rightValue);
				totalSlacks = totalSlacks + (int) rightValue;
				summation.setStartSlack(totalSlacks);
				for (BigGParameter bigParameter : this.xCardinals) {
					BigParameter parameter = ch.findParameter(bigParameter.getName());
					parameter.increaseSlacks(rightValue);
				}
			}
		}
		
		ch.instantiateXCardinals(this.xCardinals);
		
		JSONArray jsaPrefixedX = jsoInstance.optJSONArray("prefixedX");
		JSONArray xCoordinates;
		if (jsaPrefixedX!=null && jsaPrefixedX.length()>0) {
			JSONObject jsoPrefixedX;
			for (int i=0; i<jsaPrefixedX.length(); i++) {
				jsoPrefixedX = jsaPrefixedX.getJSONObject(i);
				if (jsoPrefixedX.has("value")) {
					value = jsoPrefixedX.optDouble("value");
					if (!Double.isNaN(value)) {
						xCoordinates = jsoPrefixedX.getJSONArray("coordinates");
						ch.getX().setXValue(value, xCoordinates);
					}
				}
			}
		}
		
		JSONArray variables = jsoInstance.getJSONArray("variables");
		for (int i=0; i<variables.length(); i++) {
			jso = variables.getJSONObject(i);
			BigVariable variable = this.instantiateVariable(ch, jso);
			ch.addVariable(variable);
		}
				
		JSONArray lambdas = jsoInstance.getJSONArray("lambdas");
		double lambda;
		int[] slackCounter = {0};
		for (int i=0; i<this.expressions.size(); i++) {
			BigGSummation summation = this.expressions.get(i);
			List<BigExpression> expansions = summation.expand(ch, forQASimulator, slackCounter);
			for (BigExpression expansion : expansions) {
				BigLambdadedExpression ble = (BigLambdadedExpression) expansion;
				if (ble.isEmpty())
					continue;
				lambda = lambdas.getJSONObject(i).getDouble("value");
				ble.setLambda(lambda);
				ch.addExpression(expansion);
				expansion.loadUsesOfX();
			}
		}
		return ch;
	}
	
	public BigVariable instantiateVariable(BigCH ch, JSONObject jso) throws Exception {
		String name = jso.getString("name");
		BigGVariable bgv = this.findVariable(name);
		List<BigGParameter> cardinals = bgv.getCardinals();
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<cardinals.size(); i++) {
			BigParameter parameter = ch.findParameter(cardinals.get(i).getName());
			sb.append((int) parameter.getValue() + ",");
		}
		String r = sb.toString();
		if (r.endsWith(","))
			r = r.substring(0, r.length()-1);
		
		//Object[] inequalityIndex = this.inequalityIndex(bgv);
		
		BigVariable variable = new BigVariable(name);
		variable.setCardinals(r);
		String sValues = jso.optString("values");
		if (sValues.length()>0)
			variable.setValues(sValues, bgv, ch); //, inequalityIndex);
		else {
			JSONArray jsaValues = jso.optJSONArray("values");
			if (jsaValues==null)
				throw new Exception("Missing the values of the variable " + name);
			variable.setValues(jsaValues, bgv, ch);
		}
		return variable;
	}

	public void setProblemName(String problemName) throws Exception {
		if (problemName.trim().length()==0)
			throw new Exception("Problem name expected");
		this.problemName = problemName;
	}
	
	public String getProblemName() {
		return problemName;
	}
	
	public void addSummation(BigGSummation expr) throws Exception {
		if (this.expressions.contains(expr))
			throw new Exception("The problem already contains the added expression");
		this.expressions.add(expr);
	}

	public void removeLastExpression() {
		if (!this.expressions.isEmpty())
			this.expressions.remove(this.expressions.size()-1);
	}

	public BigGSummation toEquality(int index) {
		BigGSummation expr = this.expressions.get(index);
		expr.proceedsFromInequality = true;
		expr.setOldOperator(expr.body.get(0).getOperator());
		expr.toEquality();
		return expr;
	}
	
	public void removeExpression(int index) {
		this.expressions.remove(index);
	}

	public void setXCardinals(String xCardinals) throws Exception {
		String[] tokens = xCardinals.split(",");
		BigGParameter par;
		this.xCardinals.clear();
		String token;
		for (int i=0; i<tokens.length; i++) {
			token = tokens[i].trim();
			par = this.findParameter(token);
			if (par==null)
				throw new Exception("Parameter " + token + " not found in the cardinals of x");
			this.xCardinals.add(par);
		}
	}
	
	public List<BigGSummation> getExpressions() {
		return expressions;
	}
	
	public List<BigGParameter> getXCardinals() {
		return xCardinals;
	}
	
	public void parseExpressionToIsing() {
		for (int i = 0; i < expressions.size(); i++) {
			expressions.get(i).parseToIsing();
		}
	}
	
	public void parseExpressionToQUBO() {
		for (int i = 0; i < expressions.size(); i++) {
			expressions.get(i).parseToQUBO();
		}
	}
}
