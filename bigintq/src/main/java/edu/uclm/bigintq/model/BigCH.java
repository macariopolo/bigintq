package edu.uclm.bigintq.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.bigintq.model.g.BigGH;
import edu.uclm.bigintq.model.g.BigGParameter;

public class BigCH implements IHasCombinations {
	private String functionName;
	private BigX x;
	private Map<String, BigVariable> variables;
	private Map<String, BigParameter> parameters;
	private List<BigExpression> expressions;
	private int combinationLength;
	private List<BigUseOfX> usesOfX;
	private List<BigUseOfS> usesOfS;
	
	public BigCH() {
		this.x = new BigX();
		this.variables = new HashMap<>();
		this.parameters = new HashMap<>();	
		this.expressions = new ArrayList<>();
		this.usesOfX = new ArrayList<>();
		this.usesOfS = new ArrayList<>();
	}

	public BigCH(String functionName) {
		this();
		this.functionName = functionName;
	}
	
	public BigCH(BigGH gh, JSONObject jsoInstance) throws Exception {
		this();
		JSONObject jso;
		JSONArray parameters = jsoInstance.getJSONArray("parameters");
		for (int i=0; i<parameters.length(); i++) {
			jso = parameters.getJSONObject(i);
			this.addParameter(jso.getString("name"), jso.getDouble("value"));
		}
		
		instantiateXCardinals(gh.getXCardinals());
		
		JSONArray variables = jsoInstance.getJSONArray("variables");
		for (int i=0; i<variables.length(); i++) {
			jso = variables.getJSONObject(i);
			BigVariable variable = gh.instantiateVariable(this, jso);
			this.addVariable(variable);
		}
		
		JSONArray jsaExpressions = jsoInstance.getJSONArray("expressions");
		for (int i=0; i<jsaExpressions.length(); i++) {
			JSONObject jsoExpr = jsaExpressions.getJSONObject(i);
			BigExpression expr = BigExpression.build(jsoExpr, this);
			this.expressions.add(expr);
			if (jsoExpr.has("lambda")) {
				((BigLambdadedExpression) expr).setLambda(jsoExpr.getDouble("lambda"));
			}
		}
	}
	
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		Collection<BigVariable> eVariables = this.variables.values();
		if (!eVariables.isEmpty()) {
			JSONArray jsa = new JSONArray();
			for (BigVariable variable : eVariables)
				jsa.put(variable.toJSON());
			jso.put("variables", jsa);
		}
		
		Collection<BigParameter> eParameters = this.parameters.values();
		if (!eParameters.isEmpty()) {
			JSONArray jsa = new JSONArray();
			for (BigParameter parameter : eParameters)
				jsa.put(parameter.toJSON());
			jso.put("parameters", jsa);
		}
		
		JSONArray jsa = new JSONArray();
		for (BigExpression expr : this.expressions)
			jsa.put(expr.toJSON());
		jso.put("expressions", jsa);
		
		jso.put("xCardinals", this.x.getXCardinalsAsString());
		JSONArray jsaPrefixedValues = this.x.getPrefixedValues(); 
		if (jsaPrefixedValues.length()>=0)
			jso.put("prefixedX", jsaPrefixedValues);
		return jso;
	}

	public List<String> toMathML() {
		List<String> result = new ArrayList<>();
		for (BigExpression expr : this.expressions) {
			StringBuilder sb = new StringBuilder();
			sb.append("<math xmlns='http://www.w3.org/1998/Math/MathML' display='block'>");
			sb.append(expr.toMathML());
			sb.append("</math>");
			result.add(sb.toString());
		}
		return result;
	}

	public void setXCardinals(String xCardinals) {
		this.combinationLength = this.x.setCardinals(xCardinals);
	}
	
	public void increaseXCardinals() {
		this.combinationLength = this.x.increaseCardinals();
	}
	
	public void instantiateXCardinals(List<BigGParameter> xCardinals) {
		StringBuilder sb = new StringBuilder();
		int value;
		for (int i=0; i<xCardinals.size(); i++) {
			BigParameter parameter = this.parameters.get(xCardinals.get(i).getName());
			value = (int) parameter.getValue() + parameter.getSlacks();
			sb.append(value + ",");
		}
		String r = sb.toString();
		if (r.endsWith(","))
			r = r.substring(0, r.length()-1);
		this.setXCardinals(r);
	}
	
	public BigInteger getNumberOfCombinations() {
		return this.x.getNumberOfCombinations();
	}
	
	public void addVariable(BigVariable variable) {
		this.variables.put(variable.getName(), variable);
	}
	
	public void addVariable(String name, String cardinals, String values) {
		BigVariable variable = new BigVariable(name);
		variable.setCardinals(cardinals);
		String[] tokens = values.split(",");
		for (String token : tokens)
			variable.addValue(Double.parseDouble(token.trim()));
		this.variables.put(name, variable);
	}

	public void addVariable(String name, String cardinals, JSONArray jsaValues) {
		BigVariable variable = new BigVariable(name);
		variable.setCardinals(cardinals);
		variable.setValues(jsaValues);
		this.variables.put(name, variable);
	}
	
	public void addVariable(String name, String cardinals, double... values) throws Exception {
		BigVariable variable = new BigVariable(name);
		variable.setCardinals(cardinals);
		variable.setValues(values);
		this.variables.put(name, variable);
	}

	public void addParameter(String name, double value) {
		BigParameter parameter = new BigParameter(name, value, this);
		this.parameters.put(name, parameter);
	}
	
	public void setFunction(JSONObject jsoFunction) {
		JSONArray jsa = jsoFunction.optJSONArray("variables");
		JSONObject jso;
		if (jsa!=null)
			for (int i=0; i<jsa.length(); i++) {
				jso = jsa.getJSONObject(i);
				if (jso.optJSONArray("values")!=null)
					this.addVariable(jso.getString("name"), jso.getString("cardinals"), jso.getJSONArray("values"));
				else
					this.addVariable(jso.getString("name"), jso.getString("cardinals"), jso.getString("values"));
			}
		
		String xCardinals = jsoFunction.getString("xCardinals");
		this.setXCardinals(xCardinals);
		
		this.x.setPrefixedValues(jsoFunction.getJSONArray("prefixedX"));
		
		JSONArray jsaExpressions = jsoFunction.getJSONArray("expressions");
		BigLambdadedExpression expr;
		for (int i=0; i<jsaExpressions.length(); i++) {
			jso = jsaExpressions.getJSONObject(i);
			expr = (BigLambdadedExpression) BigExpression.build(jso, this);
			expr.setLambda(jso.getDouble("lambda"));
			this.expressions.add(expr);
		}
	}

	public Map<String, Object> getResult(BigInteger index) {
		List<Integer> combination = this.getCombination(index);
		double value = 0;
		for(BigExpression expr : this.expressions)
			value = value + expr.getValue(combination);
		
		HashMap<String, Object> result = new HashMap<>(); 
		result.put("combination", combination.toString());
		result.put("energy", value);
		return result;
	}
	
	public BigX getX() {
		return x;
	}
	
	public String getFunctionName() {
		return functionName;
	}

	public void addExpression(BigExpression expr) {
		this.expressions.add(expr);
	}
	
	public BigVariable findVariable(String name) {
		return this.variables.get(name.trim());
	}
	
	public BigParameter findParameter(String name) {
		return this.parameters.get(name.trim());
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<this.expressions.size()-1; i++)
			sb = sb.append(this.expressions.get(i).toString() + " + \n");
		sb = sb.append(this.expressions.get(this.expressions.size()-1).toString());
		return sb.toString();
	}
	
	public List<BigExpression> getExpressions() {
		return expressions;
	}

	public void addUseOfX(BigUseOfX useOfX) {
		if (this.usesOfX.contains(useOfX))
			return;
		this.usesOfX.add(useOfX);
	}
	
	public List<BigUseOfX> getUsesOfX() {
		return this.usesOfX;
	}

	@Override
	public int getCombinationLength() {
		return this.combinationLength;
	}

	public void sortExpressionsByLambda(String orderType) {
		
		this.expressions.sort(new Comparator<BigExpression>() {
			@Override
			public int compare(BigExpression o1, BigExpression o2) {
				if (o1 instanceof BigLambdadedExpression && o2 instanceof BigLambdadedExpression) {
					BigLambdadedExpression a = (BigLambdadedExpression) o1;
					BigLambdadedExpression b = (BigLambdadedExpression) o2;
					if (a.getLambda()>b.getLambda())
						return -1;
					Integer bSize = b.getUsesOfX().size();
					Integer aSize = a.getUsesOfX().size();
					return bSize.compareTo(aSize);
				} else
					return 1;
			}
		});
	}

	public List<Double> detach(String sCombination) {
		sCombination = sCombination.substring(1);
		sCombination = sCombination.substring(0, sCombination.length()-1);
		String[] tokens = sCombination.split(",");
		BigInteger index = BigInteger.ZERO;
		BigInteger value;
		BigInteger two = BigInteger.valueOf(2);
		for (int i=0; i<tokens.length; i++) {
			value = new BigInteger(tokens[i].trim());
			if (value.equals(BigInteger.ONE))
				index = index.add(two.pow(tokens.length - i));
		}
		List<Integer> combination = this.getCombination(index);
		
		List<Double> result = new ArrayList<>();
		for(BigExpression expr : this.expressions)
			result.add(expr.getValue(combination));
		return result;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public void setX(BigX x) {
		this.x = x;
	}

	public Map<String, BigVariable> getVariables() {
		return this.variables;
	}

	public void setVariables(Map<String, BigVariable> variables) {
		this.variables = variables;
	}

	public Map<String, BigParameter> getParameters() {
		return this.parameters;
	}

	public void setParameters(Map<String, BigParameter> parameters) {
		this.parameters = parameters;
	}

	public void setCombinationLength(int combinationLength) {
		this.combinationLength = combinationLength;
	}

	public void setUsesOfX(List<BigUseOfX> usesOfX) {
		this.usesOfX = usesOfX;
	}
	
	public Double getPrefixedXValue(List<Integer> indexes) {
		return this.x.getPrefixedValue(indexes);
	}
	
	public List<BigUseOfS> getUsesOfS() {
		return usesOfS;
	}

	public void setUsesOfS(List<BigUseOfS> usesOfS) {
		this.usesOfS = usesOfS;
	}
	
	public void parseExpressionToIsing() {
		for (int i = 0; i < expressions.size(); i++) {
 			expressions.get(i).parseToIsing();
		}
	}
	
	public void parseExpressionToQUBO() {
		for (int i = 0; i < expressions.size(); i++) {
 			expressions.get(i).parseToQUBO();
		}
	}
	
	public void addUseOfS(BigUseOfS useOfS) {
		this.usesOfS.add(useOfS);
	}
}
