package edu.uclm.bigintq.model;

public class BigParser {
	
	public static BigQuotient parseQUBOToIsing(BigUseOfX x) {
		// x = (s + 1) / 2
		BigQuotient bgq = new BigQuotient(x.getH());
		BigDoubleValue bdv = new BigDoubleValue(1.0, x.getH());
		BigUseOfS uos = new BigUseOfS(x.getH());
		uos.setIndexes(x.getIndexes());
		BigSum bs = new BigSum(x.getH());
		bs.add(uos);
		bs.add(bdv);
		bgq.setNum(bs);
		bgq.setDenom(new BigDoubleValue(2.0, x.getH()));
		return bgq;
	}
	
	public static BigSubtraction parseIsingToQUBO(BigUseOfS s) {
		// s = 2x - 1 
		BigSubtraction bs = new BigSubtraction(s.getH());
		BigProduct bp = new BigProduct(s.getH());
		bp.setLeft(new BigDoubleValue(2.0, s.getH()));
		BigUseOfX uox = new BigUseOfX(s.getH());
		uox.setIndexes(s.getIndexes());
		bp.setRight(uox);
		bs.setLeft(bp);
		bs.setRight(new BigDoubleValue(1.0, s.getH()));
		return bs;
	}

}
