package edu.uclm.bigintq.model.g;

import java.util.Objects;

import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;

public class BigGFrom {
	private String variable;
	private BigGExpr right;
	
	public BigGFrom(BigGH h) {
	}
	
	public BigGFrom(JSONObject jso, BigGH h) throws Exception {
		this(h);
		this.variable = jso.optString("left").trim();
		if (this.variable.length()==0)
			throw new Exception("Expected variable name in the FROM clause");
		this.right = BigGExpr.build(jso.getJSONObject("right"), h);
	}
	
	@Override
	public String toString() {
		return this.variable + "=" + this.right.toString();
	}
	
	protected JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("left", variable);
		jso.put("right", this.right.toJSON());
		return jso;
	}
	
	public String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<mrow>");
		sb.append("<mi>" + this.variable + "</mi>");
		sb.append("<mo>=</mo>");
		sb.append(this.right.toMathML());
		sb.append("</mrow>");
		return sb.toString();
	}

	public String getVariable() {
		return variable;
	}

	public double getValue(BigCH h) {
		return this.right.getValue(h);
	}

	@Override
	public int hashCode() {
		return Objects.hash(right, variable);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGFrom other = (BigGFrom) obj;
		return Objects.equals(right, other.right) && Objects.equals(variable, other.variable);
	}
}
