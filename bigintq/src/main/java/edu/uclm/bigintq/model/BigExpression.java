package edu.uclm.bigintq.model;

import java.lang.reflect.Constructor;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.json.JSONObject;

public abstract class BigExpression {
	
	private BigCH h;
	protected List<BigUseOfX> usesOfX;
	
	private List<Integer> emptyCombination;
	private Vector<Integer> nullPositions, notNullPositions;

	public BigExpression(BigCH h) {
		this.h = h;
		this.usesOfX = new ArrayList<>();
	}

	@SuppressWarnings("unchecked")
	public static BigExpression build(JSONObject jso, BigCH h) {
		String type = jso.optString("type");
		if (type.equals("Substraction"))
			type = "Subtraction";
		type = "edu.uclm.bigintq.model.Big" + type;
		try {
			Class<BigExpression> clazz = (Class<BigExpression>) Class.forName(type);
			Constructor<BigExpression> constructor = clazz.getDeclaredConstructor(JSONObject.class, BigCH.class);
			BigExpression result = constructor.newInstance(jso, h);
			if (result instanceof BigUseOfX) {
				boolean slack = jso.optBoolean("slack");
				((BigUseOfX) result).setSlack(slack);
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	protected BigCH getH() {
		return h;
	}
	
	public List<BigUseOfX> getUsesOfX() {
		return usesOfX;
	}
	
	public int getCombinationLength() {
		return this.usesOfX.size();
	}
	
	public void calculateEmptyCombination() {
		this.emptyCombination = new Vector<>();
		this.nullPositions = new Vector<>();
		this.notNullPositions = new Vector<>();
		for (int i=0; i<this.h.getUsesOfX().size(); i++) {
			BigUseOfX hUox = this.h.getUsesOfX().get(i);
			if (!this.usesOfX.contains(hUox)) {
				this.notNullPositions.add(i);
				this.emptyCombination.add(null);
			} else {
				this.nullPositions.add(i);
				this.emptyCombination.add(0);
			}
		}
	}
	
	public Vector<Integer> getNullPositions() {
		return this.nullPositions;
	}
	
	public List<Integer> getNotNullPositions() {
		return notNullPositions;
	}
	
	public final BigInteger getNumberOfCombinations() {
		this.calculateEmptyCombination();
		BigInteger result = BigInteger.valueOf(2).pow(this.usesOfX.size());
		return result;
	}
	
	public List<Integer> getCombination(BigInteger index) {
		List<Integer> result = new Vector<>();
		for (Integer i : this.emptyCombination)
			result.add(i);

		BigInteger TWO = BigInteger.valueOf(2);
		
		BigInteger quotient = index;
		BigInteger remainder;
		for (int i=this.nullPositions.size()-1; i>=0; i--) {
			remainder = quotient.remainder(TWO);
			quotient = quotient.divide(TWO);
			result.set(this.nullPositions.get(i), remainder.intValue());
		} 
		return result;
	}
	
	public List<Integer> getEmptyCombination() {
		return emptyCombination;
	}
	
	public abstract double getValue(List<Integer> combination);
	
	@Override
	public abstract String toString();
	
	public abstract JSONObject toJSON();

	protected abstract String toMathML();
	
	@Override
	public abstract boolean equals(Object obj);
	
	@Override
	public abstract int hashCode();

	public abstract List<BigUseOfX> loadUsesOfX();

	protected abstract BigExpression divideInto(double value);

	protected abstract BigExpression square();
	
	public abstract BigExpression parseToIsing();
		
	public abstract BigExpression parseToQUBO();
}
