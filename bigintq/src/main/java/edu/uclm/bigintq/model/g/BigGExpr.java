package edu.uclm.bigintq.model.g;

import java.lang.reflect.Constructor;

import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigExpression;

public abstract class BigGExpr {

	private BigGH h;

	@SuppressWarnings("unchecked")
	public static BigGExpr build(JSONObject jso, BigGH bigGH) throws Exception {
		String type = jso.getString("type");
		type = "edu.uclm.bigintq.model.g.BigG" + type;
		
		Class<BigGExpr> clazz = (Class<BigGExpr>) Class.forName(type);
		Constructor<BigGExpr> constructor = clazz.getDeclaredConstructor(JSONObject.class, BigGH.class);
		try {
			BigGExpr result = constructor.newInstance(jso, bigGH);
			result.h = bigGH;
			return result;
		} catch (Exception e) {
			throw new Exception(e.getCause());
		}
	}
	
	public BigGH getGH() {
		return h;
	}

	protected abstract BigExpression getBigExpression(BigCH h, String indexName, int indexValue) throws Exception;

	public abstract double getValue(BigCH h);

	protected abstract JSONObject toJSON();

	public abstract String toMathML();
	
	@Override
	public abstract String toString();

	protected abstract BigGExpr expand(int indexValue, String indexName);

	protected final BigGExpr divideBy(BigGExpr denom) {
		BigGQuotient result = new BigGQuotient();
		result.setNum(this);
		result.setDenom(denom);
		return result;
	}

	protected abstract boolean uses(BigGVariable bgv);
	
	public abstract BigGExpr parseToIsing();
	
	public abstract BigGExpr parseToQUBO();
}
