package edu.uclm.bigintq.model.g;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigSum;

public class BigGSum extends BigGExpr {
	
	private List<BigGExpr> exprs;
	
	public BigGSum() {
		this.exprs = new ArrayList<>();
	}
	
	public BigGSum(JSONObject jso, BigGH gh) throws Exception {
		this();
		JSONArray jsaExpressions = jso.getJSONArray("expressions");
		for (int i=0; i<jsaExpressions.length(); i++) 
			this.exprs.add(BigGExpr.build(jsaExpressions.getJSONObject(i), gh));
	}
	
	public void add(BigGExpr expr) {
		this.exprs.add(expr);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<this.exprs.size(); i++)
			sb.append(this.exprs.get(i).toString() + "+");
		String r = sb.toString();
		r = r.substring(0, r.length()-1);
		return r;
	}
	
	@Override
	protected JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "Sum");
		JSONArray jsa = new JSONArray();
		for (BigGExpr expr : this.exprs)
			jsa.put(expr.toJSON());
		jso.put("expressions", jsa);
		return jso;
	}

	@Override
	public String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<mfenced separators='' open='(' close = ')'>");
		for (int i=0; i<exprs.size()-1; i++) { 
			sb.append(exprs.get(i).toMathML());
			sb.append("<mo>+</mo>");
		}
		sb.append(exprs.get(exprs.size()-1).toMathML());
		sb.append("</mfenced>");
		return sb.toString();
	}

	@Override
	protected BigExpression getBigExpression(BigCH h, String indexName, int indexValue) throws Exception {
		BigSum sum = new BigSum(h);
		for (BigGExpr expr : this.exprs)
			sum.add(expr.getBigExpression(h, indexName, indexValue));
		return sum;
	}

	@Override
	public double getValue(BigCH h) {
		double r = 0;
		for (BigGExpr expr : this.exprs)
			r = expr.getValue(h);
		return r;
	}

	@Override
	public int hashCode() {
		return Objects.hash(exprs);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGSum other = (BigGSum) obj;
		return Objects.equals(exprs, other.exprs);
	}

	@Override
	protected BigGExpr expand(int indexValue, String indexName) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	protected boolean uses(BigGVariable bgv) {
		for (BigGExpr expr : this.exprs)
			if (expr.uses(bgv))
				return true;
		return false;
	}
	
	@Override
	public BigGExpr parseToIsing() {
		for (int i = 0; i < this.exprs.size(); i++) {
			this.exprs.set(i, this.exprs.get(i).parseToIsing());
		}
		return this;
	}

	@Override
	public BigGExpr parseToQUBO() {
		for (int i = 0; i < this.exprs.size(); i++) {
			this.exprs.set(i, this.exprs.get(i).parseToQUBO());
		}
		return this;
	}
}
