package edu.uclm.bigintq.model.g;

import java.util.Objects;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigDoubleValue;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigProduct;

public class BigGProduct extends BigGExpr implements IBigGExpandibleExpr {
	
	private BigGExpr left, right;
	
	public BigGProduct() {
	}
	
	public BigGProduct(JSONObject jso, BigGH h) throws Exception {
		this();
		this.left = BigGExpr.build(jso.getJSONObject("left"), h);
		this.right = BigGExpr.build(jso.getJSONObject("right"), h);
	}
	
	public BigGProduct(String sProduct, BigGSummation summation, BigGH h) throws Exception {
		this();
		String[] tokens = sProduct.split("·");
		if (tokens.length==1)
			throw new Exception("Use '·' to multiply");
		
		if (tokens.length==2) {
			this.left = build(tokens[0], summation, h);
			this.right = build(tokens[1], summation, h);
		} else {
			this.left = this.build(tokens[0], summation, h);
			this.right = build(tokens, 1, summation, h);
		}
	}
	
	private BigGExpr build(String token, BigGSummation summation, BigGH h) throws Exception {
		String[] tokens = token.split("_");
		if (tokens.length==1)
			throw new Exception("Use '_' to separate subindexes (i.e.: x_i_j)");
		
		JSONObject jsoExpr;
		if (token.startsWith("x")) {
			 jsoExpr = new JSONObject().
				put("type", "UseOfX");
		} else {
			jsoExpr =  new JSONObject().
				put("type", "IndexedVariableReference").
				put("variable", tokens[0]);
		}
		
		JSONArray indexes = new JSONArray();
		String tk;
		for (int i=1; i<tokens.length; i++) {
			tk = tokens[i].trim();
			if (!summation.containsAsIndex(tk))
				throw new Exception("Variable " + tk + ", used as index, not found");
			indexes.put(tokens[i]);
		}
		jsoExpr.put("indexes", indexes);
		return BigGExpr.build(jsoExpr, h);
	}
	
	private BigGExpr build(String[] tokens, int start, BigGSummation summation, BigGH h) throws Exception {
		BigGExpr left = this.build(tokens[start], summation, h);
		if (start==tokens.length-1)
			return left;
		BigGExpr right = this.build(tokens, start+1, summation, h);
		BigGProduct result = new BigGProduct();
		result.left = left;
		result.right = right;
		return result;
	}
	
	@Override
	public String toString() {
		return this.left.toString() + "·" + this.right.toString();
	}
	
	@Override
	protected JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "Product");
		jso.put("left", this.left.toJSON());
		jso.put("right", this.right.toJSON());
		return jso;
	}

	@Override
	public String toMathML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<mfenced separators='' open='(' close = ')'>");
		sb.append(this.left.toMathML());
		sb.append("<mo>·</mo>");
		sb.append(this.right.toMathML());
		sb.append("</mfenced>");
		return sb.toString();
	}

	@Override
	protected BigExpression getBigExpression(BigCH h, String indexName, int indexValue) throws Exception {
		BigProduct product = new BigProduct(h);
		
		BigExpression expr = this.left.getBigExpression(h, indexName, indexValue);
		if (expr==null)
			return null;
		product.setLeft(expr);
		expr = this.right.getBigExpression(h, indexName, indexValue);
		if (expr==null)
			return null;
		product.setRight(expr);
		
		if (product.getLeft() instanceof BigDoubleValue) {
			BigDoubleValue value = (BigDoubleValue) product.getLeft();
			if (value.getValue()==0.0)
				return null;
		}
		if (product.getRight() instanceof BigDoubleValue) {
			BigDoubleValue value = (BigDoubleValue) product.getRight();
			if (value.getValue()==0.0)
				return null;
		}
		if (product.getLeft() instanceof BigDoubleValue) {
			BigDoubleValue value = (BigDoubleValue) product.getLeft();
			if (value.getValue()==1.0)
				return product.getRight();
		}
		if (product.getRight() instanceof BigDoubleValue) {
			BigDoubleValue value = (BigDoubleValue) product.getRight();
			if (value.getValue()==1.0)
				return product.getLeft();
		}
		return product;
	}

	@Override
	public double getValue(BigCH h) {
		double a = this.left.getValue(h);
		if (a==0)
			return 0;
		return a * this.right.getValue(h);
	}

	@Override
	public int hashCode() {
		return Objects.hash(left, right);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigGProduct other = (BigGProduct) obj;
		return Objects.equals(left, other.left) && Objects.equals(right, other.right);
	}

	@Override
	public BigGExpr expand(int indexValue, String indexName) {
		BigGIndexedExpr left = (BigGIndexedExpr) this.left;
		BigGExpr right = this.right;
		BigGProduct result = new BigGProduct();
		result.left = left.expand(indexValue, indexName);
		result.right = right.expand(indexValue, indexName);
		return result;
	}	
	
	public void setLeft(BigGExpr left) {
		this.left = left;
	}
	
	public void setRight(BigGExpr right) {
		this.right = right;
	}
	
	@Override
	protected boolean uses(BigGVariable bgv) {
		return this.left.uses(bgv) || this.right.uses(bgv);
	}
	
	@Override
	public BigGExpr parseToIsing() {
		this.left = left.parseToIsing();
		this.right = right.parseToIsing();
		return this;
	}

	@Override
	public BigGExpr parseToQUBO() {
		this.left = left.parseToQUBO();
		this.right = right.parseToQUBO();
		return this;
	}
}
