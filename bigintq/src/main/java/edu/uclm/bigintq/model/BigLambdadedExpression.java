package edu.uclm.bigintq.model;

public abstract class BigLambdadedExpression extends BigExpression {
	protected Double lambda;
	
	public BigLambdadedExpression(BigCH h) {
		super(h);
	}
	
	public Double getLambda() {
		return lambda;
	}
	
	public void setLambda(Double lambda) {
		this.lambda = lambda;
	}
	
	public BigDoubleValue getLambdaAsBigDoubleValue(BigCH h) {
		return new BigDoubleValue(this.lambda, h);
	}
	
	public abstract BigSum develop(BigCH h);
	
	@Override
	public abstract String toString();

	public abstract boolean isEmpty();
}
