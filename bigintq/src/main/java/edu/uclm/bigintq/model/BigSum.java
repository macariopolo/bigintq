package edu.uclm.bigintq.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.json.JSONArray;
import org.json.JSONObject;

public class BigSum extends BigLambdadedExpression {
	private List<BigExpression> expressions;
	
	public BigSum(BigCH h) {
		super(h);
		this.expressions = new ArrayList<>();
	}

	public BigSum(JSONObject jso, BigCH h) {
		this(h);
		JSONArray jsaExpressions = jso.getJSONArray("expressions");
		for (int i=0; i<jsaExpressions.length(); i++) 
			this.expressions.add(BigExpression.build(jsaExpressions.getJSONObject(i), h));
	}
	
	@Override
	protected BigExpression square() {
		BigSum result = new BigSum(getH());
		int size = this.expressions.size();
		BigExpression exprI;
		for (int i=0; i<size; i++) {
			exprI = this.expressions.get(i);
			result.add(exprI.square());
		}
		
		BigExpression exprJ;
		BigProduct twice, exprIJ;
		for (int i=0; i<size; i++) {
			exprI = this.expressions.get(i);
			for (int j=i+1; j<size; j++) {
				exprJ = this.expressions.get(j);
				exprIJ = new BigProduct(getH());
				exprIJ.setLeft(exprI);
				exprIJ.setRight(exprJ);
				BigExpression simplifiedIJ = exprIJ.simplify();
				twice = new BigProduct(getH());
				twice.setLeft(new BigDoubleValue(2.0, getH()));
				twice.setRight(simplifiedIJ);
				result.add(twice.simplify());
			}
		}
		
		return result;
	}

	public void simplifyProducts() {
		for (int i=0; i<this.expressions.size(); i++) {
			BigExpression expr = this.expressions.get(i);
			if (expr instanceof BigProduct) {
				BigProduct product = (BigProduct) expr;
				this.expressions.set(i, product.simplify());
			}
		}		
	}
	
	@Override
	protected BigExpression divideInto(double value) {
		for (BigExpression expr : this.expressions)
			expr = expr.divideInto(value);
		return this;
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public List<BigUseOfX> loadUsesOfX() {
		this.usesOfX.clear();
		for (BigExpression expr : this.expressions)
			this.usesOfX.addAll(expr.loadUsesOfX());
		return this.usesOfX;
	}
	
	@Override
	public BigSum develop(BigCH h) {
		if (this.getLambda()!=null && this.getLambda()==1)
			return this;
		if (this.getLambda()==null || this.getLambda()==0)
			return null;
		
		BigSum newSum = new BigSum(h);
		for (int i=0; i<this.expressions.size(); i++) {
			BigExpression summand = this.expressions.get(i);
			BigProduct product = new BigProduct(h);
			product.setLeft(this.getLambdaAsBigDoubleValue(h));
			product.setRight(summand);
			summand = product.simplify();
			newSum.add(summand);			
		}
		return newSum;
	}
	
	public void add(BigExpression expr) {
		this.expressions.add(expr);
	}

	@Override
	public double getValue(List<Integer> combination) {
		double r = 0;
		for (BigExpression expr : this.expressions)
			r = r + expr.getValue(combination);
		if (this.lambda!=null)
			r = this.lambda * r;
		return r;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		boolean addLambda = false;
		if (this.getLambda()!=null && this.getLambda().doubleValue()!=1)
			addLambda = true;
		for (int i=0; i<this.expressions.size()-1; i++)
			sb.append(this.expressions.get(i).toString() + " + ");
		sb.append(this.expressions.get(this.expressions.size()-1).toString());
		if (addLambda) 
			return this.getLambda() + "*[" + sb.toString() + "]";
		return sb.toString();
	}
	
	@Override
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("type", "Sum");
		JSONArray jsa = new JSONArray();
		for (BigExpression expr : this.expressions)
			jsa.put(expr.toJSON());
		jso.put("expressions", jsa);
		jso.put("lambda", this.lambda);
		return jso;
	}

	@Override
	protected String toMathML() {
		StringBuilder sb = new StringBuilder();
		if (this.lambda!=null && lambda.doubleValue()!=1) 
			sb.append("<mn>" + this.lambda + "</mn><mo>·</mo>");
		sb.append("<mfenced separators='' open='(' close = ')'>");
		for (int i=0; i<expressions.size()-1; i++) { 
			sb.append(expressions.get(i).toMathML());
			sb.append("<mo>+</mo>");
		}
		sb.append(expressions.get(expressions.size()-1).toMathML());
		sb.append("</mfenced>");
		return sb.toString();
	}
	
	public List<BigExpression> getExpressions() {
		return expressions;
	}

	@Override
	public int hashCode() {
		return Objects.hash(expressions);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BigSum other = (BigSum) obj;
		return Objects.equals(expressions, other.expressions);
	}
	
	@Override
	public BigExpression parseToIsing() {
		for (int i = 0; i < this.expressions.size(); i++) {
			this.expressions.set(i, this.expressions.get(i).parseToIsing());
		}
		return this;
	}
		
	@Override
	public BigExpression parseToQUBO() {
		for (int i = 0; i < this.expressions.size(); i++) {
			this.expressions.set(i, this.expressions.get(i).parseToQUBO());
		}
		return this;
	}
}
