package edu.uclm.bigintq.http;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.uclm.bigintq.dao.HostConfigurationDAO;
import edu.uclm.bigintq.dao.HostDAO;
import edu.uclm.bigintq.entities.Host;
import edu.uclm.bigintq.entities.HostConfiguration;

@RestController
@RequestMapping("hosts")
public class HostsController {
	@Autowired
	private HostConfigurationDAO hcDao;
	@Autowired
	private HostDAO hostDao;
	
	@GetMapping("/getAll")
	public List<HostConfiguration> getAll() {
		return hcDao.findAll();
	}
	
	@GetMapping("/getHosts")
	public List<Host> getHosts(@RequestParam String id) {
		return hostDao.findByConfigurationId(id);
	}
}
