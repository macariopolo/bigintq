package edu.uclm.bigintq.http;

public interface IHttpClientReceiver {

	void onResponseReceived(String response, String action, Object... parameters);

}
