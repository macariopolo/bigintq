package edu.uclm.bigintq.http;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import edu.uclm.bigintq.model.g.BigGH;

@RestController
@RequestMapping("mathML")
public class MathMLController {
	
	@PostMapping("/getMathML")
	public List<String> getMathML(HttpSession session, @RequestBody(required = false) Map<String, Object> info) {
		JSONObject jso = new JSONObject(info);
		try {
			BigGH gh;
			if (info!=null)
				gh = new BigGH(jso);
			else
				gh = (BigGH) session.getAttribute("gh");
			return gh.toMathML();
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
}
