package edu.uclm.bigintq.http;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.g.BigGH;
import edu.uclm.bigintq.model.g.JBigGH;

@RestController
@RequestMapping("parser")
public class ParserController {
	@PostMapping(value = "/parseGHToIsing")
	public JBigGH parseGHToIsing(HttpSession session, @RequestBody Map<String, Object> info) {
		try {		
			BigGH gh = (BigGH) session.getAttribute("gh");
			gh.parseExpressionToIsing();

			JBigGH result = new JBigGH(gh.getProblemName(), gh.toJSON().toString());
			return result;
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	
	@PostMapping(value = "/parseGHToQUBO")
	public JBigGH parseGHToQUBO(HttpSession session, @RequestBody Map<String, Object> info) {
		try {
			BigGH gh = (BigGH) session.getAttribute("ghQUBO");
			
			JBigGH result = new JBigGH(gh.getProblemName(), gh.toJSON().toString());
			return result;
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	@PostMapping(value = "/parseCHToIsing")
	public List<String> parseCHToIsing(HttpSession session, @RequestBody Map<String, Object> info) {
		try {
			BigCH ch = (BigCH) session.getAttribute("ch");
			ch.parseExpressionToIsing();

			return ch.toMathML();
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	
	@PostMapping(value = "/parseCHToQUBO")
	public List<String> parseCHToQUBO(HttpSession session, @RequestBody Map<String, Object> info) {
		try {
			BigCH ch = (BigCH) session.getAttribute("chQUBO");

			return ch.toMathML();
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

}
