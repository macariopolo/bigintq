package edu.uclm.bigintq.http;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

@Component
public class Manager {
	private JSONObject configuration;
	private ConcurrentHashMap<String, HttpSession> httpSessions;
	private ConcurrentHashMap<String, WebSocketSession> wsSessions;

	private Manager() {
		this.httpSessions = new ConcurrentHashMap<>();
		this.wsSessions = new ConcurrentHashMap<>();
		String folder = System.getProperty("user.home") + File.separatorChar + "bigintq" + File.separatorChar;
		new File(folder).mkdir();
		try {
			FileUtils.deleteDirectory(new File(folder));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void clearFolder() {
		String folder = System.getProperty("user.home") + File.separatorChar + "bigintq" + File.separatorChar;
		File file = new File(folder);
		if (file.exists())
			try {
				FileUtils.cleanDirectory(new File(folder));
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public String createFolder(String prefix, boolean useRandom) {
		String folder = System.getProperty("user.home") + File.separatorChar + "bigintq" + File.separatorChar;
		new File(folder).mkdir();
		if (prefix!=null)
			folder = folder + prefix;
		if (useRandom)
			folder = folder + UUID.randomUUID().toString();
		new File(folder).mkdir();
		return folder;
	}
	
	private static class ManagerHolder {
		static Manager singleton=new Manager();
	}
	
	@Bean
	public static Manager get() {
		return ManagerHolder.singleton;
	}

	public JSONObject getConfiguration() {
		return configuration;
	}
	
	public JSONObject read(String fileName) throws FileNotFoundException, IOException {
		 ClassLoader classLoader = getClass().getClassLoader();
		 try (InputStream fis = classLoader.getResourceAsStream(fileName)) {
			byte[] b = new byte[fis.available()];
			fis.read(b);
			String s = new String(b);
			return new JSONObject(s);
		 }
	}
	
	public String readAsText(String fileName) throws FileNotFoundException, IOException {
		 ClassLoader classLoader = getClass().getClassLoader();
		 try (InputStream fis = classLoader.getResourceAsStream(fileName)) {
			byte[] b = new byte[fis.available()];
			fis.read(b);
			String s = new String(b);
			return s;
		 }
	}

	public void putHttpSession(String id, HttpSession session) {
		this.httpSessions.put(id, session);
	}
	
	public HttpSession getHttpSession(String id) {
		return this.httpSessions.get(id);
	}
	
	public void putWsSession(String httpSessionId, WebSocketSession session) {
		this.wsSessions.put(httpSessionId, session);
	}
	
	public WebSocketSession getWsSession(String httpSessionId) {
		return this.wsSessions.get(httpSessionId);
	}
}
