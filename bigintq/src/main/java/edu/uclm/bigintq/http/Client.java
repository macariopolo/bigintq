package edu.uclm.bigintq.http;

import java.io.IOException;
import java.net.URI;

import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class Client implements Runnable {
	
	private BasicCookieStore cookieStore = new BasicCookieStore();
	private String baseUrl;
	
	private String url;
	private Class<? extends HttpRequestBase> requestType;
	private Object payload;
	private IHttpClientReceiver receiver;
	private String action;
	private Object[] parameters;
	private String sessionId;
	
	public Client() {
	}
	
	public Client(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	@Override
	public void run() {
		if (this.baseUrl!=null)
			url = this.baseUrl + url;
		try(CloseableHttpClient client = HttpClientBuilder.create().setDefaultCookieStore(this.cookieStore).build()) {
			HttpRequestBase request = requestType.newInstance();
			request.setURI(URI.create(url));
			HttpEntity entity;

			if (payload!=null) {
				entity = new StringEntity(payload.toString());
				HttpEntityEnclosingRequestBase postPut = (HttpEntityEnclosingRequestBase) request;
				postPut.setEntity(entity);
				postPut.setHeader("Accept", "application/json");
				postPut.setHeader("Content-type", "application/json");
			}
			try(CloseableHttpResponse response = client.execute(request)) {
				
				entity = response.getEntity();
				HeaderElement[] cookies = response.getFirstHeader("Set-Cookie").getElements();
				for (HeaderElement cookie : cookies) {
					if (cookie.getName().equals("JSESSIONID")) {
						this.sessionId = cookie.getValue();
						break;
					}
				}
				
				String responseText = EntityUtils.toString(entity);
				if (receiver!=null)
					receiver.onResponseReceived(responseText, action, parameters);
			} catch (Exception e) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
			}
		} catch (Exception e1) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e1.getMessage());
		}
	}
	
	public void send(String url, Class<? extends HttpRequestBase> requestType, Object payload, IHttpClientReceiver receiver, String action, Object... parameters) {
		this.url = url;
		this.requestType = requestType;
		this.payload = payload;
		this.receiver = receiver;
		this.action = action;
		this.parameters = parameters;
		
		//new Thread(this).start();
		this.run();
	}
	
	public String getSessionId() {
		return sessionId;
	}

	public JSONObject sendGet(String url) {
		if (this.baseUrl!=null)
			url = this.baseUrl + url;
		try(CloseableHttpClient client = HttpClientBuilder.create().setDefaultCookieStore(this.cookieStore).build()) {
			HttpGet get = new HttpGet(url);
			try(CloseableHttpResponse response = client.execute(get)) {
				HttpEntity entity = response.getEntity();
				String responseText = EntityUtils.toString(entity);
				if (responseText==null || responseText.length()==0)
					return null;
				return new JSONObject(responseText);
			} catch (Exception e) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
			}
		} catch (IOException e1) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e1.getMessage());
		}
	}
	
	public JSONObject sendPost(String url, JSONObject payload) {
		if (this.baseUrl!=null)
			url = this.baseUrl + url;
		try(CloseableHttpClient client = HttpClientBuilder.create().setDefaultCookieStore(this.cookieStore).build()) {
			HttpPost post = new HttpPost(url);
			try {
				HttpEntity entity = new StringEntity(payload.toString());
				post.setEntity(entity);
				post.setHeader("Accept", "application/json");
				post.setHeader("Content-type", "application/json");
				
				CloseableHttpResponse response = client.execute(post);
				entity = response.getEntity();
				String responseText = EntityUtils.toString(entity);
				if (responseText==null || responseText.length()==0)
					return null;
				client.close();
				return new JSONObject(responseText);
			} catch (Exception e) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
			}
		} catch (IOException e1) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e1.getMessage());
		}
	}
}
