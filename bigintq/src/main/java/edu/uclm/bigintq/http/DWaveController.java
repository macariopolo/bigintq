package edu.uclm.bigintq.http;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import edu.uclm.bigintq.dao.DWaveCouplerDAO;
import edu.uclm.bigintq.dao.DWaveSolverDAO;
import edu.uclm.bigintq.dwave.DWaveCoupler;
import edu.uclm.bigintq.dwave.DWaveSolver;
import edu.uclm.bigintq.dwave.Proxy;
import edu.uclm.bigintq.dwave.PyGen;
import edu.uclm.bigintq.model.BigCH;

@RestController
@RequestMapping("dwave")
public class DWaveController {
	
	@Autowired
	private DWaveSolverDAO dwaveDAO;
	@Autowired
	private DWaveCouplerDAO dwaveCouplersDAO;
	
	@GetMapping("/checkOnline")
	public String checkOnline(@RequestParam String id) {
		try {
			Proxy proxy = Proxy.get();
			JSONObject conf = proxy.getSolverConfiguration(id);
			return conf.getString("status");
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Error checking machine status: " + e.getMessage());
		}
	}
	
	@GetMapping("/getProblemAnswer")
	public Map<String, Object> getProblemAnswer(@RequestParam String id) {
		Proxy proxy = Proxy.get();
		Object answer = proxy.getProblemAnswer(id);
		return new JSONObject(answer.toString()).toMap();
	}
	
	@GetMapping("/getProblemInfo")
	public Map<String, Object> getProblemInfo(@RequestParam String id) {
		Proxy proxy = Proxy.get();
		Object answer = proxy.getProblemInfo(id);
		return new JSONObject(answer.toString()).toMap();
	}
	
	@GetMapping("/getProblems")
	public List<Object> getProblems() {
		Proxy proxy = Proxy.get();
		Object problems = proxy.getProblems();
		return new JSONArray(problems.toString()).toList();
	}
	
	@PostMapping(value = "/getPythonCode", produces = "application/json")
	public String getPythonCode(HttpSession session, @RequestBody List<Object> matrix) {
		try {
			BigCH ch = (BigCH) session.getAttribute("ch");
			JSONArray jsaMatrix = new JSONArray(matrix);
			JSONObject s = Manager.get().read("dwave.txt");
			JSONArray solvers = s.getJSONArray("solvers");
			String[] code = PyGen.getCode(ch, jsaMatrix);
			JSONObject result = new JSONObject().
				put("solvers", solvers).
				put("code", code);
			return result.toString();
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Error getting Python code: " + e.getMessage());
		}
	}
	
	@PostMapping("/send")
	public Object send(@RequestBody Map<String, Object> info) {
		try {
			Proxy proxy = Proxy.get();
			JSONObject problem = new JSONObject(info);
			return proxy.sendProblem(problem);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Error sending problem: " + e.getMessage());
		}
	}
	
	@GetMapping("/listSolvers")
	public List<DWaveSolver> listSolvers() {
		try {
			return this.dwaveDAO.findAll();
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Error reading dwave configuration file");
		}
	}
	
	@GetMapping("/updateSolvers")
	public void updateSolvers() {
		try {
			Proxy proxy = Proxy.get();
			JSONArray solvers = proxy.getSolvers(), jsaCouplers;
			JSONObject jsoSolver, jsoProperties;
			DWaveCoupler coupler;
			dwaveCouplersDAO.deleteAll();
			dwaveDAO.deleteAll();
			for (int i=0; i<solvers.length(); i++) {
				jsoSolver =  solvers.getJSONObject(i);
				DWaveSolver solver = new DWaveSolver();
				solver.setId(jsoSolver.getString("id"));
				solver.setDescription(jsoSolver.getString("description"));
				solver.setInfo(jsoSolver.toString());
				jsoProperties = jsoSolver.getJSONObject("properties");
				int qubits = jsoProperties.optInt("num_qubits");
				solver.setQubits(qubits);
				this.dwaveDAO.save(solver);
				/*jsaCouplers = jsoProperties.optJSONArray("couplers");
				if (jsaCouplers!=null) {
					for (int j=0; j<jsaCouplers.length(); j++) {
						coupler = new DWaveCoupler();
						coupler.setSolver(solver);
						coupler.setA(jsaCouplers.getJSONArray(j).getInt(0));
						coupler.setB(jsaCouplers.getJSONArray(j).getInt(1));
						this.dwaveCouplersDAO.save(coupler);
					}
				}*/
			}
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Error reading dwave configuration file");
		}
	}
}
