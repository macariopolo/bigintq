package edu.uclm.bigintq.http;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.g.BigGH;

@RestController
@RequestMapping("calculator")
public class SCCalculatorController {
	
	@PostMapping(value = "/prepare", produces = "application/json")
	public void prepare(HttpSession session, @RequestBody Map<String, Object> info) {
		try {
			JSONObject jso = new JSONObject(info);
			BigGH gh = new BigGH(jso.getJSONObject("gh"));
			BigCH ch = gh.instantiate(jso.getJSONObject("instanceInfo"), false);
			ch.sortExpressionsByLambda("GX");
			
			session.setAttribute("gh", gh);
			session.setAttribute("ch", ch);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}		
	}
}
