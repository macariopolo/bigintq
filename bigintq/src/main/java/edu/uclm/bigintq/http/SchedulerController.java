package edu.uclm.bigintq.http;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import edu.uclm.bigintq.dao.HostDAO;
import edu.uclm.bigintq.entities.Host;
import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.g.BigGH;
import edu.uclm.bigintq.services.Cluster;
import edu.uclm.bigintq.services.SchedulerService;
import edu.uclm.bigintq.services.WrapperHost;
import edu.uclm.bigintq.services.sorted.SCCalculatorService;

@RestController
@RequestMapping("scheduler")
public class SchedulerController {
	
	@Autowired
	private HostDAO hostsDao;
	
	@Autowired
	private SchedulerService service;
	
	public static final DecimalFormat formatter = new DecimalFormat("###,###,###,###");
	
	@PostMapping("/distribute")
	public Map<String, Object> distribute(HttpSession session, @RequestBody List<Object> hosts) {
		BigCH ch = (BigCH) session.getAttribute("ch");
		JSONArray jsaHosts = new JSONArray(hosts);
		
		int cores = 0;
		for (int i=0; i<jsaHosts.length(); i++)
			cores+= jsaHosts.getJSONObject(i).getInt("cores");
		
		Cluster cluster = service.distribute(ch, jsaHosts, cores);
		session.setAttribute("cluster", cluster);
		Map<String, Object> result = new HashMap<>();
		result.put("cluster", cluster);
		result.put("ch", ch.toJSON().toString());
		return result;
	}
	
	@PostMapping("/getRecommendedChunk")
	public long getRecommendedChunk(HttpSession session, @RequestBody List<Object> hosts) {
		int cores = 0;
		BigCH ch = (BigCH) session.getAttribute("ch");
		JSONArray jsaHosts = new JSONArray(hosts);
		for (int i=0; i<jsaHosts.length(); i++)
			cores+= jsaHosts.getJSONObject(i).getInt("cores");
		if (cores==0)
			return 0;
		
		BigInteger combinations = ch.getNumberOfCombinations();
		BigInteger chunk = combinations.divide(BigInteger.valueOf((long) cores*cores));
		
		if (chunk.compareTo(BigInteger.ZERO)==0)
			chunk = combinations.divide(BigInteger.valueOf(cores));
		
		if (chunk.compareTo(BigInteger.ZERO)==0)
			chunk = combinations;
		return chunk.longValue();
	}
	
	@GetMapping("/getMore/{hostId}")
	public Map<String, Object> getMore(HttpSession session, @PathVariable String hostId) {
		Cluster cluster = (Cluster) session.getAttribute("cluster");
		Map<String, Object> result = new HashMap<>();
		result.put("host", cluster.getMore(hostId));
		result.put("remainingTime", cluster.getRemainingTime());
		return result;
	}
	
	@GetMapping("/getSpeed/{hostId}")
	public String getSpeed(HttpSession session, @PathVariable String hostId) {
		Cluster cluster = (Cluster) session.getAttribute("cluster");
		return formatter.format(cluster.getSpeed(hostId));
	}
	
	@GetMapping("/getPercentage")
	public double getPercentage(HttpSession session) {
		Cluster cluster = (Cluster) session.getAttribute("cluster");
		return cluster.getPercentage();
	}
	
	@PostMapping("/startSC")
	public String startSC(HttpSession session, @RequestBody Map<String, Object> info) {
		Manager.get().clearFolder();
		JSONObject jsoInstance = new JSONObject(info);
		session.setAttribute("jsoInstance", jsoInstance);
		return session.getId();
	}
	
	@GetMapping("/calculateSC")
	public List<List<String>> calculateWithSortedConstraints(HttpSession session, @RequestParam String orderType, @RequestParam String hostConfigurationId) {
		List<Host> hosts = hostsDao.getActiveHosts(hostConfigurationId);
		BigGH gh = (BigGH) session.getAttribute("gh");
		JSONObject jsoInstance = (JSONObject) session.getAttribute("jsoInstance");
		session.removeAttribute("jsoInstance");
		
		SCCalculatorService service = new SCCalculatorService(orderType);
		service.setFolder("sm");
		service.setHttpSessionId(session.getId());
		try {
			return service.prepareHosts(hosts, gh, jsoInstance);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
}
