package edu.uclm.bigintq.http;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import edu.uclm.bigintq.dao.JBigGHDAO;
import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.g.BigGH;
import edu.uclm.bigintq.model.g.BigGIndexedSummation;
import edu.uclm.bigintq.model.g.BigGSummation;
import edu.uclm.bigintq.model.g.BigGVariable;
import edu.uclm.bigintq.model.g.JBigGH;
import edu.uclm.bigintq.model.simplifier.CHSimplifier;

@RestController
@RequestMapping("problems")
public class ProblemsController {
	
	@Autowired
	private JBigGHDAO bigGhDao;
	
	public static final DecimalFormat formatter = new DecimalFormat("###,###,###,###");
	
	@GetMapping("/get")
	public String get(HttpSession session) {
		return session.getId();
	}
	
	@PostMapping(value = "/instantiate")
	public List<String> instantiate(HttpSession session, @RequestParam(required=false) boolean forQA, @RequestBody Map<String, Object> info) {
		try {
			JSONObject jso = new JSONObject(info);
			
			BigGH gh = (BigGH) session.getAttribute("gh");
			BigCH ch = gh.instantiate(jso, forQA);
			BigCH chQUBO = gh.instantiate(jso, forQA);
			//ch.sortExpressionsByLambda("GX");
			session.setAttribute("ch", ch);
			session.setAttribute("chQUBO", chQUBO);
			Manager.get().putHttpSession(session.getId(), session);
			List<String> result = ch.toMathML();
			result.add(formatter.format(ch.getNumberOfCombinations()));
			return result;
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	
	@PostMapping("/getPrefixedX")
	public List<List<Integer>> getPrefixedX(HttpSession session, @RequestBody Map<String, Object> info) {
		try {
			JSONObject jso = new JSONObject(info);
			
			BigGH gh = (BigGH) session.getAttribute("gh");
			BigCH ch = gh.instantiate(jso, true);
			return ch.getX().getListOfCoordinates();
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	
	@PutMapping("/saveProblem")
	public void saveProblem(HttpSession session, @RequestBody Map<String, Object> info) {
		try {
			BigGH gh = (BigGH) session.getAttribute("gh");
			if (gh==null) {
				gh = new BigGH();
				session.setAttribute("gh", gh);
			}

			JSONObject jso = new JSONObject(info);
			String problemName = jso.optString("problemName");
			gh.setProblemName(problemName);
			
			String parameters = jso.optString("parameters");
			gh.setParameters(parameters);
					
			JSONArray variables = jso.optJSONArray("variables");
			gh.setVariables(variables);

			String xCardinals = jso.optString("xCardinals");
			gh.setXCardinals(xCardinals);
			
			String text = gh.toJSON().toString();
			JBigGH jBigGH = new JBigGH(problemName, text);
			this.bigGhDao.save(jBigGH);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	
	@GetMapping(value = "/getProblemNames", produces = "application/json")
	public List<String> getProblemNames(HttpSession session) {
		return this.bigGhDao.getProblemNames();
	}
	
	@GetMapping(value = "/getProblem", produces = "application/json")
	public JBigGH getProblem(HttpSession session, @RequestParam(required = false) String problemName) {
		if (problemName==null) {
			BigGH gh = (BigGH) session.getAttribute("gh");
			if (gh==null)
				return null;
			problemName = gh.getProblemName();
		}
		Optional<JBigGH> optProblem = this.bigGhDao.findById(problemName);
		if (optProblem.isPresent()) {
			JBigGH jgh = optProblem.get();
			BigGH gh;
			BigGH ghQUBO;
			try {
				gh = new BigGH(jgh.toJSON());
				ghQUBO = new BigGH(jgh.toJSON());
			} catch (Exception e) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
			}
			session.setAttribute("gh", gh);
			session.setAttribute("ghQUBO", ghQUBO);
			return optProblem.get();
		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Problem '" + problemName + "' not found");
	}
	
	@GetMapping("/getProblemInstance")
	public List<String> getProblemInstance(HttpSession session) {
		BigCH ch = (BigCH) session.getAttribute("ch");
		if (ch==null)
			return null;
		return ch.toMathML();
	}
	
	@PostMapping(value = "/checkNewRule", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String checkNewRule(HttpSession session, @RequestBody Map<String, Object> info) {
		BigGH gh = (BigGH) session.getAttribute("gh");
		try {
			if (gh==null) {
				gh = new BigGH();
				session.setAttribute("gh", gh);
			}
			
			JSONObject jso = new JSONObject(info);
			
			String type = jso.getString("type");
			if (!type.equals("IndexedSummation"))
				throw new Exception("The expected type of the expression is 'IndexedSummation'");
			
			JSONObject from = jso.optJSONObject("from");
			if (from==null)
				throw new Exception("Expected 'from' in the summation");
			
			JSONObject to = jso.optJSONObject("to");
			if (to==null)
				throw new Exception("Expected 'to' in the summation");
			
			JSONObject body = jso.optJSONObject("body");
			JSONObject next = jso.optJSONObject("next");
			if (body==null && next==null)
				throw new Exception("Expected either 'body' or 'next' in the summation");
			BigGIndexedSummation bis = new BigGIndexedSummation(jso, null, gh);			
			
			gh.addSummation(bis);
			String r = gh.toJSON().toString();
			return r;
		} catch (Exception e) {
			gh.removeLastExpression();
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	
	@GetMapping("/getMatrix")
	public String getMatrix(HttpSession session, @RequestParam String format) {
		try {
			BigCH ch = (BigCH) session.getAttribute("ch");
			if (ch!=null) {
				CHSimplifier simplifier = new CHSimplifier(ch);
				simplifier.simplify();
				if (format.equalsIgnoreCase("math"))
					return simplifier.getMathML();
				else if (format.equalsIgnoreCase("json"))
					return simplifier.toJSON().toString();
				else if (format.equalsIgnoreCase("text"))
					return simplifier.toText();
				throw new Exception("Unrecognized requested format");
			}
			else
				throw new Exception("There is not any function to build the matrix");
		} catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	
	@PostMapping("/randomValues")
	public String randomValues(HttpSession session, @RequestParam String name, @RequestParam int min, @RequestParam int max,  @RequestBody List<Object> info) {
		BigGH gh = (BigGH) session.getAttribute("gh");
		BigGVariable variable = gh.findVariable(name);
		JSONArray jsaParameters = new JSONArray(info);
		return variable.generateRandomValues(min, max, jsaParameters);
	}
	
	@GetMapping("/clear")
	public void clear(HttpSession session) {
		BigGH gh = (BigGH) session.getAttribute("gh");
		if (gh!=null)
			gh.clear();
		session.removeAttribute("ch");
		session.removeAttribute("chQUBO");
		session.removeAttribute("gh");
		session.removeAttribute("ghQUBO");
	}
	
	@GetMapping("/toEquality/{index}")
	public String toEquality(HttpSession session, @PathVariable int index) {
		BigGH gh = (BigGH) session.getAttribute("gh");
		return gh.toEquality(index).toJSON().toString();
	}
	
	@GetMapping("/getRule/{index}")
	public String getRule(HttpSession session, @PathVariable int index) {
		BigGH gh = (BigGH) session.getAttribute("gh");
		BigGSummation expr = gh.getExpressions().get(index);
		StringBuilder sb = new StringBuilder();
		sb.append("<math xmlns='http://www.w3.org/1998/Math/MathML' display='block'>");
		sb.append(expr.toMathML());
		sb.append("</math>");
		return sb.toString();
	}
	
	@DeleteMapping("/remove/{index}")
	public void remove(HttpSession session, @PathVariable int index) {
		BigGH gh = (BigGH) session.getAttribute("gh");
		gh.removeExpression(index);
	}
	
	@PostMapping("/detach")
	public List<Double> detach(HttpSession session, @RequestBody String info) {
		BigCH h = (BigCH) session.getAttribute("ch");
		return h.detach(info);
	}
}
