package edu.uclm.bigintq.so;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.socket.WebSocketSession;

import edu.uclm.bigintq.websockets.WSDWave;

public class ProcessExecutor implements Runnable {

	private String directory;
	private String classPath;
	private String[] commands;
	private JSONObject executionResult;
	private JSONArray resultFiles;
	private WebSocketSession session;
	
	private static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddhhmmss");
	
	public ProcessExecutor(String directory) {
		this.directory = directory.replace('\\', '/');
		if (!this.directory.endsWith("/"))
			this.directory = this.directory + "/";
		this.executionResult = new JSONObject();
	}
	
	public void execute(WebSocketSession session, String classPath, String... commands) {
		this.session = session;
		this.classPath = classPath;
		this.commands = commands;
		this.start();
	}
	
	public void start() {
		Thread thread = new Thread(this);
		thread.start();
	}
	
	@Override
	public void run() {
		ProcessBuilder pb=new ProcessBuilder(this.commands);
		
		Map<String, String> env = pb.environment();
		env.put("PATH", this.classPath);
		//env.put("PATH", "/Users/macariopolousaola/opt/anaconda3/bin:/Users/macariopolousaola/opt/anaconda3/condabin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:.:/Library/Apple/usr/bin");
		
		File file = new File(directory);
		pb.directory(file);
		
		Date date = new Date();
		String fileName = dateFormatter.format(date);
		File outputFile = new File(this.directory + fileName + ".output.txt");
		File errorsFile = new File(this.directory + fileName + ".errors.txt");
		pb.redirectOutput(outputFile);
		pb.redirectError(errorsFile);
		
		Process process;
		int returnCode = 0;
		try {
			process = pb.start();
			returnCode=process.waitFor();
		} catch (Exception e) {
			executionResult.put("returnCode", -1);
			executionResult.put("errorLines", new JSONArray().put(e.getMessage()));
			sendResult();
			return;
		}
		
		JSONArray outputLines=new JSONArray();
		try(FileReader fr=new FileReader(outputFile)) {  
			try(BufferedReader br=new BufferedReader(fr)) {
				String line=null;
				while ((line=br.readLine())!=null && !line.equals("SOLUTIONS"))
					;
				int cont = 0;
				while ((line=br.readLine())!=null) {
					outputLines.put(line);
					if (cont++>=100)
						break;
				}
			}
		} catch (Exception e) {
			executionResult.put("returnCode", -1);
			executionResult.put("errorLines", new JSONArray().put(e.getMessage()));
			sendResult();
			return;
		} 

		JSONArray errorLines=new JSONArray();
		try(FileReader fr=new FileReader(errorsFile)) {
			try(BufferedReader br=new BufferedReader(fr)) {
				String line=null;
				while ((line=br.readLine())!=null)
					errorLines.put(line);
			}
		} catch (Exception e) {
			executionResult.put("returnCode", -1);
			executionResult.put("errorLines", new JSONArray().put(e.getMessage()));
			sendResult();
			return;
		}
		
		executionResult.put("returnCode", returnCode);
		executionResult.put("outputLines", outputLines);
		executionResult.put("errorLines", errorLines);
		sendResult();
	}
	
	private void sendResult() {
		WSDWave.send(this.session, this.executionResult.toString());
	}
}