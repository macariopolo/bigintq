package edu.uclm.bigintq.dwave;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class DWaveCoupler {
	
	@Id @Column(length = 36)
	private String id;
	private int a;
	private int b;
	@ManyToOne
	private DWaveSolver solver;
	
	public DWaveCoupler() {
		this.id = UUID.randomUUID().toString();
	}

	public void setSolver(DWaveSolver solver) {
		this.solver = solver;
	}

	public void setA(int a) {
		this.a = a;
	}
	
	public void setB(int b) {
		this.b = b;
	}

	public String getId() {
		return id;
	}

	public DWaveSolver getSolver() {
		return solver;
	}

	public int getA() {
		return a;
	}

	public int getB() {
		return b;
	}

}
