package edu.uclm.bigintq.dwave;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Map;

import org.springframework.web.socket.WebSocketSession;

import edu.uclm.bigintq.http.Manager;
import edu.uclm.bigintq.so.ProcessExecutor;

public class Runner {
	
	public static void main(String[] args) throws Exception {
		try(FileInputStream fis = new FileInputStream("/Users/macariopolousaola/Downloads/Optimizacion/ProblemasOptimizacion-CodigoDWave/Manolo.py")) {
			byte[] b = new byte[fis.available()];
			fis.read(b);
			String classPath = "/Users/macariopolousaola/opt/anaconda3/bin:/Users/macariopolousaola/opt/anaconda3/condabin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:.:/Library/Apple/usr/bin";
			String code = new String(b);
			run(null, classPath, code);
		}
	}

	public static void run(WebSocketSession session, String classPath, String pythonCode) throws Exception {
		String workingDirectory = Manager.get().createFolder(null, true);
		ProcessExecutor pe = new ProcessExecutor(workingDirectory);
		String fileName = workingDirectory + "/test.py";
		try(FileOutputStream fos = new FileOutputStream(fileName)) { 
			fos.write(pythonCode.getBytes());
			String[] commands = {
					"bash", "-c",
					"pip install dwave-system ; python " + fileName
				};
			pe.execute(session, classPath, commands);
		}
	}

}
