package edu.uclm.bigintq.dwave;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

@Entity
public class DWaveSolver {
	@Id
	private String id;	
	private String description;
	private int qubits;
	
	@OneToMany
	private List<DWaveCoupler> couplers;

	@Column(columnDefinition = "LONGTEXT")
	@Lob
	private String info;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getInfo() {
		return info;
	}
	
	public void setInfo(String info) {
		this.info = info;
	}

	public void setDescription(String description) {
		this.description = description;
	}	
	
	public String getDescription() {
		return description;
	}
	
	public void setQubits(int qubits) {
		this.qubits = qubits;
	}
	
	public int getQubits() {
		return qubits;
	}
	
	public void setCouplers(List<DWaveCoupler> couplers) {
		this.couplers = couplers;
	}
	
	public List<DWaveCoupler> getCouplers() {
		return couplers;
	}
}
