package edu.uclm.bigintq.dwave;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;

import edu.uclm.bigintq.http.Manager;
import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigParameter;
import edu.uclm.bigintq.model.BigVariable;

public class PyGen {

	public static String[] getCode(BigCH ch, JSONArray jsaMatrix) throws FileNotFoundException, IOException {
		String template = Manager.get().readAsText("dwaveTemplate.txt");
		
		Collection<BigParameter> pars = ch.getParameters().values();
		StringBuilder sb = new StringBuilder("#PARAMETERS#\n");
		for (BigParameter par : pars)
			sb.append(par.getName() + " = " + par.getValue() + "\n");
		template = template.replace("#PARAMETERS#", sb.toString());	
		
		sb = new StringBuilder("#VARIABLES#\n");
		Collection<BigVariable> vars = ch.getVariables().values();
		for (BigVariable bv : vars) {
			sb.append(bv.getName() + " = [");
			ArrayList<Double> values = bv.getValues();
			for (int i=0; i<values.size(); i++) {
				sb.append(values.get(i));
				if (i<values.size()-1)
					sb.append(", ");
				if (i>0 && i%8==0)
					sb.append("\\\n    ");
			}
			sb.append("]\n");
		}
		template = template.replace("#VARIABLES#", sb.toString());
		
		
		sb = new StringBuilder("#X#\n");
		sb.append("x = []\n");
		JSONArray aliases = jsaMatrix.getJSONArray(0);
		sb.append("for i in range (0, " + (aliases.length()-1) + "):\n");
		sb.append("\tx.append(\"x\" + str(i))\n");
		template = template.replace("#X#", sb.toString());
		
		String var0, var1;
		sb = new StringBuilder("#RULES#\n");
		for (int i=1; i<jsaMatrix.length(); i++) {
			var0 = "x" + (i-1);
			JSONArray fila = jsaMatrix.getJSONArray(i);
			for (int j=i; j<fila.length(); j++) {
				var1 = "x" + (j-1);
				sb.append("Q[(\"" + var0 + "\", \"" + var1 + "\")] = " + fila.getDouble(j) + "\n");
			}
		}
		template = template.replace("#RULES#", sb.toString());
		
		return template.split("\n");
	}

}
