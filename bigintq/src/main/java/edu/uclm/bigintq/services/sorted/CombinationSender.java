package edu.uclm.bigintq.services.sorted;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.bigintq.entities.Host;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.websockets.IWSClientReceiver;
import edu.uclm.bigintq.websockets.WSClient;

public abstract class CombinationSender implements IWSClientReceiver {
	
	protected SCCalculatorService service;
	protected int calculateds;

	public CombinationSender(SCCalculatorService service) {
		this.service = service;
	}
	
	protected final List<Integer> fromJSON(JSONArray combination) {
		List<Integer> result = new ArrayList<>();
		Object value;
		for (int i=0; i<combination.length(); i++) {
			value = combination.get(i);
			if (value.equals(JSONObject.NULL))
				result.add(null);
			else
				result.add((Integer) value);
		}
		return result;
	}
	
	public final List<BigExpression> getExpressions() {
		return this.service.getCH().getExpressions();
	}

	public final ArrayList<Integer> getLambdaIndexes() {
		return this.service.getLambdaIndexes();
	}
	
	public final ArrayList<Integer> getOfIndexes() {
		return this.service.getOfIndexes();
	}
	
	public final int incrCurrentExpressionIndex() {
		return this.service.incrCurrentExpressionIndex();
	}
	
	public final BigInteger incrSolutionsCounter() {
		return this.service.incrSolutionsCounter();
	}
	
	public final BigInteger getSolutionsCounter() {
		return this.service.getSolutionsCounter();
	}
	
	public final List<WSClient> getWsClients() {
		return this.service.getWsClients();
	}
	
	public final String getFolder() {
		return this.service.getFolder();
	}
	
	public final int getCurrentExpressionIndex() {
		return this.service.getCurrentExpressionIndex();
	}
	
	public final List<Host> getHosts() {
		return this.service.getHosts();
	}
	
	public final void changeReceiver(IWSClientReceiver receiver) {
		List<WSClient> clients = this.service.getWsClients();
		for (WSClient client : clients)
			client.setReceiver(receiver);
	}
}
