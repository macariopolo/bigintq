package edu.uclm.bigintq.services;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.NumberFormat;

import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class WrapperHost implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id;
	private BigInteger start;
	private BigInteger end;
	private BigInteger total;
	
	private BigInteger cores;
	private boolean finished;
	private long timeIni;
	private int moreRequests;

	private double speed;

	private boolean ask;

	private BigInteger chunk;
	
	private static final NumberFormat formatter = NumberFormat.getNumberInstance();

	public WrapperHost(JSONObject jso) {
		this.id = jso.getString("host");
		this.cores = BigInteger.valueOf(jso.getInt("cores"));
		this.total = BigInteger.ZERO;
		this.timeIni = System.currentTimeMillis();
	}
		
	public BigInteger initialDistribution(BigInteger combinations, BigInteger[] remainder, BigInteger chunk) {
		this.chunk = chunk.multiply(cores);
		
		//if (this.chunk.compareTo(BigInteger.valueOf(100_000))>0)
		//	this.chunk = BigInteger.valueOf(100_000);
		
		this.end = this.start.add(this.chunk);
		if (this.end.compareTo(remainder[0])>=0)
			this.end = combinations;
		
		remainder[0] = combinations.subtract(this.end);
		
		this.total = this.total.add(this.end.subtract(this.start));	
		return this.end;
	}
	
	public void calculateSpeed() {
		long time = System.currentTimeMillis() - this.timeIni;
		this.speed = 1000 * this.total.doubleValue() / time;			
	}
	
	public BigInteger getMore(BigInteger combinations, BigInteger[] remainder) {
		this.end = this.start.add(this.chunk);
		if (remainder[0].compareTo(BigInteger.ZERO)<=0)
			this.end = combinations;
		
		remainder[0] = combinations.subtract(this.end);
		
		this.total = this.total.add(this.end.subtract(this.start));
		
		return this.end;
	}

	public String getId() {
		return this.id;
	}

	public void setStart(BigInteger start) {
		this.start = start;
	}

	public String getStart() {
		return start.toString();
	}
	
	public String getEnd() {
		return end.toString();
	}

	@JsonIgnore
	public BigInteger getEndAsBI() {
		return this.end;
	}

	@JsonIgnore
	public BigInteger getStartAsBI() {
		return this.start;
	}
	
	public String getTotal() {
		return formatter.format(this.total);
	}

	public void finish() {
		this.finished = true;
	}
	
	public long getSpeed() {
		return (long) this.speed;
	}
	
	public boolean isFinished() {
		return finished;
	}

	public double requestMore() {
		return ++this.moreRequests;
	}

	public void setAsk(boolean ask) {
		this.ask = ask;
	}
	
	public boolean isAsk() {
		return ask;
	}

	public double getMoreRequests() {
		return this.moreRequests;
	}
	
	public String getChunk() {
		return formatter.format(this.chunk).toString();
	}

}
