package edu.uclm.bigintq.services.sorted;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.bigintq.websockets.WSClient;

public class OtherCombinationSender extends CombinationSender {

	private Iterator<File> files;
	private int emptyPositions;
	private BigInteger servers;
	private BigInteger combinations;
	private BigInteger chunk;
	private double bestEnergy;
	
	public OtherCombinationSender(SCCalculatorService service) {
		super(service);
		this.bestEnergy = Double.MAX_VALUE;
	}

	public void calculate() {
		if (this.files.hasNext()) {
			File file = files.next();
			List<Integer> combination = this.readCombination(file);
			this.send(combination);
		} else {
			this.moveFromTemp();
			JSONObject jso = new JSONObject().
					put("action", "CALCULATE OTHER");
			this.service.onResponseReceived(jso);
		}
	}

	@SuppressWarnings("unchecked")
	private List<Integer> readCombination(File file) {
		List<Integer> combination=null;
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
			combination = (List<Integer>) ois.readObject();
		} catch(Exception e) {
			System.err.println(e);
		} finally {
			//file.delete();
		}
		file.renameTo(new File(file.getAbsolutePath() + ".sent"));
		
		if (emptyPositions==0) {
			for (int i=0; i<combination.size(); i++)
				if (combination.get(i)==null)
					emptyPositions++;
			combinations = BigInteger.valueOf(2).pow(emptyPositions);
			if (combinations.compareTo(servers)==-1)
				servers = combinations;
			this.chunk = combinations.divide(servers);
		}
		return combination;
	}

	private void send(List<Integer> combination) {
		
		BigInteger start = BigInteger.ZERO;
		BigInteger end = start;
		
		JSONObject jso = new JSONObject().
			put("type", "CALCULATE");
		
		List<WSClient> wsClients = this.getWsClients();
		WSClient client;
		int nServers = this.servers.intValue();
		for (int i=0; i<nServers; i++) {
			client = wsClients.get(i);
			if (i<wsClients.size()-1)
				end = end.add(this.chunk);
			else
				end = combinations;
			jso.put("start", new String(start.toString())).
				put("end", new String(end.toString())).
				put("constraintIndex", this.getCurrentExpressionIndex()).
				put("combination", this.toJSONArray(combination));
			client.sendMessage(jso);
			start = start.add(this.chunk);
			end = end.add(this.chunk);
		}
	}

	private synchronized void moveFromTemp() {
		String sParent = this.getFolder();
		String sTemp = this.getFolder() + "temp" + File.separatorChar;
		File temp = new File(sTemp);
		String[] extensions = { "combination" };
		Iterator<File> files = FileUtils.iterateFiles(temp, extensions, false);
		File file;
		String fileName;
		File dest;
		while (files.hasNext()) {
			file = files.next();
			fileName = file.getName();
			dest = new File(sParent + fileName);
			try {
				FileUtils.moveFile(file, dest);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			FileUtils.cleanDirectory(temp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadFiles() {
		this.emptyPositions=0;
		this.bestEnergy = Double.MAX_VALUE;
		this.servers = BigInteger.valueOf(this.getWsClients().size());
		File directory = new File(this.getFolder());
		String[] extensions = { "combination" };
		this.files = FileUtils.iterateFiles(directory, extensions, false);
	}

	@Override
	public void onResponseReceived(JSONObject response) {
		String action = response.getString("action"); 
		if (action.equals("CALCULATED")) {
			if (response.getString("subaction").equals("combination")) {
				double energy = response.getDouble("value");
				if (energy<=bestEnergy) {
					if (energy<bestEnergy) {
						bestEnergy = energy;
						String temp = this.getFolder() + "temp";
						try {
							File f = new File(temp);
							if (f.exists())
								FileUtils.cleanDirectory(f);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					this.save(response.getJSONArray("combination"), energy);
				}
			} else {
				this.calculateds++;
				if (calculateds==this.servers.intValue()) {
					this.calculateds=0;
					this.calculate();
				}
			}
		}
	}
	
	private void save(JSONArray combination, double value) {
		String temp = this.getFolder() + "temp";
		new File(temp).mkdir();
		String fileName = temp + File.separatorChar + combination.toString() + ".combination";
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
			List<Integer> data = this.fromJSON(combination);
			oos.writeObject(data);
		} catch (Exception e) {
			System.err.println(e);
		}
		this.incrSolutionsCounter();
	}
	
	private JSONArray toJSONArray(List<Integer> combination) {
		JSONArray jsa = new JSONArray();
		for (Integer value : combination)
			jsa.put(value);
		return jsa;
	}
}
