package edu.uclm.bigintq.services.sorted;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.BigLambdadedExpression;
import edu.uclm.bigintq.model.BigSum;
import edu.uclm.bigintq.websockets.WSClient;

public class FirstCombinationSender extends CombinationSender {
	public FirstCombinationSender(SCCalculatorService service) {
		super(service);
	}

	@Override
	public void onResponseReceived(JSONObject response) {
		String action = response.getString("action");
		if (action.equals("CALCULATED")) {
			if (response.optString("subaction").equals("finish")) {
				this.calculateds++;
			} else
				this.save(response.getJSONArray("combination"));
			if (this.calculateds==this.getHosts().size()) {
				this.calculateds = 0;
				JSONObject jso = new JSONObject().
						put("action", "CALCULATE OTHER");
				this.service.onResponseReceived(jso);
			}	
		}
	}

	public void calculate() {
		this.changeReceiver(this);
		
		List<BigExpression> expressions = this.getExpressions();
				
		int n = expressions.size();
		
		for (int i=0; i<n; i++) {
			BigLambdadedExpression expr = (BigLambdadedExpression) expressions.get(i);
			if (expr.getLambda()==0.0 || expr instanceof BigSum)
				this.getOfIndexes().add(i);
			else
				this.getLambdaIndexes().add(i);
		}
		
		BigExpression expr = expressions.get(this.service.getCurrentExpressionIndex());
		
		expr.calculateEmptyCombination();
		
		BigInteger combinations = expr.getNumberOfCombinations();
		
		List<WSClient> wsClients = this.getWsClients();
		BigInteger chunk = combinations.divide(BigInteger.valueOf(wsClients.size()));		
		BigInteger start = BigInteger.ZERO;
		BigInteger end = start;
		
		WSClient client;
		JSONObject jso = new JSONObject().
			put("type", "CALCULATE");
		for (int i=0; i<wsClients.size(); i++) {
			client = wsClients.get(i);
			if (i<wsClients.size()-1)
				end = end.add(chunk);
			else
				end = combinations;
			jso.put("start", new String(start.toString())).
				put("end", "" + new String(end.toString())).
				put("constraintIndex", this.service.getCurrentExpressionIndex());
			client.sendMessage(jso);
			start = start.add(chunk);
			end = end.add(chunk);
		}
	}
	
	protected final void save(JSONArray combination) {
		String fileName = this.getFolder() + File.separatorChar + combination.toString() + ".combination";
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
			List<Integer> data = this.fromJSON(combination);
			oos.writeObject(data);
		} catch (Exception e) {
			System.err.println(e);
		}
		this.incrSolutionsCounter();
	}


}
