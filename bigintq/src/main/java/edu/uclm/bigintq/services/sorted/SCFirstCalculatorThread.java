package edu.uclm.bigintq.services.sorted;

import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigExpression;

public class SCFirstCalculatorThread extends SCCalculatorThread {
	
	public SCFirstCalculatorThread(SCCalculatorService service, int core, BigCH ch, int constraintIndex, BigInteger start, BigInteger end) throws FileNotFoundException {
		super(service, core, ch, constraintIndex, start, end);
	}

	@Override
	public void run() {
		//System.out.println("--- Inicio core first " + core + ", start=" + start + ", end = " + end + " ---");
		BigInteger counter = start;
		double energy;
		BigExpression expr = ch.getExpressions().get(constraintIndex);
		List<Integer> combination = new ArrayList<>();
		while (counter.compareTo(this.end)==-1) {
			combination = expr.getCombination(counter);
			energy = Math.abs(expr.getValue(combination));
			//System.out.print(toString(combination, energy));
			if (energy<=bestEnergy) {
				if (energy<bestEnergy) {
					deleteFolder();
					bestEnergy = energy;
				}
				this.write(combination, bestEnergy);
			}
			counter = counter.add(BigInteger.ONE);
		} 
	}
}
