package edu.uclm.bigintq.services.sorted;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

import org.apache.commons.io.FileUtils;

import edu.uclm.bigintq.model.BigCH;

public abstract class SCCalculatorThread implements Runnable {

	protected SCCalculatorService service;
	protected BigCH ch;
	protected BigInteger start, end;
	protected int constraintIndex;
	protected int core;
	protected String folder;
	protected double bestEnergy = Double.MAX_VALUE;
	protected SecureRandom dado;
	
	public SCCalculatorThread(SCCalculatorService service, int core, BigCH ch, int constraintIndex, BigInteger start, BigInteger end) throws FileNotFoundException {
		this.service = service;
		this.core = core;
		this.ch = ch;
		this.constraintIndex = constraintIndex;
		this.end = end;
		this.start = start;
		this.dado = new SecureRandom();
	}
	
	protected final String toString(List<Integer> combination, double energy) {
		String r = "Core " + this.core + ": ";
		for (Integer v : combination)
			if (v==null)
				r = r + "-";
			else
				r = r + v;
		r = r + " -> " + energy;
		return r;
	}
	
	protected final void deleteFolder() {
		try {
			FileUtils.cleanDirectory(new File(this.folder));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected final void write(List<Integer> combination, double energy) {
		String data = combination.toString() + "-" + energy;
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(this.folder + data + ".combination"))) {
			oos.writeObject(combination);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected final void print(List<Integer> combination, double energy) {
		System.out.print("\t");
		for (Integer v : combination)
			System.out.print(v + "\t");
		System.out.println(energy);
	}
	
	public final void setFolder(String folder) throws FileNotFoundException {
		this.folder = folder;
	}
	
	public final double getBestEnergy() {
		return bestEnergy;
	}
}
