package edu.uclm.bigintq.services;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Cluster implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private HashMap<String, WrapperHost> hosts;
	private HashMap<String, Double> moreRequests;
	private Vector<WrapperHost> hostsList;	
	
	private int cores;
	private BigInteger last;
	private BigInteger[] remainder;
	private BigInteger combinations;
	private boolean finished;
	private double percentage;

	private long remainingTime;
	
	public Cluster() {
		this.hosts = new HashMap<>();
		this.moreRequests = new HashMap<>();
		this.hostsList = new Vector<>();
		this.remainder = new BigInteger[1];
	}

	public void distribute(JSONArray jsaHosts) {
		WrapperHost host;
		JSONObject jso;
		
		this.last = BigInteger.ZERO;
		this.remainder[0] = combinations;
		
		BigInteger chunk = BigInteger.valueOf(jsaHosts.getJSONObject(0).optLong("chunk"));
		if (chunk.compareTo(BigInteger.ZERO)==0)
			chunk = combinations.divide(BigInteger.valueOf((long) cores*cores));
		
		if (chunk.compareTo(BigInteger.ZERO)==0)
			chunk = combinations.divide(BigInteger.valueOf(cores));
		
		if (chunk.compareTo(BigInteger.ZERO)==0)
			chunk = combinations;
		
		for (int i=0; i<jsaHosts.length(); i++) {
			jso = jsaHosts.getJSONObject(i);
			host = new WrapperHost(jso);
			host.setStart(this.last);
			this.last = host.initialDistribution(this.combinations, this.remainder, chunk);
			this.last = this.last.add(BigInteger.ONE);
			this.hosts.put(host.getId(), host);
			this.moreRequests.put(host.getId(), 0.0);
			this.hostsList.add(host);
			if (remainder[0].compareTo(BigInteger.ZERO)==0) {
				break;
			}
		}
		this.percentage = 100 * ((this.last.doubleValue() - 1.0) / this.combinations.doubleValue());
	}
	
	public synchronized WrapperHost getMore(String hostId) {
		WrapperHost host = this.hosts.get(hostId);
		if (this.remainder[0].compareTo(BigInteger.ZERO)==0) {
			host.finish();
			return host;
		}
		host.setStart(this.last);
		
		host.calculateSpeed();
		double meanSpeed = this.getMeanSpeed();
		this.remainingTime = (long) (remainder[0].longValue()/meanSpeed);

		/*this.hostsList.sort((o1, o2) -> (int) (o2.getSpeed()-o1.getSpeed()));
		
		int i = this.hostsList.size()-1;
		WrapperHost slowestHost;
		double slowestSpeed; // = this.getMeanSpeed();
		do {
			slowestHost = this.hostsList.get(i--);
			slowestSpeed = slowestHost.getSpeed();
		} while (slowestSpeed==0.0);
		
		double hostSpeed = host.getSpeed();
		
		long ratio = (long) (hostSpeed / slowestSpeed);*/
		this.last = host.getMore(this.combinations, this.remainder);
		
		host.setAsk(false);
		if (!this.finished && this.last.compareTo(this.combinations)>=0) {
			this.finished = true;
			host.setAsk(true);
		}

		if (this.finished)
			host.finish();

		this.percentage = 100 * (this.last.doubleValue() / this.combinations.doubleValue());
		this.last = this.last.add(BigInteger.ONE);
		return host;
	}
	
	public long getRemainingTime() {
		return remainingTime;
	}
	
	private double getMeanSpeed() {
		double meanSpeed = 0;
		for (WrapperHost host : this.hostsList)
			meanSpeed = meanSpeed + host.getSpeed();
		return meanSpeed/this.hostsList.size();
	}

	public void setCores(int cores) {
		this.cores = cores;
	}

	public void setLast(BigInteger last) {
		this.last = last;
	}
		
	@JsonIgnore
	public BigInteger getLast() {
		return last;
	}
	
	public Collection<WrapperHost> getHosts() {
		return hosts.values();
	}

	public void setCombinations(BigInteger combinations) {
		this.combinations = combinations;
	}
	
	public double getPercentage() {
		return percentage;
	}

	public long getSpeed(String hostId) {
		WrapperHost host = this.hosts.get(hostId);
		return host.getSpeed();
	}
	
}
