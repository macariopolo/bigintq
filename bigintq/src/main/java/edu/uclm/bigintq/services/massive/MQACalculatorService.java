package edu.uclm.bigintq.services.massive;

import java.io.IOException;
import java.math.BigInteger;

import org.json.JSONObject;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import edu.uclm.bigintq.model.BigCH;

public class MQACalculatorService {

	private WebSocketSession webSocketSession;

	public void calculate(BigCH ch, BigInteger start, BigInteger end) {
		int cores = 1; //Runtime.getRuntime().availableProcessors();

		BigInteger combinations = end.subtract(start);
		if (combinations.compareTo(BigInteger.valueOf(cores))==-1)
			cores = combinations.intValue();
		
		BigInteger chunk = combinations.divide(BigInteger.valueOf(cores));
		
		MQACalculatorThread[] cctt = new MQACalculatorThread[cores];
		Thread[] tt = new Thread[cores];
		BigInteger currentLoadStart = start;
		BigInteger currentLoadEnd = BigInteger.ZERO; 
		
		for (int i=0; i<cores; i++) {
			if (i==cores-1)
				currentLoadEnd = start.add(combinations);
			else
				currentLoadEnd = currentLoadStart.add(chunk);
			MQACalculatorThread ct = new MQACalculatorThread(this, i+1, ch, currentLoadStart, currentLoadEnd);
			cctt[i] = ct;
			tt[i] = new Thread(ct);
			currentLoadStart = currentLoadStart.add(chunk);
		}
				
		for (int i=0; i<cores; i++)
			tt[i].start();
		
		for (int i=0; i<cores; i++) {
			try {
				tt[i].join();
			} catch (Exception e) {
				System.err.println(e);
			}
		}
	}

	public void setWebSocketSession(WebSocketSession session) {
		this.webSocketSession = session;
	}

	public synchronized void setSolution(JSONObject solution) {
		try {
			this.webSocketSession.sendMessage(new TextMessage(solution.toString()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
