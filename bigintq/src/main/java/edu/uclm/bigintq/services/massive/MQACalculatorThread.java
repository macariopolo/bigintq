package edu.uclm.bigintq.services.massive;

import java.math.BigInteger;
import java.util.Map;

import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;

public class MQACalculatorThread implements Runnable {

	private int index;
	private BigCH ch;
	private BigInteger start;
	private BigInteger end;
	private MQACalculatorService calculatorService;
	private JSONObject solution;

	public MQACalculatorThread(MQACalculatorService calculatorService, int index, BigCH ch, BigInteger start, BigInteger end) {
		this.calculatorService = calculatorService;
		this.index = index;
		this.ch = ch;
		this.start = start; 
		this.end = end;
		this.solution = new JSONObject().
			put("core", this.index).
			put("massive", true);
	}

	@Override
	public void run() {
		BigInteger counter = start;
		double energy;
		double bestEnergy = Double.MAX_VALUE;
		while (counter.compareTo(end)==-1) {
			Map<String, Object> result = ch.getResult(counter);
			energy = (double) result.get("value"); 
			if (energy<bestEnergy) {
				this.solution.put("value", energy);
				this.solution.put("combination", result.get("combination"));
				bestEnergy = energy;
			}
			counter = counter.add(BigInteger.ONE);
		}
		this.calculatorService.setSolution(this.solution);
	}
}
