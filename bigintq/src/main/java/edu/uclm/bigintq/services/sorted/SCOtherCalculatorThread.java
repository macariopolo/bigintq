package edu.uclm.bigintq.services.sorted;

import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigExpression;

public class SCOtherCalculatorThread extends SCCalculatorThread {

	private List<Integer> combination;
	
	public SCOtherCalculatorThread(SCCalculatorService service, int core, BigCH ch, int constraintIndex, 
			JSONArray combination, BigInteger start, BigInteger end) throws FileNotFoundException {
		super(service, core, ch, constraintIndex, start, end);
		this.combination = this.fromJSON(combination);
	}

	@Override
	public void run() {
		//System.out.println("--- Inicio core other " + core + ", start=" + start + ", end = " + end + ", " + this.combination + " ---");
		BigInteger counter = start;
		double energy;
		BigExpression expr = ch.getExpressions().get(constraintIndex);
		List<Integer> currentCombination = new ArrayList<>();;
		List<Integer> variablePositions = new ArrayList<>();
		for (int i=0; i<this.combination.size(); i++) {
			currentCombination.add(this.combination.get(i));			
			if (this.combination.get(i)==null)
				variablePositions.add(i);
		}
		while (counter.compareTo(this.end)==-1) {
			this.developCombination(currentCombination, counter, variablePositions);
			energy = Math.abs(expr.getValue(currentCombination));
			if (energy<=bestEnergy) {
				if (energy<bestEnergy) {
					deleteFolder();
					bestEnergy = energy;
				}
				this.write(currentCombination, bestEnergy);
			}
			counter = counter.add(BigInteger.ONE);
		} 
	}
	
	private void developCombination(List<Integer> developed, BigInteger index, List<Integer> variablePositions) {
		BigInteger quotient = index;
		BigInteger remainder;
		BigInteger TWO = BigInteger.valueOf(2);
		for (int i=variablePositions.size()-1; i>=0; i--) {
			remainder = quotient.mod(TWO);
			quotient = quotient.divide(TWO);
			developed.set(variablePositions.get(i), remainder.intValue());
		}
	}
	
	private List<Integer> fromJSON(JSONArray combination) {
		List<Integer> result = new ArrayList<>();
		Object value;
		for (int i=0; i<combination.length(); i++) {
			value = combination.get(i);
			if (value.equals(JSONObject.NULL))
				result.add(null);
			else
				result.add((Integer) value);
		}
		return result;
	}
}
