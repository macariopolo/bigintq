package edu.uclm.bigintq.services;

import java.math.BigInteger;

import org.json.JSONObject;

import edu.uclm.bigintq.model.BigCH;

public class QACalculatorThread implements Runnable {

	private QACalculatorService service;
	private BigCH ch;
	private BigInteger start;
	private BigInteger end;
	private double bestEnergy;

	public QACalculatorThread(QACalculatorService service, BigCH ch, BigInteger start, BigInteger end, double bestEnergy) {
		this.service = service;
		this.ch = ch;
		this.start = start; 
		this.end = end;
		this.bestEnergy = bestEnergy;
	}

	@Override
	public void run() {
		BigInteger counter = start;
		JSONObject best = null, random = null;
		double energy;
		long time = System.currentTimeMillis();
		while (counter.compareTo(end)<=0) {
			JSONObject result = new JSONObject(ch.getResult(counter));
			energy = (double) result.get("energy"); 
			if (energy<=this.bestEnergy) {
				best = result;
				bestEnergy = energy;
				this.send(best, energy, "partial");
			} else if ((System.currentTimeMillis()-time)%5000==0) {
				random = result;
				this.send(random, energy, "random");
			} 
			if (QACalculatorService.ARTIFICIAL_PAUSE>0) {
				try {
					Thread.sleep(QACalculatorService.ARTIFICIAL_PAUSE);
				} catch (Exception e) {}
			}
			counter = counter.add(BigInteger.ONE);
		}
	}

	private void send(JSONObject solution, double energy, String type) {
		this.bestEnergy = this.service.send(solution, energy, type);
	}
}
