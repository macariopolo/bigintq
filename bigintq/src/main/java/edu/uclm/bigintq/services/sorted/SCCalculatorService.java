package edu.uclm.bigintq.services.sorted;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import edu.uclm.bigintq.entities.Host;
import edu.uclm.bigintq.http.Manager;
import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.model.BigExpression;
import edu.uclm.bigintq.model.g.BigGH;
import edu.uclm.bigintq.websockets.IWSClientReceiver;
import edu.uclm.bigintq.websockets.WSClient;

public class SCCalculatorService implements IWSClientReceiver {	
	private List<WSClient> wsClients;
	private JSONObject jsoProblem;;
	private BigCH ch;
	private int preparedWS, receivedProblems;
	private int currentExpressionIndex;
	private ArrayList<Integer> lambdaIndexes, ofIndexes;
	private List<Host> hosts;
	private WebSocketSession webSocketSession;
	private String folder;
	private BigInteger solutionsCounter = BigInteger.ZERO;
	private int host;
	private int cores;
	
	private FirstCombinationSender firstCS;
	private OtherCombinationSender otherCS;
	private String httpSessionId;
	private long startTime;
	private String orderType;
	
	public SCCalculatorService(String orderType) {
		this.lambdaIndexes = new ArrayList<>();
		this.ofIndexes = new ArrayList<>();
		
		this.orderType = orderType;
		this.firstCS = new FirstCombinationSender(this);
		this.otherCS = new OtherCombinationSender(this);
	}
	
	public void setProblem(JSONObject jsoProblem) throws JSONException, Exception {
		BigGH gh = new BigGH(jsoProblem.getJSONObject("gh"));
		JSONObject jsoInstance = jsoProblem.getJSONObject("instanceInfo");
		this.ch = gh.instantiate(jsoInstance, false); 
		this.ch.sortExpressionsByLambda(orderType);
		JSONObject jso = new JSONObject().
			put("type", "action").
			put("action", "RECEIVED_PROBLEM");
		this.send(jso);
	}
	
	public List<List<String>> prepareHosts(List<Host> hosts, BigGH gh, JSONObject jsoInstance) throws Exception {
		this.startTime = System.currentTimeMillis();
		this.hosts = hosts;
		this.wsClients = new ArrayList<>();
		
		this.ch = gh.instantiate(jsoInstance, false);
		this.ch.sortExpressionsByLambda(this.orderType);
		
		Host host;
		WSClient client;
		jsoInstance.remove("mathMLExpressions");
		this.jsoProblem = new JSONObject().
			put("gh", gh.toJSON()).
			put("instanceInfo", jsoInstance);
		
		for (int i=0; i<hosts.size(); i++) {
			client = new WSClient(this);
			this.wsClients.add(client);
		}
		
		List<String> mathML = ch.toMathML();
		
		String url;
		List<String> lHosts = new ArrayList<>();
		for (int i=0; i<hosts.size(); i++) {
			host = hosts.get(i);
			url = host.getWsUrl() + "wsSCCalculator";
			lHosts.add(url);
			client = this.wsClients.get(i);
			client.open(url);
		}
		List<List<String>> result = new ArrayList<>();
		result.add(mathML);
		result.add(lHosts);
		return result;
	}

	@Override
	public void onResponseReceived(JSONObject response) {
		String action = response.getString("action");
		JSONObject jso = new JSONObject().
			put("type", "calculating");
		if (action.equals("WS_PREPARED")) {
			this.preparedWS++;
			if (this.preparedWS == this.hosts.size()) {
				this.sendProblem();
			}
		} else if (action.equals("RECEIVED_PROBLEM")) {
			this.receivedProblems++;
			if (this.receivedProblems==this.hosts.size()) {
				this.firstCS.calculate();
			}
		} else if (action.equals("CALCULATE OTHER")) {
			this.currentExpressionIndex++;
			jso.put("currentExpressionIndex", this.currentExpressionIndex);
			this.sendToUA(jso);
			if (this.currentExpressionIndex<this.lambdaIndexes.size()) {
				this.otherCS.changeReceiver(this.otherCS);
				this.otherCS.loadFiles();
				this.otherCS.calculate();
			} else {
				this.calculateObjectiveFunction();
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void calculateObjectiveFunction() {
		List<Integer> combination;
		File temp = new File(this.getFolder());
		String[] extensions = { "combination" };
		Iterator<File> files = FileUtils.iterateFiles(temp, extensions, false);
		File file;
		
		List<List<Integer>> results = new ArrayList<>();
		double bestEnergy = Double.MAX_VALUE;
		double energy;
		while (files.hasNext()) {
			file = files.next();
			try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
				combination = (List<Integer>) ois.readObject();
				energy = 0.0;
				for (int j=0; j<this.ofIndexes.size(); j++) {
					BigExpression expr = this.ch.getExpressions().get(this.ofIndexes.get(j));
					energy = energy + expr.getValue(combination);
				}
				if (energy<bestEnergy) {
					results.clear();
					results.add(combination);
					bestEnergy = energy;
				} else if (energy==bestEnergy) {
					results.add(combination);
				}
				//System.out.println(solution);
			} catch(Exception e) {
				System.err.println(e);
			} finally {
				file.delete();
			}
		}
		
		long totalTime = (System.currentTimeMillis() - this.startTime) / 1000;

		JSONObject solution = new JSONObject().
				put("type", "solution");
		for (int i=0; i<results.size(); i++) {
			combination = results.get(i);
			solution.put("combination", this.toJSONArray(combination));
			solution.put("value", bestEnergy);
			solution.put("totalTime", totalTime);
			this.sendToUA(solution);
		}
	}
	
	private JSONArray toJSONArray(List<Integer> combination) {
		JSONArray jsa = new JSONArray();
		for (Integer value : combination) {
			jsa.put(value);
		}
		return jsa;
	}

	private void sendProblem() {
		WSClient client;
		JSONObject jso = new JSONObject().
			put("type", "problem").
			put("orderType", this.orderType).
			put("problem", this.jsoProblem);
		for (int i=0; i<this.wsClients.size(); i++) {
			client = this.wsClients.get(i);
			jso.put("host", i);
			client.sendMessage(jso);
		}
	}
	
	public void calculate(BigInteger start, BigInteger end, int constraintIndex, JSONArray jsaCombination) throws FileNotFoundException {
		this.cores = Runtime.getRuntime().availableProcessors();
		BigInteger combinations = end.subtract(start);
		if (combinations.intValue()<cores)
			cores = combinations.intValue();
		BigInteger chunk = combinations.divide(BigInteger.valueOf(cores));
		SCCalculatorThread[] cctt = new SCCalculatorThread[cores];
		Thread[] tt = new Thread[cores];
		BigInteger currentLoadStart = start;
		BigInteger currentLoadEnd = BigInteger.ZERO; 
		
		for (int i=0; i<cores; i++) {
			if (i<cores-1)
				currentLoadEnd = currentLoadStart.add(chunk);
			else
				currentLoadEnd = combinations;
			SCCalculatorThread ct = new SCOtherCalculatorThread(this, i, ch, constraintIndex, jsaCombination, currentLoadStart, currentLoadEnd);
			ct.setFolder(Manager.get().createFolder("m" + this.host + "_" + i, false) + File.separatorChar);
			
			cctt[i] = ct;
			tt[i] = new Thread(ct);
			currentLoadStart = currentLoadStart.add(chunk);
		}
		
		for (int i=0; i<cores; i++)
			tt[i].start();
		
		double bestEnergy = Double.MAX_VALUE;
		for (int i=0; i<cores; i++) {
			try {
				tt[i].join();
				if (cctt[i].getBestEnergy()<bestEnergy)
					bestEnergy = cctt[i].getBestEnergy();
			} catch (Exception e) {
				System.err.println(e);
			}
		}
		
		//System.out.println("Constraint: " + constraintIndex);
		this.filterResults(bestEnergy);
		
		JSONObject jso = new JSONObject().
				put("type", "action").
				put("action", "CALCULATED").
				put("subaction", "finish");
		this.send(jso);
	}
	
	public void calculate(BigInteger start, BigInteger end, int constraintIndex) throws FileNotFoundException {
		this.cores = 1; // Runtime.getRuntime().availableProcessors();
		BigInteger combinations = end.subtract(start);
		if (combinations.intValue()<cores)
			cores = combinations.intValue();
		BigInteger chunk = combinations.divide(BigInteger.valueOf(cores));
		SCCalculatorThread[] cctt = new SCCalculatorThread[cores];
		Thread[] tt = new Thread[cores];
		BigInteger currentLoadStart = start;
		BigInteger currentLoadEnd = BigInteger.ZERO; 
		
		BigExpression expr = this.ch.getExpressions().get(constraintIndex);
		expr.calculateEmptyCombination();
		
		for (int i=0; i<cores; i++) {
			if (i<cores-1)
				currentLoadEnd = currentLoadStart.add(chunk);
			else
				currentLoadEnd = combinations;
			SCCalculatorThread ct = new SCFirstCalculatorThread(this, i, ch, constraintIndex, currentLoadStart, currentLoadEnd);
			ct.setFolder(Manager.get().createFolder("m" + this.host + "_" + i, false) + File.separatorChar);
			
			cctt[i] = ct;
			tt[i] = new Thread(ct);
			currentLoadStart = currentLoadStart.add(chunk);
		}
		
		for (int i=0; i<cores; i++)
			tt[i].start();
		
		double bestEnergy = Double.MAX_VALUE;
		for (int i=0; i<cores; i++) {
			try {
				tt[i].join();
				if (cctt[i].getBestEnergy()<bestEnergy)
					bestEnergy = cctt[i].getBestEnergy();
			} catch (Exception e) {
				System.err.println(e);
			}
		}
		
		//System.out.println("Constraint: " + constraintIndex);
		this.filterResults(bestEnergy);

		JSONObject jso = new JSONObject().
				put("type", "action").
				put("action", "CALCULATED").
				put("subaction", "finish");
		this.send(jso);
	}
	
	@SuppressWarnings("unchecked")
	private void filterResults(double bestEnergy) {
		JSONObject jso = new JSONObject().
			put("type", "action").
			put("action", "CALCULATED").
			put("subaction", "combination");
		
		for (int i=0; i<this.cores; i++) {
			String folder = this.folder.substring(0, this.folder.length()-1) + "_" + i + File.separatorChar;
			File directory = new File(folder);
			String[] extensions = { "combination" };
			String extension = "-" + bestEnergy + ".combination";
			Iterator<File> files = FileUtils.iterateFiles(directory, extensions, false);
			File file;
			List<Integer> combination;
			while (files.hasNext()) {
				file = files.next();
				if (!file.getName().endsWith(extension)) {
					//file.delete();
					file.renameTo(new File(file.getAbsolutePath() + ".invalid"));
					continue;
				}
				try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
					combination = (List<Integer>) ois.readObject();
					jso.put("combination", this.toJSONArray(combination));
					jso.put("value", bestEnergy);
					this.send(jso);
				} catch(Exception e) {
					System.err.println(e);
				}
				file.renameTo(new File(file.getAbsolutePath() + ".passed"));
			}
			try {
				//FileUtils.deleteDirectory(directory);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void setWebSocketSession(WebSocketSession session) {
		this.webSocketSession = session;		
	}
	
	private void send(JSONObject jso) {
		try {
			this.webSocketSession.sendMessage(new TextMessage(jso.toString()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void sendToUA(JSONObject jso) {
		try {
			WebSocketSession ws = Manager.get().getWsSession(httpSessionId);
			ws.sendMessage(new TextMessage(jso.toString()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setHttpSessionId(String httpSessionId) {
		this.httpSessionId = httpSessionId;
	}

	public void setHost(int host) {
		this.host = host;
		this.folder = Manager.get().createFolder("m" + this.host, false) + File.separatorChar;
	}
	
	public void setFolder(String folder) {
		this.folder = Manager.get().createFolder(folder, false) + File.separatorChar;
	}
	
	public BigCH getCH() {
		return ch;
	}
	
	public ArrayList<Integer> getLambdaIndexes() {
		return lambdaIndexes;
	}
	
	public ArrayList<Integer> getOfIndexes() {
		return ofIndexes;
	}
	
	public int incrCurrentExpressionIndex() {
		this.currentExpressionIndex++;
		return this.currentExpressionIndex;
	}
	
	public List<WSClient> getWsClients() {
		return wsClients;
	}
	
	public String getFolder() {
		return folder;
	}
	
	public BigInteger getSolutionsCounter() {
		return solutionsCounter;
	}
	
	public int getCurrentExpressionIndex() {
		return currentExpressionIndex;
	}
	
	public List<Host> getHosts() {
		return hosts;
	}

	public BigInteger incrSolutionsCounter() {
		this.solutionsCounter = this.solutionsCounter.add(BigInteger.ONE);
		return this.solutionsCounter;
	}

	public synchronized void takePartialSolution(List<Integer> combination, BigInteger counter) {
		JSONObject jso = new JSONObject().
			put("type", "action").
			put("action", "partial").
			put("combination", combination.toString()).
			put("counter", counter.toString());
		TextMessage message = new TextMessage(jso.toString());
		try {
			this.webSocketSession.sendMessage(message);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
