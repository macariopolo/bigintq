package edu.uclm.bigintq.services;

import java.math.BigInteger;

import org.json.JSONObject;
import org.springframework.web.socket.WebSocketSession;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.websockets.WSQACalculator;

public class QACalculatorService {

	private WebSocketSession wsSession;
	private BigCH ch;
	private double bestEnergy;
	
	public static int CORES = Runtime.getRuntime().availableProcessors();
	public static long ARTIFICIAL_PAUSE;

	public QACalculatorService(WebSocketSession session) {
		this.wsSession = session;
		this.bestEnergy = Double.MAX_VALUE;
	}
	
	public void calculate(BigCH ch, BigInteger start, BigInteger end) {
		this.ch = ch;
		this.calculate(start, end, this.bestEnergy);
	}
	
	public void calculate(BigInteger start, BigInteger end, double bestEnergy) {
		int cores = CORES;
		BigInteger combinations = end.subtract(start);
		if (combinations.compareTo(BigInteger.valueOf(cores))<0)
			cores = combinations.intValue();
		
		BigInteger chunk = combinations.divide(BigInteger.valueOf(cores));
		if (chunk.longValue()==0)
			chunk = combinations;
		
		QACalculatorThread[] cctt = new QACalculatorThread[cores];
		Thread[] tt = new Thread[cores];
		BigInteger currentLoadStart = start;
		BigInteger currentLoadEnd; 
		
		int usedCores = 0;
		for (int i=0; i<cores; i++) {
			currentLoadEnd = currentLoadStart.add(chunk);
			if (currentLoadEnd.compareTo(end)>0)
				currentLoadEnd = end;
			
			QACalculatorThread ct = new QACalculatorThread(this, ch, currentLoadStart, currentLoadEnd, bestEnergy);
			cctt[i] = ct;
			tt[i] = new Thread(ct);
			currentLoadStart = currentLoadStart.add(chunk).add(BigInteger.ONE);
			usedCores++;
			if (currentLoadStart.compareTo(end)>0)
				break;
		}
		
		for (int i=0; i<usedCores; i++)
			tt[i].start();
		
		for (int i=0; i<usedCores; i++) {
			try {
				tt[i].join();
			} catch (Exception e) {
				System.err.println(e);
			}
		}
		
		WSQACalculator.send(wsSession, new JSONObject().put("type", "final").toString());
	}

	public synchronized double send(JSONObject solution, double energy, String type) {
		if (type.equals("random")) {
			solution.put("type", type);
			WSQACalculator.send(wsSession, solution.toString());
		} else if (energy<=this.bestEnergy) {
			this.bestEnergy = energy;
			solution.put("type", "partial");
			WSQACalculator.send(wsSession, solution.toString());
		}
		return this.bestEnergy;
	}
	
}
