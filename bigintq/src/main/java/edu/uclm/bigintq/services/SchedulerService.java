package edu.uclm.bigintq.services;

import org.json.JSONArray;
import org.springframework.stereotype.Service;

import edu.uclm.bigintq.model.BigCH;

@Service
public class SchedulerService {
	
	public Cluster distribute(BigCH ch, JSONArray jsaHosts, int cores) {
		Cluster cluster = new Cluster();
		cluster.setCombinations(ch.getNumberOfCombinations());
		cluster.setCores(cores);
		cluster.distribute(jsaHosts);
		return cluster;
	}
}
