package edu.uclm.bigintq.websockets;

import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.services.QACalculatorService;

@Component
public class WSQACalculator extends TextWebSocketHandler {
	
	private static HashMap<String, QACalculatorService> services = new HashMap<>();

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		session.setTextMessageSizeLimit(1000*1024*1024);
		JSONObject jso = new JSONObject().
			put("type", "threads").
			put("threads", QACalculatorService.CORES);
		send(session, jso.toString());
	}
	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		services.remove(session.getId());
		session.close();
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		String payload = message.getPayload();
		JSONObject jso = new JSONObject(payload);
		String type = jso.getString("type");
		
		String sStart = jso.getString("start");
		BigInteger start = new BigInteger(sStart);
		String sEnd = jso.getString("end");
		BigInteger end = new BigInteger(sEnd);

		if (type.equals("FIRST")) {
			BigCH ch = new BigCH();
			ch.setFunction(jso.getJSONObject("ch"));
	
			QACalculatorService calculatorService = new QACalculatorService(session);
			services.put(session.getId(), calculatorService);
			calculatorService.calculate(ch, start, end);
		} else if (type.equals("NEXT")) {
			double bestEnergy = jso.getDouble("bestEnergy");
			QACalculatorService calculatorService = services.get(session.getId());
			calculatorService.calculate(start, end, bestEnergy);
		}
	}
	
	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		services.remove(session.getId());
	}
	
	public static synchronized void send(WebSocketSession session, String message) {
		WebSocketMessage<?> wsMessage=new TextMessage(message);
		try {
			session.sendMessage(wsMessage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
