package edu.uclm.bigintq.websockets;

import java.io.IOException;
import java.math.BigInteger;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.uclm.bigintq.model.BigCH;
import edu.uclm.bigintq.services.massive.MQACalculatorService;

@Component
public class WSMassiveQACalculator extends TextWebSocketHandler {

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		session.setTextMessageSizeLimit(1000*1024*1024);
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		long time = System.currentTimeMillis();
		String payload = message.getPayload();
		JSONObject jso = new JSONObject(payload);
		BigCH ch = new BigCH();
		ch.setFunction(jso.getJSONObject("ch"));

		String sStart = jso.getString("start");
		BigInteger start = new BigInteger(sStart);
		String sEnd = jso.getString("end");
		BigInteger end = new BigInteger(sEnd);
		MQACalculatorService calculatorService = new MQACalculatorService();
		calculatorService.setWebSocketSession(session);
		calculatorService.calculate(ch, start, end);
		JSONObject result = new JSONObject(); 
		time = (System.currentTimeMillis() - time)/1000;
		this.send(session, result.toString());
		session.close();
	}
	
	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		exception.printStackTrace();
	}
	
	private void send(WebSocketSession session, String message) {
		WebSocketMessage<?> wsMessage=new TextMessage(message);
		try {
			session.sendMessage(wsMessage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
