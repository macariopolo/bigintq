package edu.uclm.bigintq.websockets;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

@Configuration
@EnableWebSocket
public class WSConfigurer implements WebSocketConfigurer {
	
	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.
			addHandler(new WSQACalculator(), "/wsQACalculator").
			addHandler(new WSMassiveQACalculator(), "/wsMassiveQACalculator").
			addHandler(new WSSCResultsSender(), "/wsSCResults").
			addHandler(new WSSCCalculator(), "/wsSCCalculator").
			addHandler(new WSDWave(), "/wsDwave").
			setAllowedOrigins("*").
			addInterceptors(new HttpSessionHandshakeInterceptor());
	}
}
