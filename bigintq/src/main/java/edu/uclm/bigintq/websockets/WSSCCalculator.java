package edu.uclm.bigintq.websockets;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.uclm.bigintq.services.sorted.SCCalculatorService;

@Component
public class WSSCCalculator extends TextWebSocketHandler {
	
	private static Map<String, SCCalculatorService> services = new HashMap<>();

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		session.setBinaryMessageSizeLimit(1000*1024*1024);
		//String httpSessionId = session.getAttributes().get("HTTP.SESSION.ID").toString();
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		JSONObject jso;
		try { 
			jso = new JSONObject(message.getPayload());
		} catch (Exception e) {
			return;
		}
		
		String type = jso.optString("type");
		if (type.length()==0)
			return;
		SCCalculatorService service;
		if (type.equals("problem")) {
			JSONObject jsoProblem = jso.getJSONObject("problem");
			String orderType = jso.getString("orderType");
			service = new SCCalculatorService(orderType);
			int host = jso.getInt("host");
			service.setHost(host);
			services.put(session.getId(), service);
			service.setWebSocketSession(session);
			service.setProblem(jsoProblem);
		} else if (type.equals("CALCULATE")) {
			service = services.get(session.getId());
			service.setWebSocketSession(session);
			BigInteger start = new BigInteger(jso.getString("start"));
			BigInteger end = new BigInteger(jso.getString("end"));
			int constraintIndex = jso.getInt("constraintIndex");
			JSONArray combination = jso.optJSONArray("combination");
			if (combination!=null)
				service.calculate(start, end, constraintIndex, combination);
			else
				service.calculate(start, end, constraintIndex);
		}
	}
	
	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		exception.printStackTrace();
	}
}
