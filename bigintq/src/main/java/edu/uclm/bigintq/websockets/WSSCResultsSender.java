package edu.uclm.bigintq.websockets;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.uclm.bigintq.http.Manager;

@Component
public class WSSCResultsSender extends TextWebSocketHandler {
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		session.setBinaryMessageSizeLimit(1000*1024*1024);
		String httpSessionId = session.getAttributes().get("HTTP.SESSION.ID").toString();
		Manager.get().putWsSession(httpSessionId, session);
	}
		
	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		exception.printStackTrace();
	}
}
