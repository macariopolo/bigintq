package edu.uclm.bigintq.websockets;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.CountDownLatch;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.json.JSONObject;

@ClientEndpoint
public class WSClient {
	private CountDownLatch latch = new CountDownLatch(1);
    private Session session;
	private IWSClientReceiver receiver;
 
    public WSClient(IWSClientReceiver receiver) {
    	this.receiver = receiver;
	}
    
    public void setReceiver(IWSClientReceiver receiver) {
		this.receiver = receiver;
	}
    
    public void open(String url) throws Exception {
    	WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        container.connectToServer(this, new URI(url));
        this.latch.await();    	
    }

	@OnOpen
    public void onOpen(Session session) {
        this.session = session;
        latch.countDown();
        this.receiver.onResponseReceived(new JSONObject().put("type", "action").put("action", "WS_PREPARED"));
    }
	
	@OnError
	public void onError(Session session, Throwable t) {
		this.session = session;
		latch.countDown();
		this.receiver.onResponseReceived(new JSONObject().put("type", "action").put("action", "WS_ERROR"));
	}
 
    @OnMessage
    public void onText(String message, Session session) {
        JSONObject jso = new JSONObject(message);
        String type = jso.getString("type");
        if (type.equals("action"))
        	this.receiver.onResponseReceived(jso);
    }
 
    @OnClose
    public void onClose(CloseReason reason, Session session) {
        System.out.println("Closing a WebSocket due to " + reason.getReasonPhrase());
    }
 
    public CountDownLatch getLatch() {
        return latch;
    }
    
    public void sendMessage(JSONObject message) {
        try {
            session.getBasicRemote().sendText(message.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
