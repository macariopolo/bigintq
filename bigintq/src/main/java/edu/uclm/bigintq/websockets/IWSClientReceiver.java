package edu.uclm.bigintq.websockets;

import org.json.JSONObject;

public interface IWSClientReceiver {
	void onResponseReceived(JSONObject jso);
}
