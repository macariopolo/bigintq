package edu.uclm.bigintq.websockets;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.uclm.bigintq.dwave.Runner;

@Component
public class WSDWave extends TextWebSocketHandler {

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		session.setTextMessageSizeLimit(1000*1024*1024);
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		String payload = message.getPayload();
		JSONObject jso = new JSONObject(payload);
		String classPath = jso.getString("classPath");
		JSONArray lines = jso.getJSONArray("code");
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<lines.length(); i++)
			sb.append(lines.getString(i) + "\n");
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Runner.run(session, classPath, sb.toString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		exception.printStackTrace();
	}
	
	public static synchronized void send(WebSocketSession session, String message) {
		WebSocketMessage<?> wsMessage=new TextMessage(message);
		try {
			session.sendMessage(wsMessage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
