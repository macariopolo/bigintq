package edu.uclm.bigintq.entities;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class HostConfiguration {
	@Id @Column(length = 36)
	private String id;
	private String name;
	@OneToMany
	private List<Host> hosts;

	public HostConfiguration() {
		this.id = UUID.randomUUID().toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setHosts(List<Host> hosts) {
		this.hosts = hosts;
	}
	
	public List<Host> getHosts() {
		return hosts;
	}
}
