package edu.uclm.bigintq.entities;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Host {
	@Id @Column(length = 36)
	private String id;
	private String ip;
	private Integer port;
	private Boolean active;
	@ManyToOne
	private HostConfiguration configuration;
	
	@Transient
	private boolean working;

	public Host() {
		this.id = UUID.randomUUID().toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
	
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	public Boolean getActive() {
		return active;
	}
	
	public void setConfiguration(HostConfiguration configuration) {
		this.configuration = configuration;
	}
	
	@JsonIgnore
	public HostConfiguration getConfiguration() {
		return configuration;
	}

	public String getHttpUrl() {
		return "http://" + this.ip + ":" + this.port + "/";
	}
	
	public String getWsUrl() {
		return "ws://" + this.ip + ":" + this.port + "/";
	}

	public void setWorking(boolean working) {
		this.working = working;		
	}
	
	public boolean isWorking() {
		return working;
	}
}
