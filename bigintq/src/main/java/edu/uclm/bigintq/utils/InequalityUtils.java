package edu.uclm.bigintq.utils;

public class InequalityUtils {
	public static boolean hasGreaterOperator(String operator) {
		return operator.equals(">") || operator.equals(">=");
	}
}
