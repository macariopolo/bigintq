package edu.uclm.bigintq;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import edu.uclm.bigintq.services.QACalculatorService;

@SpringBootApplication
public class LanzadoraBIS {

    public static void main(String[] args) throws IOException {
    	String defaultPort = "80";
    	
    	String sPort = null, sCores = null, sArtificialPause=null;
    	if (args.length>0) {
    		sPort = read(args, "-port:");
    		sCores = read(args, "-cores:");
    		sArtificialPause = read(args, "-ap:");
    	}
    	
    	if (sPort!=null) {
    		defaultPort = sPort;
    		System.out.print("Listening at port: " + defaultPort);
    	} else {
	    	System.out.print("Listening port (enter for default: " + defaultPort + "): ");
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	    	
	    	sPort = reader.readLine().trim();
	    	if (sPort.length()==0)
	    		sPort = defaultPort;
    	}
    	
    	if (sCores!=null) {
    		QACalculatorService.CORES = Integer.parseInt(sCores);
    	}
    	
    	if (sArtificialPause!=null) {
    		QACalculatorService.ARTIFICIAL_PAUSE = Long.parseLong(sArtificialPause);
    	}
    	
    	SpringApplication app = new SpringApplication(LanzadoraBIS.class);
    	app.setDefaultProperties(Collections.singletonMap("server.port", sPort.trim()));
    	
    	app.run(args);
    }

	private static String read(String[] args, String token) {
		String result = null;
		for (int i=0; i<args.length; i++) {
			if (args[i].startsWith(token)) {
				result = args[i].substring(token.length());
				break;
			}
		}
		return result;
	}
}
