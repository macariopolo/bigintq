{
	"functionName" : "cajas",
	"variables": [
		{
			"name": "v",
			"values": [ 1, 3, 2, 6, 4, 5, 4, 10 ],
			"cardinals": "8"
		}
	],
	"xCardinals": "8",
	"expressions": [
		{
			"type" : "Sum",
			"expressions" : [
				{
					"type" : "Product",
					"left" : {
						"type" : "DoubleValue",
						"value" : 1
					},
					"right" : {
						"type" : "UseOfX",
						"value" : "0"
					}
				},
				{
					"type" : "Product",
					"left" : {
						"type" : "DoubleValue",
						"value" : 3
					},
					"right" : {
						"type" : "UseOfX",
						"value" : "1"
					}
				},
				{
					"type" : "Product",
					"left" : {
						"type" : "DoubleValue",
						"value" : 2
					},
					"right" : {
						"type" : "UseOfX",
						"value" : "2"
					}
				},
				{
					"type" : "Product",
					"left" : {
						"type" : "DoubleValue",
						"value" : 6
					},
					"right" : {
						"type" : "UseOfX",
						"value" : "3"
					}
				},
				{
					"type" : "Product",
					"left" : {
						"type" : "DoubleValue",
						"value" : 4
					},
					"right" : {
						"type" : "UseOfX",
						"value" : "4"
					}
				},
				{
					"type" : "Product",
					"left" : {
						"type" : "DoubleValue",
						"value" : 5
					},
					"right" : {
						"type" : "UseOfX",
						"value" : "5"
					}
				},
				{
					"type" : "Product",
					"left" : {
						"type" : "DoubleValue",
						"value" : 4
					},
					"right" : {
						"type" : "UseOfX",
						"value" : "6"
					}
				},
				{
					"type" : "Product",
					"left" : {
						"type" : "DoubleValue",
						"value" : 10
					},
					"right" : {
						"type" : "UseOfX",
						"value" : "7"
					}
				}
			]
		},
		{
			"type": "Product",
			"left": {
				"type": "Lambda",
				"value": 10
			},
			"right" : {
				"type" : "Square",
				"value" : {
					"type" : "Sum",
					"expressions" : [
						{
							"type" : "UseOfX",
							"value" : "0"
						},
						{
							"type" : "UseOfX",
							"value" : "1"
						},
						{
							"type" : "UseOfX",
							"value" : "2"
						},
						{
							"type" : "DoubleValue",
							"value" : -1
						},
					]
				}
			}
		},
		{
			"type": "Product",
			"left": {
				"type": "Lambda",
				"value": 10
			},
			"right" : {
				"type" : "Square",
				"value" : {
					"type" : "Sum",
					"expressions" : [
						{
							"type" : "UseOfX",
							"value" : "3"
						},
						{
							"type" : "UseOfX",
							"value" : "4"
						},
						{
							"type" : "UseOfX",
							"value" : "5"
						},
						{
							"type" : "DoubleValue",
							"value" : -2
						},
					]
				}
			}
		},
		{
			"type": "Product",
			"left": {
				"type": "Lambda",
				"value": 10
			},
			"right" : {
				"type" : "Square",
				"value" : {
					"type" : "Sum",
					"expressions" : [
						{
							"type" : "UseOfX",
							"value" : "6"
						},
						{
							"type" : "UseOfX",
							"value" : "7"
						},
						{
							"type" : "DoubleValue",
							"value" : -1
						},
					]
				}
			}
		}
	]
}