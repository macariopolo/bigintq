package edu.uclm.bigintq.http;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import edu.uclm.bigintq.websockets.WSClient;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class SchedulerTest {

	@Autowired
	private MockMvc mvc;
	
	public HttpSession testSetProblemGHOk() throws Exception {
		JSONObject f2 = Manager.get().read("g2.txt");
		
		MvcResult result = mvc.perform(MockMvcRequestBuilders.
				post("/scheduler/setProblem").
				contentType("application/json").
				content(f2.toString())).andReturn();	
		
		HttpSession session = result.getRequest().getSession(); 
		return session;
	}
	
	public HttpSession testInstantiate() throws Exception {
		HttpSession session = this.testSetProblemGHOk();
		
		JSONObject jso = Manager.get().read("g2Instance.txt");
		
		MvcResult result = mvc.perform(MockMvcRequestBuilders.
				post("/scheduler/instantiate").
				session((MockHttpSession) session).
				contentType("application/json").
				content(jso.toString())).andReturn();
		return session;
	}
	
	//@Test
	public void testInstantiateAndCalculate() throws Exception {
		HttpSession session = this.testInstantiate();
		
		MvcResult result = mvc.perform(MockMvcRequestBuilders.
				get("/scheduler/getWebSockets").
				contentType("application/json").
				session((MockHttpSession) session)).andReturn();
		
		JSONArray jsa = new JSONArray(result.getResponse().getContentAsString());
		JSONObject jso;
		for (int i=0; i<jsa.length(); i++) {
			jso = jsa.getJSONObject(i);
			//WSClient ws = new WSClient(jso.getString("url"));
			//ws.sendMessage(jso.toString());
		}
		
		System.out.println();
	}
	
	public HttpSession testSetF2Ok() throws Exception {
		JSONObject f2 = Manager.get().read("f2.txt");
		
		MvcResult result = mvc.perform(MockMvcRequestBuilders.
				post("/scheduler/setFunction").
				contentType("application/json").
				content(f2.toString())).andReturn();
		
		HttpSession session = result.getRequest().getSession(); 
		return session;
	}
	
	public HttpSession testSaveF2Ok() throws Exception {	
		HttpSession session = this.testSetF2Ok(); 
		
		mvc.perform(MockMvcRequestBuilders.
				put("/scheduler/save").
				session((MockHttpSession) session)).
			andExpect(status().isOk());
		
		return session;
	}
	
	public void testDistributeProblemF2Ok() throws Exception {	
		HttpSession session = this.testSaveF2Ok(); 
		
		mvc.perform(MockMvcRequestBuilders.
				get("/scheduler/distributeProblem").
				session((MockHttpSession) session)).
			andExpect(status().isOk());
	}
}
